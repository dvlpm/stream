ENV?=dev

ifneq ("$(wildcard docker-compose.override.yml)","")
include .env.local
endif

ifneq ("$(wildcard docker-compose.override.yml)","")
DOCKER_COMPOSE_TEST_OVERRIDE=-f docker-compose.override.yml
else
DOCKER_COMPOSE_TEST_OVERRIDE=
endif

dca := docker-compose \
	-f docker-compose.yml \
	$(DOCKER_COMPOSE_TEST_OVERRIDE)

dct := $(dca) \
	-f docker-compose.test.yml

de := docker-compose exec
de-php := $(de) php sh -c

### ---------------------
###	Init
### ---------------------
init: up composer-install migrate

composer:
	$(de-php) '$(MAKECMDGOALS)'

composer-install:
	$(de-php) 'composer install'

console:
	$(de-php) "bin/console $(filter-out $@,$(MAKECMDGOALS))"

migrate:
	$(de-php) 'bin/console doctrine:migrations:migrate --no-interaction'

fixture:
	$(de-php) 'bin/console fixtures:load "Dvlpm\Stream\Tests\Support\Fixture\HallFixture"'

blackfire:
	docker-compose exec php blackfire curl \
	  -H "Accept: application/json" \
	  -H "Content-Type: application/json" \
	  -H "Authorization: Bearer $(BLACKFIRE_TOKEN)" \
	  http://nginx$(BLACKFIRE_URL)

prepare:
	cp docker-compose.override.dist.yml docker-compose.override.yml

### ---------------------
###	Develop
### ---------------------
up:
	$(dca) up -d

ups:
	$(dca) up

down:
	$(dca) down

sh:
	$(de-php) 'bash'

redis-cli:
	$(de) redis 'redis-cli'

### ---------------------
###	Test
### ---------------------
test-up:
	$(dct) up -d

test-ups:
	$(dct) up

test-down:
	$(dct) down

test-migrate:
	$(de-php) 'bin/console doctrine:migrations:migrate --env=test --no-interaction'

test: codecept-run

codecept-run:
	$(de-php) 'vendor/bin/codecept run'

### ---------------------
###	Builds
### ---------------------
@test: test-up composer-install test-migrate codecept-run

### ---------------------
###	Help
### ---------------------
# ignore all not-found targets
%:
	@: