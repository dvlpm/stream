# Stream service

API for stream service.

* [Postman Collection](https://documenter.getpostman.com/view/8776767/SztK2QJt?version=latest)

## Development

Use following commands for make development easier:

### Run

Up containers:

`
make up
`

Bash inside app container:

`
make sh
`

### Test

Up test environment:

`
make test-up
`

Run tests:

`
make test
`

Bash inside test container:

`
make sh
`
