<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Command;

use Dvlpm\Stream\Domain\Space\Payload\AddParticipantToRoomPayload;
use Ramsey\Uuid\UuidInterface;

final class AddParticipantToRoomCommand
{
    private UuidInterface $userId;
    private UuidInterface $roomId;
    private AddParticipantToRoomPayload $payload;

    public function __construct(UuidInterface $userId, UuidInterface $roomId, AddParticipantToRoomPayload $payload)
    {
        $this->userId = $userId;
        $this->roomId = $roomId;
        $this->payload = $payload;
    }

    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }

    public function getRoomId(): UuidInterface
    {
        return $this->roomId;
    }

    public function getPayload(): AddParticipantToRoomPayload
    {
        return $this->payload;
    }
}
