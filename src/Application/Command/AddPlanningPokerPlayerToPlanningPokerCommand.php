<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Command;

use Ramsey\Uuid\UuidInterface;

final class AddPlanningPokerPlayerToPlanningPokerCommand
{
    private UuidInterface $userId;
    private UuidInterface $planningPokerPlayerId;
    private UuidInterface $playerUserId;
    private UuidInterface $planningPokerId;

    public function __construct(
        UuidInterface $userId,
        UuidInterface $planningPokerPlayerId,
        UuidInterface $playerUserId,
        UuidInterface $planningPokerId
    ) {
        $this->userId = $userId;
        $this->planningPokerPlayerId = $planningPokerPlayerId;
        $this->playerUserId = $playerUserId;
        $this->planningPokerId = $planningPokerId;
    }

    public function getPlanningPokerPlayerId(): UuidInterface
    {
        return $this->planningPokerPlayerId;
    }

    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }

    public function getPlayerUserId(): UuidInterface
    {
        return $this->playerUserId;
    }

    public function getPlanningPokerId(): UuidInterface
    {
        return $this->planningPokerId;
    }
}
