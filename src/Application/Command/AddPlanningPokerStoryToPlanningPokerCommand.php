<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Command;

use Dvlpm\Stream\Domain\Content\Payload\AddPlanningPokerStoryToPlanningPokerPayload;
use Ramsey\Uuid\UuidInterface;

final class AddPlanningPokerStoryToPlanningPokerCommand
{
    private UuidInterface $userId;
    private UuidInterface $planningPokerId;
    private AddPlanningPokerStoryToPlanningPokerPayload $payload;

    public function __construct(
        UuidInterface $userId,
        UuidInterface $planningPokerId,
        AddPlanningPokerStoryToPlanningPokerPayload $payload
    ) {
        $this->userId = $userId;
        $this->planningPokerId = $planningPokerId;
        $this->payload = $payload;
    }

    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }

    public function getPlanningPokerId(): UuidInterface
    {
        return $this->planningPokerId;
    }

    public function getPayload(): AddPlanningPokerStoryToPlanningPokerPayload
    {
        return $this->payload;
    }
}
