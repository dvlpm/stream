<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Command;

use Dvlpm\Stream\Domain\Content\Payload\AddPlanningPokerStoryVoteToPlanningPokerStoryPayload;
use Ramsey\Uuid\UuidInterface;

final class AddPlanningPokerStoryVoteToPlanningPokerStoryCommand
{
    private UuidInterface $userId;
    private UuidInterface $planningPokerStoryId;
    private AddPlanningPokerStoryVoteToPlanningPokerStoryPayload $payload;

    public function __construct(
        UuidInterface $userId,
        UuidInterface $planningPokerStoryId,
        AddPlanningPokerStoryVoteToPlanningPokerStoryPayload $payload
    ) {
        $this->userId = $userId;
        $this->planningPokerStoryId = $planningPokerStoryId;
        $this->payload = $payload;
    }

    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }

    public function getPlanningPokerStoryId(): UuidInterface
    {
        return $this->planningPokerStoryId;
    }

    public function getPayload(): AddPlanningPokerStoryVoteToPlanningPokerStoryPayload
    {
        return $this->payload;
    }
}
