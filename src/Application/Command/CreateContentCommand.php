<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Command;

use Dvlpm\Stream\Domain\Content\Payload\CreateContentPayloadInterface;
use Ramsey\Uuid\UuidInterface;

final class CreateContentCommand
{
    private UuidInterface $userId;
    private CreateContentPayloadInterface $payload;

    public function __construct(UuidInterface $userId, CreateContentPayloadInterface $payload)
    {
        $this->userId = $userId;
        $this->payload = $payload;
    }

    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }

    public function getPayload(): CreateContentPayloadInterface
    {
        return $this->payload;
    }
}
