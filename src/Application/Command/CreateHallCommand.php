<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Command;

use Dvlpm\Stream\Domain\Space\Payload\CreateHallPayload;
use Ramsey\Uuid\UuidInterface;

final class CreateHallCommand
{
    private UuidInterface $userId;
    private CreateHallPayload $payload;

    public function __construct(UuidInterface $userId, CreateHallPayload $payload)
    {
        $this->userId = $userId;
        $this->payload = $payload;
    }

    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }

    public function getPayload(): CreateHallPayload
    {
        return $this->payload;
    }
}
