<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Command;

use Dvlpm\Stream\Domain\Space\Payload\EnterRoomPayload;
use Ramsey\Uuid\UuidInterface;

final class EnterRoomCommand
{
    private UuidInterface $userId;
    private UuidInterface $roomId;
    private EnterRoomPayload $payload;

    public function __construct(UuidInterface $userId, UuidInterface $roomId, EnterRoomPayload $payload)
    {
        $this->userId = $userId;
        $this->roomId = $roomId;
        $this->payload = $payload;
    }

    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }

    public function getRoomId(): UuidInterface
    {
        return $this->roomId;
    }

    public function getPayload(): EnterRoomPayload
    {
        return $this->payload;
    }
}
