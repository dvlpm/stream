<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Command;

use Ramsey\Uuid\UuidInterface;

final class RemoveContentCommand
{
    private UuidInterface $userId;
    private UuidInterface $contentId;

    public function __construct(UuidInterface $userId, UuidInterface $contentId)
    {
        $this->userId = $userId;
        $this->contentId = $contentId;
    }

    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }

    public function getContentId(): UuidInterface
    {
        return $this->contentId;
    }
}
