<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Command;

use Ramsey\Uuid\UuidInterface;

final class RemoveContentFromRoomCommand
{
    private UuidInterface $userId;
    private UuidInterface $roomId;
    private UuidInterface $contentId;

    public function __construct(UuidInterface $userId, UuidInterface $roomId, UuidInterface $contentId)
    {
        $this->userId = $userId;
        $this->roomId = $roomId;
        $this->contentId = $contentId;
    }

    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }

    public function getRoomId(): UuidInterface
    {
        return $this->roomId;
    }

    public function getContentId(): UuidInterface
    {
        return $this->contentId;
    }
}
