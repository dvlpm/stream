<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Command;

use Dvlpm\Stream\Domain\Space\Model\Identifier;
use Ramsey\Uuid\UuidInterface;

final class RemoveHallCommand
{
    private UuidInterface $userId;
    private Identifier $hallIdentifier;

    public function __construct(UuidInterface $userId, Identifier $hallIdentifier)
    {
        $this->userId = $userId;
        $this->hallIdentifier = $hallIdentifier;
    }

    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }

    public function getHallIdentifier(): Identifier
    {
        return $this->hallIdentifier;
    }
}
