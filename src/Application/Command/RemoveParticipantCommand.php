<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Command;

use Ramsey\Uuid\UuidInterface;

final class RemoveParticipantCommand
{
    private UuidInterface $userId;
    private UuidInterface $participantId;

    public function __construct(UuidInterface $userId, UuidInterface $participantId)
    {
        $this->userId = $userId;
        $this->participantId = $participantId;
    }

    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }

    public function getParticipantId(): UuidInterface
    {
        return $this->participantId;
    }
}
