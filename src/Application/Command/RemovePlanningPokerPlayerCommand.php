<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Command;

use Ramsey\Uuid\UuidInterface;

final class RemovePlanningPokerPlayerCommand
{
    private UuidInterface $userId;
    private UuidInterface $planningPokerPlayerId;

    public function __construct(UuidInterface $userId, UuidInterface $planningPokerPlayerId)
    {
        $this->userId = $userId;
        $this->planningPokerPlayerId = $planningPokerPlayerId;
    }

    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }

    public function getPlanningPokerPlayerId(): UuidInterface
    {
        return $this->planningPokerPlayerId;
    }
}
