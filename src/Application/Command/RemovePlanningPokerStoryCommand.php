<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Command;

use Ramsey\Uuid\UuidInterface;

final class RemovePlanningPokerStoryCommand
{
    private UuidInterface $userId;
    private UuidInterface $planningPokerStoryId;

    public function __construct(UuidInterface $userId, UuidInterface $planningPokerStoryId)
    {
        $this->userId = $userId;
        $this->planningPokerStoryId = $planningPokerStoryId;
    }

    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }

    public function getPlanningPokerStoryId(): UuidInterface
    {
        return $this->planningPokerStoryId;
    }
}
