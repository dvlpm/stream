<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Command;

use Ramsey\Uuid\UuidInterface;

final class RemoveRoomCommand
{
    private UuidInterface $userId;
    private UuidInterface $roomId;

    public function __construct(UuidInterface $userId, UuidInterface $roomId)
    {
        $this->userId = $userId;
        $this->roomId = $roomId;
    }

    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }

    public function getRoomId(): UuidInterface
    {
        return $this->roomId;
    }
}
