<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Command;

use Dvlpm\Stream\Domain\Content\Payload\UpdateContentPayloadInterface;
use Ramsey\Uuid\UuidInterface;

final class UpdateContentCommand
{
    private UuidInterface $userId;
    private UuidInterface $contentId;
    private UpdateContentPayloadInterface $payload;

    public function __construct(UuidInterface $userId, UuidInterface $contentId, UpdateContentPayloadInterface $payload)
    {
        $this->userId = $userId;
        $this->contentId = $contentId;
        $this->payload = $payload;
    }

    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }

    public function getContentId(): UuidInterface
    {
        return $this->contentId;
    }

    public function getPayload(): UpdateContentPayloadInterface
    {
        return $this->payload;
    }
}
