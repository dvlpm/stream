<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Command;

use Dvlpm\Stream\Domain\Space\Model\Identifier;
use Dvlpm\Stream\Domain\Space\Payload\UpdateHallPayload;
use Ramsey\Uuid\UuidInterface;

final class UpdateHallCommand
{
    private UuidInterface $userId;
    private Identifier $hallIdentifier;
    private UpdateHallPayload $payload;

    public function __construct(UuidInterface $userId, Identifier $hallIdentifier, UpdateHallPayload $payload)
    {
        $this->userId = $userId;
        $this->hallIdentifier = $hallIdentifier;
        $this->payload = $payload;
    }

    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }

    public function getHallIdentifier(): Identifier
    {
        return $this->hallIdentifier;
    }

    public function getPayload(): UpdateHallPayload
    {
        return $this->payload;
    }
}
