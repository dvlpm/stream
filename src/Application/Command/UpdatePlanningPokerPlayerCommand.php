<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Command;

use Dvlpm\Stream\Domain\Content\Model\PlanningPokerPlayerStatus;
use Ramsey\Uuid\UuidInterface;

final class UpdatePlanningPokerPlayerCommand
{
    private UuidInterface $userId;
    private UuidInterface $planningPokerPlayerId;
    private PlanningPokerPlayerStatus $status;

    public function __construct(
        UuidInterface $userId,
        UuidInterface $planningPokerPlayerId,
        PlanningPokerPlayerStatus $status
    ) {
        $this->userId = $userId;
        $this->planningPokerPlayerId = $planningPokerPlayerId;
        $this->status = $status;
    }

    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }

    public function getPlanningPokerPlayerId(): UuidInterface
    {
        return $this->planningPokerPlayerId;
    }

    public function getStatus(): PlanningPokerPlayerStatus
    {
        return $this->status;
    }
}
