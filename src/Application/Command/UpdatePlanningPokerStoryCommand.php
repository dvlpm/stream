<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Command;

use Dvlpm\Stream\Domain\Content\Payload\UpdatePlanningPokerStoryPayload;
use Ramsey\Uuid\UuidInterface;

final class UpdatePlanningPokerStoryCommand
{
    private UuidInterface $userId;
    private UuidInterface $planningPokerStoryId;
    private UpdatePlanningPokerStoryPayload $payload;

    public function __construct(UuidInterface $userId, UuidInterface $planningPokerStoryId, UpdatePlanningPokerStoryPayload $payload)
    {
        $this->userId = $userId;
        $this->planningPokerStoryId = $planningPokerStoryId;
        $this->payload = $payload;
    }

    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }

    public function getPlanningPokerStoryId(): UuidInterface
    {
        return $this->planningPokerStoryId;
    }

    public function getPayload(): UpdatePlanningPokerStoryPayload
    {
        return $this->payload;
    }
}
