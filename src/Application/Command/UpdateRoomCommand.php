<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Command;

use Dvlpm\Stream\Domain\Space\Payload\UpdateRoomPayload;
use Ramsey\Uuid\UuidInterface;

final class UpdateRoomCommand
{
    private UuidInterface $userId;
    private UuidInterface $roomId;
    private UpdateRoomPayload $payload;

    public function __construct(UuidInterface $userId, UuidInterface $roomId, UpdateRoomPayload $payload)
    {
        $this->userId = $userId;
        $this->roomId = $roomId;
        $this->payload = $payload;
    }

    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }

    public function getRoomId(): UuidInterface
    {
        return $this->roomId;
    }

    public function getPayload(): UpdateRoomPayload
    {
        return $this->payload;
    }
}
