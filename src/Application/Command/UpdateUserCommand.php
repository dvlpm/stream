<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Command;

use Dvlpm\Stream\Domain\User\Payload\UpdateUserPayload;
use Ramsey\Uuid\UuidInterface;

final class UpdateUserCommand
{
    private UuidInterface $userId;
    private UpdateUserPayload $payload;

    public function __construct(UuidInterface $userId, UpdateUserPayload $payload)
    {
        $this->userId = $userId;
        $this->payload = $payload;
    }

    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }

    public function getPayload(): UpdateUserPayload
    {
        return $this->payload;
    }
}
