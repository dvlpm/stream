<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\CommandBus;

interface CommandBusInterface
{
    public function handle(object $command): void;
}
