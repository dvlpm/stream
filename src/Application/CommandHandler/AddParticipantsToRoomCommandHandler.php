<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\CommandHandler;

use Dvlpm\Stream\Application\Command\AddParticipantToRoomCommand;
use Dvlpm\Stream\Domain\Common\Event\EventDispatcherInterface;
use Dvlpm\Stream\Domain\Space\Repository\RoomRepositoryInterface;
use Dvlpm\Stream\Domain\User\Repository\UserRepositoryInterface;

final class AddParticipantsToRoomCommandHandler
{
    private EventDispatcherInterface $eventDispatcher;
    private UserRepositoryInterface $userRepository;
    private RoomRepositoryInterface $roomRepository;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        UserRepositoryInterface $userRepository,
        RoomRepositoryInterface $roomRepository
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->userRepository = $userRepository;
        $this->roomRepository = $roomRepository;
    }

    public function __invoke(AddParticipantToRoomCommand $command): void
    {
        $room = $this->roomRepository->findOneByIdOrFail($command->getRoomId());

        $room->addParticipant(
            $this->userRepository->findOneByIdOrFail($command->getUserId()),
            $command->getPayload()
        );

        $this->eventDispatcher->dispatchFrom($room);
    }
}
