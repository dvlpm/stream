<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\CommandHandler;

use Dvlpm\Stream\Application\Command\AddPlanningPokerPlayerToPlanningPokerCommand;
use Dvlpm\Stream\Domain\Common\Event\EventDispatcherInterface;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerPlayer;
use Dvlpm\Stream\Domain\Content\Repository\PlanningPokerRepositoryInterface;
use Dvlpm\Stream\Domain\User\Repository\UserRepositoryInterface;

final class AddPlanningPokerPlayerToPlanningPokerCommandHandler
{
    private EventDispatcherInterface $eventDispatcher;
    private UserRepositoryInterface $userRepository;
    private PlanningPokerRepositoryInterface $planningPokerRepository;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        UserRepositoryInterface $userRepository,
        PlanningPokerRepositoryInterface $planningPokerRepository
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->userRepository = $userRepository;
        $this->planningPokerRepository = $planningPokerRepository;
    }

    public function __invoke(AddPlanningPokerPlayerToPlanningPokerCommand $command): void
    {
        $planningPoker = $this->planningPokerRepository->findOneByIdOrFail($command->getPlanningPokerId());

        $planningPoker->addPlayer(
            $this->userRepository->findOneByIdOrFail($command->getUserId()),
            PlanningPokerPlayer::draft(
                $command->getPlanningPokerPlayerId(),
                $this->userRepository->findOneByIdOrFail($command->getPlayerUserId())
            )
        );

        $this->eventDispatcher->dispatchFrom($planningPoker);
    }
}
