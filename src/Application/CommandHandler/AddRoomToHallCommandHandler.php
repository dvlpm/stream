<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\CommandHandler;

use Dvlpm\Stream\Application\Command\AddRoomToHallCommand;
use Dvlpm\Stream\Application\Provider\UserProvider;
use Dvlpm\Stream\Domain\Common\Event\EventDispatcherInterface;
use Dvlpm\Stream\Domain\Space\Repository\HallRepositoryInterface;

final class AddRoomToHallCommandHandler
{
    private HallRepositoryInterface $hallRepository;
    private EventDispatcherInterface $eventDispatcher;
    private UserProvider $userProvider;

    public function __construct(
        HallRepositoryInterface $hallRepository,
        EventDispatcherInterface $eventDispatcher,
        UserProvider $userProvider
    ) {
        $this->hallRepository = $hallRepository;
        $this->eventDispatcher = $eventDispatcher;
        $this->userProvider = $userProvider;
    }

    public function __invoke(AddRoomToHallCommand $command): void
    {
        $hall = $this->hallRepository->findOneByIdentifierOrFail($command->getHallIdentifier());

        $hall->addRoom(
            $this->userProvider->provideByIdOrFail($command->getUserId()),
            $command->getPayload()
        );

        $this->eventDispatcher->dispatchFrom($hall);
    }
}
