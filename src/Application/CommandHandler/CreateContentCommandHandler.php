<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\CommandHandler;

use Dvlpm\Stream\Application\Command\CreateContentCommand;
use Dvlpm\Stream\Domain\Common\Event\EventDispatcherInterface;
use Dvlpm\Stream\Domain\Content\Factory\ContentFactory;
use Dvlpm\Stream\Domain\Content\Repository\ContentRepositoryInterface;
use Dvlpm\Stream\Domain\User\Repository\UserRepositoryInterface;

final class CreateContentCommandHandler
{
    private UserRepositoryInterface $userRepository;
    private ContentRepositoryInterface $contentRepository;
    private ContentFactory $contentFactory;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        UserRepositoryInterface $userRepository,
        ContentRepositoryInterface $contentRepository,
        ContentFactory $contentFactory,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->userRepository = $userRepository;
        $this->contentRepository = $contentRepository;
        $this->contentFactory = $contentFactory;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(CreateContentCommand $command): void
    {
        $content = $this->contentFactory->create(
            $this->userRepository->findOneByIdOrFail($command->getUserId()),
            $command->getPayload(),
        );

        $this->contentRepository->save($content);
        $this->eventDispatcher->dispatchFrom($content);
    }
}
