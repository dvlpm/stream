<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\CommandHandler;

use Dvlpm\Stream\Application\Command\CreateHallCommand;
use Dvlpm\Stream\Application\Provider\UserProvider;
use Dvlpm\Stream\Domain\Common\Event\EventDispatcherInterface;
use Dvlpm\Stream\Domain\Space\Model\Hall;
use Dvlpm\Stream\Domain\Space\Repository\HallRepositoryInterface;

final class CreateHallCommandHandler
{
    private HallRepositoryInterface $hallRepository;
    private EventDispatcherInterface $eventDispatcher;
    private UserProvider $userProvider;

    public function __construct(
        HallRepositoryInterface $hallRepository,
        EventDispatcherInterface $eventDispatcher,
        UserProvider $userProvider
    ) {
        $this->hallRepository = $hallRepository;
        $this->eventDispatcher = $eventDispatcher;
        $this->userProvider = $userProvider;
    }

    public function __invoke(CreateHallCommand $command): void
    {
        $hall = Hall::create(
            $this->hallRepository,
            $this->userProvider->provideByIdOrFail($command->getUserId()),
            $command->getPayload(),
        );

        $this->hallRepository->save($hall);
        $this->eventDispatcher->dispatchFrom($hall);
    }
}
