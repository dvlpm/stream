<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\CommandHandler;

use Dvlpm\Stream\Application\Command\RemoveContentCommand;
use Dvlpm\Stream\Domain\Common\Event\EventDispatcherInterface;
use Dvlpm\Stream\Domain\Content\Repository\ContentRepositoryInterface;
use Dvlpm\Stream\Domain\User\Repository\UserRepositoryInterface;

final class RemoveContentCommandHandler
{
    private EventDispatcherInterface $eventDispatcher;
    private UserRepositoryInterface $userRepository;
    private ContentRepositoryInterface $contentRepository;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        UserRepositoryInterface $userRepository,
        ContentRepositoryInterface $contentRepository
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->userRepository = $userRepository;
        $this->contentRepository = $contentRepository;
    }

    public function __invoke(RemoveContentCommand $command): void
    {
        $content = $this->contentRepository->findOneById($command->getContentId());

        if ($content === null) {
            return;
        }

        $content->remove($this->userRepository->findOneByIdOrFail($command->getUserId()));

        $this->contentRepository->remove($content);
        $this->eventDispatcher->dispatchFrom($content);
    }
}
