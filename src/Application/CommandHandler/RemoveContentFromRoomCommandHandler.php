<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\CommandHandler;

use Dvlpm\Stream\Application\Command\RemoveContentFromRoomCommand;
use Dvlpm\Stream\Domain\Common\Event\EventDispatcherInterface;
use Dvlpm\Stream\Domain\Content\Repository\ContentRepositoryInterface;
use Dvlpm\Stream\Domain\Space\Repository\RoomRepositoryInterface;
use Dvlpm\Stream\Domain\User\Repository\UserRepositoryInterface;

final class RemoveContentFromRoomCommandHandler
{
    private EventDispatcherInterface $eventDispatcher;
    private UserRepositoryInterface $userRepository;
    private RoomRepositoryInterface $roomRepository;
    private ContentRepositoryInterface $contentRepository;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        UserRepositoryInterface $userRepository,
        RoomRepositoryInterface $roomRepository,
        ContentRepositoryInterface $contentRepository
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->userRepository = $userRepository;
        $this->roomRepository = $roomRepository;
        $this->contentRepository = $contentRepository;
    }

    public function __invoke(RemoveContentFromRoomCommand $command): void
    {
        $room = $this->roomRepository->findOneByIdOrFail($command->getRoomId());

        $room->removeContent(
            $this->userRepository->findOneByIdOrFail($command->getUserId()),
            $this->contentRepository->findOneByIdOrFail($command->getContentId())
        );

        $this->eventDispatcher->dispatchFrom($room);
    }
}
