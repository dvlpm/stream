<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\CommandHandler;

use Dvlpm\Stream\Application\Command\RemoveHallCommand;
use Dvlpm\Stream\Domain\Common\Event\EventDispatcherInterface;
use Dvlpm\Stream\Domain\Space\Repository\HallRepositoryInterface;
use Dvlpm\Stream\Domain\User\Repository\UserRepositoryInterface;

final class RemoveHallCommandHandler
{
    private EventDispatcherInterface $eventDispatcher;
    private UserRepositoryInterface $userRepository;
    private HallRepositoryInterface $hallRepository;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        UserRepositoryInterface $userRepository,
        HallRepositoryInterface $hallRepository
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->userRepository = $userRepository;
        $this->hallRepository = $hallRepository;
    }

    public function __invoke(RemoveHallCommand $command): void
    {
        $hall = $this->hallRepository->findOneByIdentifier($command->getHallIdentifier());

        if ($hall === null) {
            return;
        }

        $hall->remove($this->userRepository->findOneByIdOrFail($command->getUserId()));

        $this->hallRepository->remove($hall);
        $this->eventDispatcher->dispatchFrom($hall);
    }
}
