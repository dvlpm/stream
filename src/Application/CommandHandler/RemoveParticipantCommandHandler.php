<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\CommandHandler;

use Dvlpm\Stream\Application\Command\RemoveParticipantCommand;
use Dvlpm\Stream\Domain\Common\Event\EventDispatcherInterface;
use Dvlpm\Stream\Domain\Space\Repository\ParticipantRepositoryInterface;
use Dvlpm\Stream\Domain\User\Repository\UserRepositoryInterface;

final class RemoveParticipantCommandHandler
{
    private EventDispatcherInterface $eventDispatcher;
    private UserRepositoryInterface $userRepository;
    private ParticipantRepositoryInterface $participantRepository;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        UserRepositoryInterface $userRepository,
        ParticipantRepositoryInterface $participantRepository
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->userRepository = $userRepository;
        $this->participantRepository = $participantRepository;
    }

    public function __invoke(RemoveParticipantCommand $command): void
    {
        $participant = $this->participantRepository->findOneById($command->getParticipantId());

        if ($participant === null) {
            return;
        }

        $participant->remove($this->userRepository->findOneByIdOrFail($command->getUserId()));

        $this->participantRepository->remove($participant);
        $this->eventDispatcher->dispatchFrom($participant);
    }
}
