<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\CommandHandler;

use Dvlpm\Stream\Application\Command\RemovePlanningPokerStoryCommand;
use Dvlpm\Stream\Domain\Common\Event\EventDispatcherInterface;
use Dvlpm\Stream\Domain\Content\Repository\PlanningPokerStoryRepositoryInterface;
use Dvlpm\Stream\Domain\User\Repository\UserRepositoryInterface;

final class RemovePlanningPokerStoryCommandHandler
{
    private EventDispatcherInterface $eventDispatcher;
    private UserRepositoryInterface $userRepository;
    private PlanningPokerStoryRepositoryInterface $planningPokerStoryRepository;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        UserRepositoryInterface $userRepository,
        PlanningPokerStoryRepositoryInterface $planningPokerStoryRepository
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->userRepository = $userRepository;
        $this->planningPokerStoryRepository = $planningPokerStoryRepository;
    }

    public function __invoke(RemovePlanningPokerStoryCommand $command): void
    {
        $planningPokerStory = $this->planningPokerStoryRepository->findOneById($command->getPlanningPokerStoryId());

        if ($planningPokerStory === null) {
            return;
        }

        $planningPokerStory->remove($this->userRepository->findOneByIdOrFail($command->getUserId()));

        $this->planningPokerStoryRepository->remove($planningPokerStory);
        $this->eventDispatcher->dispatchFrom($planningPokerStory);
    }
}
