<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\CommandHandler;

use Dvlpm\Stream\Application\Command\RemovePlanningPokerStoryVotesCommand;
use Dvlpm\Stream\Domain\Common\Event\EventDispatcherInterface;
use Dvlpm\Stream\Domain\Content\Repository\PlanningPokerStoryRepositoryInterface;
use Dvlpm\Stream\Domain\User\Repository\UserRepositoryInterface;

final class RemovePlanningPokerStoryVotesCommandHandler
{
    private EventDispatcherInterface $eventDispatcher;
    private UserRepositoryInterface $userRepository;
    private PlanningPokerStoryRepositoryInterface $planningPokerStoryRepository;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        UserRepositoryInterface $userRepository,
        PlanningPokerStoryRepositoryInterface $planningPokerStoryRepository
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->userRepository = $userRepository;
        $this->planningPokerStoryRepository = $planningPokerStoryRepository;
    }

    public function __invoke(RemovePlanningPokerStoryVotesCommand $command): void
    {
        $planningPokerStory = $this->planningPokerStoryRepository->findOneByIdOrFail($command->getPlanningPokerStoryId());

        $planningPokerStory->removeVotes($this->userRepository->findOneByIdOrFail($command->getUserId()));

        $this->eventDispatcher->dispatchFrom($planningPokerStory);
    }
}
