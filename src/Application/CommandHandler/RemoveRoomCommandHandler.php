<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\CommandHandler;

use Dvlpm\Stream\Application\Command\RemoveRoomCommand;
use Dvlpm\Stream\Domain\Common\Event\EventDispatcherInterface;
use Dvlpm\Stream\Domain\Space\Repository\RoomRepositoryInterface;
use Dvlpm\Stream\Domain\User\Repository\UserRepositoryInterface;

final class RemoveRoomCommandHandler
{
    private EventDispatcherInterface $eventDispatcher;
    private UserRepositoryInterface $userRepository;
    private RoomRepositoryInterface $roomRepository;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        UserRepositoryInterface $userRepository,
        RoomRepositoryInterface $roomRepository
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->userRepository = $userRepository;
        $this->roomRepository = $roomRepository;
    }

    public function __invoke(RemoveRoomCommand $command): void
    {
        $room = $this->roomRepository->findOneById($command->getRoomId());

        if ($room === null) {
            return;
        }

        $room->remove($this->userRepository->findOneByIdOrFail($command->getUserId()));

        $this->roomRepository->remove($room);
        $this->eventDispatcher->dispatchFrom($room);
    }
}
