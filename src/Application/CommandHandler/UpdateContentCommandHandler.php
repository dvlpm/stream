<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\CommandHandler;

use Dvlpm\Stream\Application\Command\UpdateContentCommand;
use Dvlpm\Stream\Domain\Common\Event\EventDispatcherInterface;
use Dvlpm\Stream\Domain\Content\Applier\UpdateContentApplier;
use Dvlpm\Stream\Domain\Content\Repository\ContentRepositoryInterface;
use Dvlpm\Stream\Domain\User\Repository\UserRepositoryInterface;

final class UpdateContentCommandHandler
{
    private EventDispatcherInterface $eventDispatcher;
    private UserRepositoryInterface $userRepository;
    private UpdateContentApplier $updateContentApplier;
    private ContentRepositoryInterface $contentRepository;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        UserRepositoryInterface $userRepository,
        UpdateContentApplier $updateContentApplier,
        ContentRepositoryInterface $contentRepository
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->userRepository = $userRepository;
        $this->updateContentApplier = $updateContentApplier;
        $this->contentRepository = $contentRepository;
    }

    public function __invoke(UpdateContentCommand $command): void
    {
        $this->updateContentApplier->apply(
            $this->userRepository->findOneByIdOrFail($command->getUserId()),
            $content = $this->contentRepository->findOneByIdOrFail($command->getContentId()),
            $command->getPayload()
        );

        $this->eventDispatcher->dispatchFrom($content);
    }
}
