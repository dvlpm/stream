<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\CommandHandler;

use Dvlpm\Stream\Application\Command\UpdateHallCommand;
use Dvlpm\Stream\Domain\Common\Event\EventDispatcherInterface;
use Dvlpm\Stream\Domain\Space\Repository\HallRepositoryInterface;
use Dvlpm\Stream\Domain\User\Repository\UserRepositoryInterface;

final class UpdateHallCommandHandler
{
    private HallRepositoryInterface $hallRepository;
    private EventDispatcherInterface $eventDispatcher;
    private UserRepositoryInterface $userRepository;

    public function __construct(
        HallRepositoryInterface $hallRepository,
        EventDispatcherInterface $eventDispatcher,
        UserRepositoryInterface $userRepository
    ) {
        $this->hallRepository = $hallRepository;
        $this->eventDispatcher = $eventDispatcher;
        $this->userRepository = $userRepository;
    }

    public function __invoke(UpdateHallCommand $command): void
    {
        $hall = $this->hallRepository->findOneByIdentifierOrFail($command->getHallIdentifier());

        $hall->update(
            $this->userRepository->findOneByIdOrFail($command->getUserId()),
            $command->getPayload()
        );

        $this->eventDispatcher->dispatchFrom($hall);
    }
}
