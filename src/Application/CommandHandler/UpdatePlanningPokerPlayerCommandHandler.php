<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\CommandHandler;

use Dvlpm\Stream\Application\Command\UpdatePlanningPokerPlayerCommand;
use Dvlpm\Stream\Domain\Common\Event\EventDispatcherInterface;
use Dvlpm\Stream\Domain\Content\Repository\PlanningPokerPlayerRepositoryInterface;
use Dvlpm\Stream\Domain\User\Repository\UserRepositoryInterface;

final class UpdatePlanningPokerPlayerCommandHandler
{
    private EventDispatcherInterface $eventDispatcher;
    private UserRepositoryInterface $userRepository;
    private PlanningPokerPlayerRepositoryInterface $planningPokerPlayerRepository;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        UserRepositoryInterface $userRepository,
        PlanningPokerPlayerRepositoryInterface $planningPokerPlayerRepository
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->userRepository = $userRepository;
        $this->planningPokerPlayerRepository = $planningPokerPlayerRepository;
    }

    public function __invoke(UpdatePlanningPokerPlayerCommand $command): void
    {
        $planningPokerPlayer = $this->planningPokerPlayerRepository->findOneByIdOrFail($command->getPlanningPokerPlayerId());

        $planningPokerPlayer->update(
            $this->userRepository->findOneByIdOrFail($command->getUserId()),
            $command->getStatus()
        );

        $this->eventDispatcher->dispatchFrom($planningPokerPlayer);
    }
}
