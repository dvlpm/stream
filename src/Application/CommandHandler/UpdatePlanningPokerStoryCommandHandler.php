<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\CommandHandler;

use Dvlpm\Stream\Application\Command\UpdatePlanningPokerStoryCommand;
use Dvlpm\Stream\Domain\Common\Event\EventDispatcherInterface;
use Dvlpm\Stream\Domain\Content\Repository\PlanningPokerStoryRepositoryInterface;
use Dvlpm\Stream\Domain\User\Repository\UserRepositoryInterface;

final class UpdatePlanningPokerStoryCommandHandler
{
    private EventDispatcherInterface $eventDispatcher;
    private UserRepositoryInterface $userRepository;
    private PlanningPokerStoryRepositoryInterface $planningPokerStoryRepository;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        UserRepositoryInterface $userRepository,
        PlanningPokerStoryRepositoryInterface $planningPokerStoryRepository
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->userRepository = $userRepository;
        $this->planningPokerStoryRepository = $planningPokerStoryRepository;
    }

    public function __invoke(UpdatePlanningPokerStoryCommand $command): void
    {
        $planningPokerStory = $this->planningPokerStoryRepository->findOneByIdOrFail($command->getPlanningPokerStoryId());

        $planningPokerStory->update(
            $this->userRepository->findOneByIdOrFail($command->getUserId()),
            $command->getPayload()
        );

        $this->eventDispatcher->dispatchFrom($planningPokerStory);
    }
}
