<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\CommandHandler;

use Dvlpm\Stream\Application\Command\UpdateUserCommand;
use Dvlpm\Stream\Domain\Common\Event\EventDispatcherInterface;
use Dvlpm\Stream\Domain\User\Exception\UserException;
use Dvlpm\Stream\Domain\User\Repository\UserRepositoryInterface;

final class UpdateUserCommandHandler
{
    private EventDispatcherInterface $eventDispatcher;
    private UserRepositoryInterface $userRepository;

    public function __construct(EventDispatcherInterface $eventDispatcher, UserRepositoryInterface $userRepository)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->userRepository = $userRepository;
    }

    public function __invoke(UpdateUserCommand $command): void
    {
        $existingWithUsername = $this->userRepository->findOneByUsername(
            $username = $command->getPayload()->getUsername()
        );

        if ($existingWithUsername !== null && !$existingWithUsername->hasId($command->getUserId())) {
            throw UserException::notUniqueUsername($username);
        }

        $user = $this->userRepository->findOneByIdOrFail($command->getUserId());

        $user->update($command->getPayload());

        $this->eventDispatcher->dispatchFrom($user);
    }
}
