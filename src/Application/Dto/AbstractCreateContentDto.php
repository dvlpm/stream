<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Dto;

use Dvlpm\Stream\Domain\Content\Model\ContentType;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Serializer\Annotation\DiscriminatorMap;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @DiscriminatorMap(typeProperty="type", mapping={
 *    ContentType::YOUTUBE_STREAM="Dvlpm\Stream\Application\Dto\CreateYouTubeStreamDto",
 *    ContentType::PLANNING_POKER="Dvlpm\Stream\Application\Dto\CreatePlanningPokerDto",
 * })
 */
abstract class AbstractCreateContentDto
{
    /**
     * @var UuidInterface|null
     */
    public ?UuidInterface $id = null;
    /**
     * @Assert\NotNull()
     * @Assert\Choice(callback="provideTypeChoices")
     * @var ContentType|null
     */
    public ?ContentType $type = null;

    public static function provideTypeChoices(): array
    {
        return ContentType::availableTypes();
    }
}
