<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Dto;

use Dvlpm\Stream\Domain\Content\Model\ContentType;
use Symfony\Component\Serializer\Annotation\DiscriminatorMap;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @DiscriminatorMap(typeProperty="type", mapping={
 *    ContentType::YOUTUBE_STREAM="Dvlpm\Stream\Application\Dto\UpdateYouTubeStreamDto",
 *    ContentType::PLANNING_POKER="Dvlpm\Stream\Application\Dto\UpdatePlanningPokerDto",
 * })
 */
abstract class AbstractUpdateContentDto
{
    /**
     * @Assert\NotNull()
     * @Assert\Choice(callback="provideTypeChoices")
     * @var ContentType|null
     */
    public ?ContentType $type = null;

    public static function provideTypeChoices(): array
    {
        return ContentType::availableTypes();
    }
}
