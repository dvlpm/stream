<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Dto;

use Dvlpm\Stream\Domain\Common\Model\Name;
use Dvlpm\Stream\Domain\Space\Model\Identifier;
use Symfony\Component\Validator\Constraints as Assert;

final class CreateHallDto
{
    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="6")
     * @Assert\Regex("/^[A-Za-z0-9]+$/")
     * @var Identifier|null
     */
    public ?Identifier $identifier = null;
    /**
     * @var Name|null
     */
    public ?Name $name = null;
    /**
     * @var string|null
     */
    public ?string $description = null;
}
