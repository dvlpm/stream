<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Dto;

use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

final class CreateParticipantDto
{
    /**
     * @var UuidInterface|null
     */
    public ?UuidInterface $id = null;
    /**
     * @Assert\Valid()
     * @var ParticipantIdentificationDto|null
     */
    public ?ParticipantIdentificationDto $identification = null;
}
