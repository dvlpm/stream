<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Dto;

use Dvlpm\Stream\Domain\Content\Model\PlanningPokerCard;
use Symfony\Component\Validator\Constraints as Assert;

final class CreatePlanningPokerDto extends AbstractCreateContentDto
{
    /**
     * @Assert\Count(min="2")
     * @var PlanningPokerCard[]
     */
    public iterable $cards = [];
}
