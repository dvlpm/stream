<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Dto;

use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

final class CreatePlanningPokerPlayerDto
{
    /**
     * @var UuidInterface|null
     */
    public ?UuidInterface $id = null;
    /**
     * @Assert\Valid()
     * @Assert\NotBlank()
     * @var UserDto|null
     */
    public ?UserDto $user = null;
}
