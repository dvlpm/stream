<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Dto;

use Dvlpm\Stream\Domain\Common\Model\Name;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

final class CreatePlanningPokerStoryDto
{
    /**
     * @var UuidInterface|null
     */
    public ?UuidInterface $id = null;
    /**
     * @Assert\NotBlank()
     * @var Name|null
     */
    public ?Name $name = null;
    /**
     * @var int|null
     */
    public ?int $order = 0;
    /**
     * @var string|null
     */
    public ?string $description = null;
    /**
     * @var string|null
     */
    public ?string $link = null;
}
