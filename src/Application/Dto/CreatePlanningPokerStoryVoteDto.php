<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Dto;

use Dvlpm\Stream\Domain\Content\Model\PlanningPokerCard;
use Symfony\Component\Validator\Constraints as Assert;

final class CreatePlanningPokerStoryVoteDto
{
    /**
     * @Assert\NotBlank()
     * @var PlanningPokerCard|null
     */
    public ?PlanningPokerCard $card = null;
}
