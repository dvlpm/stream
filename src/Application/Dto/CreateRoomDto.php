<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Dto;

use Dvlpm\Stream\Domain\Common\Model\Name;
use Dvlpm\Stream\Domain\Space\Model\AccessType;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

final class CreateRoomDto
{
    /**
     * @var UuidInterface|null
     */
    public ?UuidInterface $id = null;
    /**
     * @Assert\NotBlank()
     * @var Name|null
     */
    public ?Name $name = null;
    /**
     * @Assert\NotBlank()
     * @Assert\Choice(callback="provideAccessTypeChoices")
     * @var AccessType|null
     */
    public ?AccessType $accessType = null;
    /**
     * @var string|null
     */
    public ?string $description = null;

    public static function provideAccessTypeChoices(): array
    {
        return [
            AccessType::AUTHORIZED(),
            AccessType::ANONYMOUS(),
            AccessType::LISTED(),
        ];
    }
}
