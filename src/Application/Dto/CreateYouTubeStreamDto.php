<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Dto;

use Symfony\Component\Validator\Constraints as Assert;

final class CreateYouTubeStreamDto extends AbstractCreateContentDto
{
    /**
     * @Assert\NotBlank()
     * @var string|null
     */
    public ?string $link = null;
}
