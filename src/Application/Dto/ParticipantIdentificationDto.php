<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Dto;

use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

final class ParticipantIdentificationDto
{
    /**
     * @var UuidInterface|null
     */
    public ?UuidInterface $userId = null;
    /**
     * @Assert\Email()
     * @var string|null
     */
    public ?string $email = null;

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     */
    public function validate(ExecutionContextInterface $context): void
    {
        if ($this->userId === null
            && $this->email === null) {
            $context->buildViolation('Either user uuid or email must be provided.')
                ->atPath('email')
                ->addViolation();

            $context->buildViolation('Either user uuid or email must be provided.')
                ->atPath('userId')
                ->addViolation();
        }
    }
}
