<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Dto;

use Dvlpm\Stream\Domain\Common\Model\Name;

final class UpdateHallDto
{
    /**
     * @var Name|null
     */
    public ?Name $name = null;
    /**
     * @var string|null
     */
    public ?string $description = null;
}
