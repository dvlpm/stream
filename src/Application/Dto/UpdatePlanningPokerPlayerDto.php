<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Dto;

use Dvlpm\Stream\Domain\Content\Model\PlanningPokerPlayerStatus;
use Symfony\Component\Validator\Constraints as Assert;

final class UpdatePlanningPokerPlayerDto
{
    /**
     * @Assert\NotBlank()
     * @var PlanningPokerPlayerStatus|null
     */
    public ?PlanningPokerPlayerStatus $status = null;
}
