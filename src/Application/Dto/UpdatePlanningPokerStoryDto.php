<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Dto;

use Dvlpm\Stream\Domain\Common\Model\Name;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerStoryStatus;
use Symfony\Component\Validator\Constraints as Assert;

final class UpdatePlanningPokerStoryDto
{
    /**
     * @Assert\NotBlank()
     * @var PlanningPokerStoryStatus|null
     */
    public ?PlanningPokerStoryStatus $status = null;
    /**
     * @Assert\NotBlank()
     * @var Name|null
     */
    public ?Name $name = null;
    /**
     * @Assert\NotBlank()
     * @var string|null
     */
    public ?string $score = null;
    /**
     * @var int|null
     */
    public ?int $order = 0;
    /**
     * @var string|null
     */
    public ?string $description = null;
    /**
     * @var string|null
     */
    public ?string $link = null;
}
