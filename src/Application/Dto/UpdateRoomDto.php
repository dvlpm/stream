<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Dto;

use Dvlpm\Stream\Domain\Common\Model\Name;
use Dvlpm\Stream\Domain\Space\Model\AccessType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

final class UpdateRoomDto
{
    /**
     * @Assert\NotBlank()
     * @var Name|null
     */
    public ?Name $name = null;
    /**
     * @Assert\NotBlank()
     * @Assert\Choice(callback="provideAccessTypeChoices")
     * @var AccessType|null
     */
    public ?AccessType $accessType = null;
    /**
     * @var string|null
     */
    public ?string $description = null;

    public static function provideAccessTypeChoices(): array
    {
        return AccessType::availableTypes();
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     */
    public function validate(ExecutionContextInterface $context): void
    {
        if (empty($this->participants)
            && $this->accessType === AccessType::LISTED()
        ) {
            $context->buildViolation('You should add participants in case of user restricted access.')
                ->atPath('participants')
                ->addViolation();
        }
    }
}
