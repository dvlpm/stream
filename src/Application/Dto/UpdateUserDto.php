<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Dto;

use Dvlpm\Stream\Domain\Common\Model\Meta;
use Dvlpm\Stream\Domain\Common\Model\Name;
use Dvlpm\Stream\Domain\User\Model\Username;
use Symfony\Component\Validator\Constraints as Assert;

final class UpdateUserDto
{
    /**
     * @var Meta|null
     */
    public ?Meta $meta;
    /**
     * @Assert\Regex("/^[A-Za-z0-9]+$/")
     * @var Username|null
     */
    public ?Username $username;
    /**
     * @var Name|null
     */
    public ?Name $name;
}
