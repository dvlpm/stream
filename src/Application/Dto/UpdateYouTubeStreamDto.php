<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Dto;

use Symfony\Component\Validator\Constraints as Assert;

final class UpdateYouTubeStreamDto extends AbstractUpdateContentDto
{
    /**
     * @Assert\NotBlank()
     * @var string|null
     */
    public ?string $link = null;
}
