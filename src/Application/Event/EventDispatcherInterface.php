<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Event;

interface EventDispatcherInterface extends \Dvlpm\Stream\Domain\Common\Event\EventDispatcherInterface
{
    public function flush(): void;
}

