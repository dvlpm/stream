<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Event;

use Dvlpm\Stream\Domain\Common\Event\DomainEventInterface;

interface EventListenerInterface
{
    public function handle(DomainEventInterface $event): void;
}
