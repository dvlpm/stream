<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Event;

use Dvlpm\Stream\Domain\Common\Event\DomainEventInterface;

final class MultipleEventListener implements EventListenerInterface
{
    /** @var EventListenerInterface[] */
    private array $listeners;

    public function __construct(EventListenerInterface ...$listeners)
    {
        $this->listeners = $listeners;
    }

    public function handle(DomainEventInterface $event): void
    {
        foreach ($this->listeners as $listener) {
            $listener->handle($event);
        }
    }
}
