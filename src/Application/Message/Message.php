<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Message;

final class Message
{
    private MessageType $type;
    /** @var string|array */
    private $content;

    private function __construct(MessageType $type, $content)
    {
        $this->type = $type;
        $this->content = $content;
    }

    public static function notification(string $message): self
    {
        return new self(MessageType::NOTIFICATION(), $message);
    }

    public static function warning(string $warning): self
    {
        return new self(MessageType::WARNING(), $warning);
    }

    public static function violation(string $property, string $message): self
    {
        return new self(MessageType::VIOLATION(), [$property => $message]);
    }

    public static function error(string $error): self
    {
        return new self(MessageType::ERROR(), $error);
    }
}
