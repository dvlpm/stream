<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Message;

final class MessageBag
{
    /** @var Message[] */
    private array $messages;

    private function __construct(Message ...$messages)
    {
        $this->messages = $messages;
    }

    public static function withMessages(Message ...$messages): self
    {
        return new self(...$messages);
    }
}
