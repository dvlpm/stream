<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Message;

use Dvlpm\Stream\Domain\Common\Model\Enum;

/**
 * @method static static NOTIFICATION()
 * @method static static VIOLATION()
 * @method static static WARNING()
 * @method static static ERROR()
 */
final class MessageType extends Enum
{
    private const NOTIFICATION = 'NOTIFICATION';
    private const VIOLATION = 'VIOLATION';
    private const WARNING = 'WARNING';
    private const ERROR = 'ERROR';
}
