<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\PayloadFactory;

use Dvlpm\Stream\Application\Dto\CreateParticipantDto;
use Dvlpm\Stream\Domain\Space\Payload\AddParticipantToRoomPayload;

final class AddParticipantsToRoomPayloadFactory
{
    private CreateParticipantPayloadFactory $participantPayloadFactory;

    public function __construct(CreateParticipantPayloadFactory $participantPayloadFactory)
    {
        $this->participantPayloadFactory = $participantPayloadFactory;
    }

    public function createFromDto(CreateParticipantDto $dto): AddParticipantToRoomPayload
    {
        return new AddParticipantToRoomPayload(
            $this->participantPayloadFactory->createFromDto($dto)
        );
    }
}
