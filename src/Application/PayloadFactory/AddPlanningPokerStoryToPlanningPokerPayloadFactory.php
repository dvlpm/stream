<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\PayloadFactory;

use Dvlpm\Stream\Application\Dto\CreatePlanningPokerStoryDto;
use Dvlpm\Stream\Domain\Content\Payload\AddPlanningPokerStoryToPlanningPokerPayload;

final class AddPlanningPokerStoryToPlanningPokerPayloadFactory
{
    private CreatePlanningPokerStoryPayloadFactory $createPlanningPokerStoryPayloadFactory;

    public function __construct(CreatePlanningPokerStoryPayloadFactory $createPlanningPokerStoryPayloadFactory)
    {
        $this->createPlanningPokerStoryPayloadFactory = $createPlanningPokerStoryPayloadFactory;
    }

    public function createFromDto(CreatePlanningPokerStoryDto $dto): AddPlanningPokerStoryToPlanningPokerPayload
    {
        return new AddPlanningPokerStoryToPlanningPokerPayload(
            $this->createPlanningPokerStoryPayloadFactory->createFromDto($dto)
        );
    }
}
