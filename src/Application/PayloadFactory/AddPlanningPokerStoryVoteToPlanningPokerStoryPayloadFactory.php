<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\PayloadFactory;

use Dvlpm\Stream\Application\Dto\CreatePlanningPokerStoryVoteDto;
use Dvlpm\Stream\Domain\Content\Payload\AddPlanningPokerStoryVoteToPlanningPokerStoryPayload;

final class AddPlanningPokerStoryVoteToPlanningPokerStoryPayloadFactory
{
    private CreatePlanningPokerStoryVotePayloadFactory $createPlanningPokerStoryVotePayloadFactory;

    public function __construct(CreatePlanningPokerStoryVotePayloadFactory $createPlanningPokerStoryVotePayloadFactory)
    {
        $this->createPlanningPokerStoryVotePayloadFactory = $createPlanningPokerStoryVotePayloadFactory;
    }

    public function createFromDto(CreatePlanningPokerStoryVoteDto $dto): AddPlanningPokerStoryVoteToPlanningPokerStoryPayload
    {
        return new AddPlanningPokerStoryVoteToPlanningPokerStoryPayload(
            $this->createPlanningPokerStoryVotePayloadFactory->createFromDto($dto)
        );
    }
}
