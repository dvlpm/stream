<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\PayloadFactory;

use Dvlpm\Stream\Application\Dto\CreateRoomDto;
use Dvlpm\Stream\Domain\Space\Payload\AddRoomToHallPayload;

final class AddRoomToHallPayloadFactory
{
    private CreateRoomPayloadFactory $roomPayloadFactory;

    public function __construct(CreateRoomPayloadFactory $roomPayloadFactory)
    {
        $this->roomPayloadFactory = $roomPayloadFactory;
    }

    public function createFromDto(CreateRoomDto $dto): AddRoomToHallPayload
    {
        return new AddRoomToHallPayload(
            $this->roomPayloadFactory->createFromDto($dto)
        );
    }
}
