<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\PayloadFactory;

use Dvlpm\Stream\Application\Dto\AbstractCreateContentDto;
use Dvlpm\Stream\Application\Dto\CreatePlanningPokerDto;
use Dvlpm\Stream\Application\Dto\CreateYouTubeStreamDto;
use Dvlpm\Stream\Application\PayloadFactory\Exception\ContentPayloadFactoryException;
use Dvlpm\Stream\Domain\Content\Payload\CreateContentPayloadInterface;

final class CreateContentPayloadFactory
{
    private CreateYouTubeStreamPayloadFactory $createYouTubeStreamPayloadFactory;
    private CreatePlanningPokerPayloadFactory $createPlanningPokerPayloadFactory;

    public function __construct(
        CreateYouTubeStreamPayloadFactory $createYouTubeStreamPayloadFactory,
        CreatePlanningPokerPayloadFactory $createPlanningPokerPayloadFactory
    ) {
        $this->createYouTubeStreamPayloadFactory = $createYouTubeStreamPayloadFactory;
        $this->createPlanningPokerPayloadFactory = $createPlanningPokerPayloadFactory;
    }

    public function createFromDto(AbstractCreateContentDto $dto): CreateContentPayloadInterface
    {
        if ($dto instanceof CreateYouTubeStreamDto) {
            return $this->createYouTubeStreamPayloadFactory->createFromDto($dto);
        }

        if ($dto instanceof CreatePlanningPokerDto) {
            return $this->createPlanningPokerPayloadFactory->createFromDto($dto);
        }

        throw ContentPayloadFactoryException::invalidDto();
    }
}
