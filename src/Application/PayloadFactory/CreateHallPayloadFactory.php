<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\PayloadFactory;

use Dvlpm\Stream\Application\Dto\CreateHallDto;
use Dvlpm\Stream\Domain\Space\Payload\CreateHallPayload;

final class CreateHallPayloadFactory
{
    public function createFromDto(CreateHallDto $dto): CreateHallPayload
    {
        return new CreateHallPayload(
            $dto->identifier,
            $dto->name,
            $dto->description,
        );
    }
}
