<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\PayloadFactory;

use Dvlpm\Stream\Application\Dto\CreateParticipantDto;
use Dvlpm\Stream\Application\PayloadFactory\Exception\ParticipantPayloadFactoryException;
use Dvlpm\Stream\Domain\Space\Model\ParticipantIdentification;
use Dvlpm\Stream\Domain\Space\Payload\CreateParticipantPayload;

final class CreateParticipantPayloadFactory
{
    public function createFromDto(CreateParticipantDto $dto): CreateParticipantPayload
    {
        $identification = null;

        if (($dto->identification->userId ?? null) !== null) {
            $identification = ParticipantIdentification::fromUserId($dto->identification->userId);
        }

        if (($dto->identification->email ?? null) !== null) {
            $identification = ParticipantIdentification::fromEmail($dto->identification->email);
        }

        if ($identification === null) {
            throw ParticipantPayloadFactoryException::invalidDto();
        }

        return new CreateParticipantPayload($identification, $dto->id);
    }
}
