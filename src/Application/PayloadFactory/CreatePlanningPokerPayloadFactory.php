<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\PayloadFactory;

use Dvlpm\Stream\Application\Dto\CreatePlanningPokerDto;
use Dvlpm\Stream\Domain\Content\Payload\CreatePlanningPokerPayload;

final class CreatePlanningPokerPayloadFactory
{
    public function createFromDto(CreatePlanningPokerDto $dto): CreatePlanningPokerPayload
    {
        return new CreatePlanningPokerPayload(
            $dto->cards,
            $dto->id
        );
    }
}
