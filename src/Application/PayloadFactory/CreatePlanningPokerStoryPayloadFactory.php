<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\PayloadFactory;

use Dvlpm\Stream\Application\Dto\CreatePlanningPokerStoryDto;
use Dvlpm\Stream\Domain\Content\Payload\CreatePlanningPokerStoryPayload;

final class CreatePlanningPokerStoryPayloadFactory
{
    public function createFromDto(CreatePlanningPokerStoryDto $dto): CreatePlanningPokerStoryPayload
    {
        return new CreatePlanningPokerStoryPayload(
            $dto->name,
            $dto->order,
            $dto->link,
            $dto->description,
            $dto->id
        );
    }
}
