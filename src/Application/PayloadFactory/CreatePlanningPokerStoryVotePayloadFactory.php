<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\PayloadFactory;

use Dvlpm\Stream\Application\Dto\CreatePlanningPokerStoryVoteDto;
use Dvlpm\Stream\Domain\Content\Payload\CreatePlanningPokerStoryVotePayload;

final class CreatePlanningPokerStoryVotePayloadFactory
{
    public function createFromDto(CreatePlanningPokerStoryVoteDto $dto): CreatePlanningPokerStoryVotePayload
    {
        return new CreatePlanningPokerStoryVotePayload($dto->card);
    }
}
