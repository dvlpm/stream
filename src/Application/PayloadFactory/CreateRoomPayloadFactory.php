<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\PayloadFactory;

use Dvlpm\Stream\Application\Dto\CreateRoomDto;
use Dvlpm\Stream\Domain\Space\Payload\CreateRoomPayload;

final class CreateRoomPayloadFactory
{
    public function createFromDto(CreateRoomDto $dto): CreateRoomPayload
    {
        return new CreateRoomPayload(
            $dto->name,
            $dto->accessType,
            $dto->description,
            $dto->id,
        );
    }
}
