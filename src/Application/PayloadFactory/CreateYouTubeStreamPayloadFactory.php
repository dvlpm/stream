<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\PayloadFactory;

use Dvlpm\Stream\Application\Dto\CreateYouTubeStreamDto;
use Dvlpm\Stream\Domain\Content\Payload\CreateYouTubeStreamPayload;

final class CreateYouTubeStreamPayloadFactory
{
    public function createFromDto(CreateYouTubeStreamDto $dto): CreateYouTubeStreamPayload
    {
        return new CreateYouTubeStreamPayload(
            $dto->link,
            $dto->id
        );
    }
}
