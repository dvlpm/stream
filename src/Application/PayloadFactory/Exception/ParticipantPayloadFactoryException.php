<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\PayloadFactory\Exception;

use Exception;

final class ParticipantPayloadFactoryException extends Exception
{
    public static function invalidDto(): self
    {
        return new static('Invalid participant dto provided');
    }
}
