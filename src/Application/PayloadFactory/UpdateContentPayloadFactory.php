<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\PayloadFactory;

use Dvlpm\Stream\Application\Dto\AbstractUpdateContentDto;
use Dvlpm\Stream\Application\Dto\UpdatePlanningPokerDto;
use Dvlpm\Stream\Application\Dto\UpdateYouTubeStreamDto;
use Dvlpm\Stream\Application\PayloadFactory\Exception\ContentPayloadFactoryException;
use Dvlpm\Stream\Domain\Content\Payload\UpdateContentPayloadInterface;

final class UpdateContentPayloadFactory
{
    private UpdateYouTubeStreamPayloadFactory $updateYouTubeStreamPayloadFactory;
    private UpdatePlanningPokerPayloadFactory $updatePlanningPokerPayloadFactory;

    public function __construct(
        UpdateYouTubeStreamPayloadFactory $updateYouTubeStreamPayloadFactory,
        UpdatePlanningPokerPayloadFactory $updatePlanningPokerPayloadFactory
    ) {
        $this->updateYouTubeStreamPayloadFactory = $updateYouTubeStreamPayloadFactory;
        $this->updatePlanningPokerPayloadFactory = $updatePlanningPokerPayloadFactory;
    }

    public function createFromDto(AbstractUpdateContentDto $dto): UpdateContentPayloadInterface
    {
        if ($dto instanceof UpdateYouTubeStreamDto) {
            return $this->updateYouTubeStreamPayloadFactory->createFromDto($dto);
        }

        if ($dto instanceof UpdatePlanningPokerDto) {
            return $this->updatePlanningPokerPayloadFactory->createFromDto($dto);
        }

        throw ContentPayloadFactoryException::invalidDto();
    }
}
