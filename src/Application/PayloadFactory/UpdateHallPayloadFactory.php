<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\PayloadFactory;

use Dvlpm\Stream\Application\Dto\UpdateHallDto;
use Dvlpm\Stream\Domain\Space\Payload\UpdateHallPayload;

final class UpdateHallPayloadFactory
{
    public function createFromDto(UpdateHallDto $dto): UpdateHallPayload
    {
        return new UpdateHallPayload(
            $dto->name,
            $dto->description,
        );
    }
}
