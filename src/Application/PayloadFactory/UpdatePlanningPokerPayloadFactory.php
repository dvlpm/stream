<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\PayloadFactory;

use Dvlpm\Stream\Application\Dto\UpdatePlanningPokerDto;
use Dvlpm\Stream\Domain\Content\Payload\UpdatePlanningPokerPayload;

final class UpdatePlanningPokerPayloadFactory
{
    public function createFromDto(UpdatePlanningPokerDto $dto): UpdatePlanningPokerPayload
    {
        return new UpdatePlanningPokerPayload($dto->cards);
    }
}
