<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\PayloadFactory;

use Dvlpm\Stream\Application\Dto\UpdatePlanningPokerStoryDto;
use Dvlpm\Stream\Domain\Content\Payload\UpdatePlanningPokerStoryPayload;

final class UpdatePlanningPokerStoryPayloadFactory
{
    public function createFromDto(UpdatePlanningPokerStoryDto $dto): UpdatePlanningPokerStoryPayload
    {
        return new UpdatePlanningPokerStoryPayload(
            $dto->name,
            $dto->status,
            $dto->order,
            $dto->score,
            $dto->link,
            $dto->description,
        );
    }
}
