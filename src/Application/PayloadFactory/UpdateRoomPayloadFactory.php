<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\PayloadFactory;

use Dvlpm\Stream\Application\Dto\UpdateRoomDto;
use Dvlpm\Stream\Domain\Space\Payload\UpdateRoomPayload;

final class UpdateRoomPayloadFactory
{
    public function createFromDto(UpdateRoomDto $dto): UpdateRoomPayload
    {
        return new UpdateRoomPayload(
            $dto->name,
            $dto->accessType,
            $dto->description,
        );
    }
}
