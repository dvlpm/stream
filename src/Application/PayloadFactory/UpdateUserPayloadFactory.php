<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\PayloadFactory;

use Dvlpm\Stream\Application\Dto\UpdateUserDto;
use Dvlpm\Stream\Domain\Common\Model\Meta;
use Dvlpm\Stream\Domain\User\Payload\UpdateUserPayload;

final class UpdateUserPayloadFactory
{
    public function createFromDto(UpdateUserDto $dto): UpdateUserPayload
    {
        return new UpdateUserPayload(
            $dto->meta ?? Meta::empty(),
            $dto->name,
            $dto->username
        );
    }
}
