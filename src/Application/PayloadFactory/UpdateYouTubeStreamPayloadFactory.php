<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\PayloadFactory;

use Dvlpm\Stream\Application\Dto\UpdateYouTubeStreamDto;
use Dvlpm\Stream\Domain\Content\Payload\UpdateYouTubeStreamPayload;

final class UpdateYouTubeStreamPayloadFactory
{
    public function createFromDto(UpdateYouTubeStreamDto $dto): UpdateYouTubeStreamPayload
    {
        return new UpdateYouTubeStreamPayload($dto->link);
    }
}
