<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\ProcessManager\CommandEmitter;

use Dvlpm\Stream\Domain\Common\Event\DomainEventInterface;
use Generator;

abstract class AbstractCommandEmitter implements CommandEmitterInterface
{
    abstract protected function getSupportedClassName(): string;

    final public function supportsEvent(DomainEventInterface $event): bool
    {
        $supportedClass = $this->getSupportedClassName();

        return $event instanceof $supportedClass;
    }

    abstract protected function emitForEvent(DomainEventInterface $event): Generator;

    final public function emit(DomainEventInterface $event): Generator
    {
        if (!$this->supportsEvent($event)) {
            return;
        }

        yield from $this->emitForEvent($event);
    }
}
