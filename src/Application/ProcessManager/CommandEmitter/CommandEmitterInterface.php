<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\ProcessManager\CommandEmitter;

use Dvlpm\Stream\Domain\Common\Event\DomainEventInterface;
use Generator;

interface CommandEmitterInterface
{
    public function emit(DomainEventInterface $event): Generator;
}
