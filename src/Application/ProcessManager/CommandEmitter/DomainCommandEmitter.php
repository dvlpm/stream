<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\ProcessManager\CommandEmitter;

use Dvlpm\Stream\Domain\Common\Event\DomainEventInterface;
use Generator;

final class DomainCommandEmitter extends AbstractCommandEmitter
{
    protected function getSupportedClassName(): string
    {
        return DomainEventInterface::class;
    }

    protected function emitForEvent(DomainEventInterface $event): Generator
    {
        yield from [];
    }
}
