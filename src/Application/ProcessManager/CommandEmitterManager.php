<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\ProcessManager;

use Dvlpm\Stream\Application\ProcessManager\CommandEmitter\CommandEmitterInterface;
use Dvlpm\Stream\Domain\Common\Event\DomainEventInterface;
use Generator;

final class CommandEmitterManager
{
    /** @var CommandEmitterInterface[] */
    private $emitters;

    /** @param CommandEmitterInterface[] $emitters */
    public function __construct(iterable $emitters)
    {
        $this->emitters = $emitters;
    }

    public function emitForEvent(DomainEventInterface $event): Generator
    {
        foreach ($this->emitters as $emitter) {
            yield from $emitter->emit($event);
        }
    }
}
