<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\ProcessManager;

use Dvlpm\Stream\Application\CommandBus\CommandBusInterface;
use Dvlpm\Stream\Application\Event\EventListenerInterface;
use Dvlpm\Stream\Domain\Common\Event\DomainEventInterface;

final class ProcessManager
{
    private CommandBusInterface $commandBus;
    private EventListenerInterface $listener;
    private CommandEmitterManager $commandEmitterManager;

    public function __construct(
        CommandBusInterface $commandBus,
        EventListenerInterface $listener,
        CommandEmitterManager $commandEmitterManager
    ) {
        $this->commandBus = $commandBus;
        $this->listener = $listener;
        $this->commandEmitterManager = $commandEmitterManager;
    }

    public function handleEvents(DomainEventInterface ...$events): void
    {
        foreach ($events as $event) {
            $this->handleEvent($event);
        }
    }

    private function handleEvent(DomainEventInterface $event): void
    {
        foreach ($this->commandEmitterManager->emitForEvent($event) as $command) {
            $this->commandBus->handle($command);
        }

        $this->listener->handle($event);
    }
}
