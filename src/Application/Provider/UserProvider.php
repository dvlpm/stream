<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Provider;

use Dvlpm\Stream\Domain\Common\Exception\UserRequiredException;
use Dvlpm\Stream\Domain\User\Model\User;
use Dvlpm\Stream\Domain\User\Repository\UserRepositoryInterface;
use Ramsey\Uuid\UuidInterface;

final class UserProvider
{
    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function provideByIdOrFail(UuidInterface $id): User
    {
        $user = $this->userRepository->findOneById($id);

        if ($user === null) {
            throw UserRequiredException::create();
        }

        return $user;
    }
}
