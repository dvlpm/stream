<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Dvlpm\Stream\Domain\Content\Exception\UndefinedContentException;
use Dvlpm\Stream\Domain\Content\Model\AbstractContent;
use Dvlpm\Stream\Domain\Content\Repository\ContentRepositoryInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * @method AbstractContent|null find($id, $lockMode = null, $lockVersion = null)
 * @method AbstractContent|null findOneBy(array $criteria, array $orderBy = null)
 * @method AbstractContent[]    findAll()
 * @method AbstractContent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContentRepository extends ServiceEntityRepository implements ContentRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AbstractContent::class);
    }

    public function findOneById(UuidInterface $id): ?AbstractContent
    {
        return $this->find($id);
    }

    public function findOneByIdOrFail(UuidInterface $id): AbstractContent
    {
        $content = $this->find($id);

        if ($content === null) {
            throw UndefinedContentException::createWithId($id);
        }

        return $content;
    }

    public function save(AbstractContent $content): void
    {
        $this->getEntityManager()->persist($content);
    }

    public function remove(AbstractContent $content): void
    {
        $this->getEntityManager()->remove($content);
    }
}
