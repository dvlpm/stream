<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use Dvlpm\Stream\Domain\Space\Exception\UndefinedHallException;
use Dvlpm\Stream\Domain\Space\Model\Hall;
use Dvlpm\Stream\Domain\Space\Model\HallCollection;
use Dvlpm\Stream\Domain\Space\Model\Host;
use Dvlpm\Stream\Domain\Space\Model\Identifier;
use Dvlpm\Stream\Domain\Space\Repository\HallCriteria;
use Dvlpm\Stream\Domain\Space\Repository\HallRepositoryInterface;
use Dvlpm\Stream\Domain\User\Model\User;

/**
 * @method Hall|null find($id, $lockMode = null, $lockVersion = null)
 * @method Hall|null findOneBy(array $criteria, array $orderBy = null)
 * @method Hall[]    findAll()
 * @method Hall[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HallRepository extends ServiceEntityRepository implements HallRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Hall::class);
    }

    public function findAllByCriteria(HallCriteria $hallCriteria): HallCollection
    {
        return new HallCollection(...$this->createFindAllByCriteriaQuery($hallCriteria)->getResult());
    }

    private function createFindAllByCriteriaQuery(HallCriteria $hallCriteria): Query
    {
        $qb = $this->createQueryBuilder('hall');

        if (($hostCriteria = $hallCriteria->getHostCriteria()) === null) {
            return $qb->getQuery();
        }

        if (!empty($hostCriteria->getRoles()) || $hostCriteria->getUserId() !== null) {
            $qb->join(Host::class, 'host', Join::WITH, 'host.hall = hall')
                ->join(User::class, 'user', Join::WITH, 'host.user = user');
        }

        if ($hostCriteria->getUserId() !== null) {
            $qb->where('user.id = :hostUserId')
                ->setParameter('hostUserId', $hostCriteria->getUserId());
        }

        if (!empty($hostCriteria->getRoles())) {
            $qb->andWhere($qb->expr()->in('host.role', $hostCriteria->getRoles()));
        }

        return $qb->getQuery();
    }

    public function findOneByIdentifierOrFail(Identifier $identifier): Hall
    {
        $hall = $this->findOneByIdentifier($identifier);

        if ($hall === null) {
            throw UndefinedHallException::createWithIdentifier($identifier);
        }

        return $hall;
    }

    public function findOneByIdentifier(Identifier $identifier): ?Hall
    {
        return $this->findOneBy(['identifier' => $identifier]);
    }

    public function save(Hall $hall): void
    {
        $this->getEntityManager()->persist($hall);
    }

    public function remove(Hall $hall): void
    {
        $this->getEntityManager()->remove($hall);
    }
}
