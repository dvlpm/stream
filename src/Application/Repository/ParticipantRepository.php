<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Dvlpm\Stream\Domain\Space\Model\Participant;
use Dvlpm\Stream\Domain\Space\Repository\ParticipantRepositoryInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * @method Participant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Participant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Participant[]    findAll()
 * @method Participant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParticipantRepository extends ServiceEntityRepository implements ParticipantRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Participant::class);
    }

    public function findOneById(UuidInterface $id): ?Participant
    {
        return $this->find($id);
    }

    public function remove(Participant $participant): void
    {
        $this->getEntityManager()->remove($participant);
    }
}
