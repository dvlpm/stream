<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Dvlpm\Stream\Domain\Content\Exception\UndefinedContentException;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerPlayer;
use Dvlpm\Stream\Domain\Content\Repository\PlanningPokerPlayerRepositoryInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * @method PlanningPokerPlayer|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlanningPokerPlayer|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlanningPokerPlayer[]    findAll()
 * @method PlanningPokerPlayer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlanningPokerPlayerRepository extends ServiceEntityRepository implements PlanningPokerPlayerRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlanningPokerPlayer::class);
    }

    public function findOneById(UuidInterface $id): ?PlanningPokerPlayer
    {
        return $this->find($id);
    }

    public function findOneByIdOrFail(UuidInterface $id): PlanningPokerPlayer
    {
        $content = $this->find($id);

        if ($content === null) {
            throw UndefinedContentException::createWithId($id);
        }

        return $content;
    }

    public function remove(PlanningPokerPlayer $planningPokerPlayer): void
    {
        $this->getEntityManager()->remove($planningPokerPlayer);
    }
}
