<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Dvlpm\Stream\Domain\Content\Exception\UndefinedContentException;
use Dvlpm\Stream\Domain\Content\Model\PlanningPoker;
use Dvlpm\Stream\Domain\Content\Repository\PlanningPokerRepositoryInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * @method PlanningPoker|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlanningPoker|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlanningPoker[]    findAll()
 * @method PlanningPoker[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlanningPokerRepository extends ServiceEntityRepository implements PlanningPokerRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlanningPoker::class);
    }

    public function findOneByIdOrFail(UuidInterface $id): PlanningPoker
    {
        $content = $this->find($id);

        if ($content === null) {
            throw UndefinedContentException::createWithId($id);
        }

        return $content;
    }
}
