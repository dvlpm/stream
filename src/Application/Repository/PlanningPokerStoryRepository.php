<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Dvlpm\Stream\Domain\Content\Exception\UndefinedContentException;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerStory;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerStoryCollection;
use Dvlpm\Stream\Domain\Content\Repository\PlanningPokerStoryCriteria;
use Dvlpm\Stream\Domain\Content\Repository\PlanningPokerStoryRepositoryInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * @method PlanningPokerStory|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlanningPokerStory|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlanningPokerStory[]    findAll()
 * @method PlanningPokerStory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlanningPokerStoryRepository extends ServiceEntityRepository implements PlanningPokerStoryRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlanningPokerStory::class);
    }

    public function findAllByCriteria(PlanningPokerStoryCriteria $planningPokerStoryCriteria): PlanningPokerStoryCollection
    {
        $criteria = [
            'poker' => [
                'id' => $planningPokerStoryCriteria->getPlanningPokerId(),
            ],
        ];

        if ($planningPokerStoryCriteria->getStatus() !== null) {
            $criteria['status'] = $planningPokerStoryCriteria->getStatus();
        }

        $stories = $this->findBy(
            $criteria,
            ['createdAt' => 'DESC'],
            $planningPokerStoryCriteria->getLimit(),
            $planningPokerStoryCriteria->getOffset()
        );

        return new PlanningPokerStoryCollection(...$stories);
    }

    public function findOneById(UuidInterface $id): ?PlanningPokerStory
    {
        return $this->find($id);
    }

    public function findOneByIdOrFail(UuidInterface $id): PlanningPokerStory
    {
        $content = $this->find($id);

        if ($content === null) {
            throw UndefinedContentException::createWithId($id);
        }

        return $content;
    }

    public function remove(PlanningPokerStory $planningPokerStory): void
    {
        $this->getEntityManager()->remove($planningPokerStory);
    }
}
