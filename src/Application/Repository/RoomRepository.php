<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Dvlpm\Stream\Domain\Space\Exception\UndefinedRoomException;
use Dvlpm\Stream\Domain\Space\Model\Room;
use Dvlpm\Stream\Domain\Space\Repository\RoomRepositoryInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * @method Room|null find($id, $lockMode = null, $lockVersion = null)
 * @method Room|null findOneBy(array $criteria, array $orderBy = null)
 * @method Room[]    findAll()
 * @method Room[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoomRepository extends ServiceEntityRepository implements RoomRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Room::class);
    }

    public function findOneByIdOrFail(UuidInterface $id): Room
    {
        $room = $this->findOneById($id);

        if ($room === null) {
            throw UndefinedRoomException::createWithId($id);
        }

        return $room;
    }

    public function findOneById(UuidInterface $id): ?Room
    {
        return $this->find($id);
    }

    public function save(Room $room): void
    {
        $this->getEntityManager()->persist($room);
    }

    public function remove(Room $room): void
    {
        $this->getEntityManager()->remove($room);
    }
}
