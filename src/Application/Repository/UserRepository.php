<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use Dvlpm\Stream\Domain\User\Exception\UndefinedUserException;
use Dvlpm\Stream\Domain\User\Model\Email;
use Dvlpm\Stream\Domain\User\Model\ExternalId;
use Dvlpm\Stream\Domain\User\Model\User;
use Dvlpm\Stream\Domain\User\Model\UserCollection;
use Dvlpm\Stream\Domain\User\Model\Username;
use Dvlpm\Stream\Domain\User\Model\UserRole;
use Dvlpm\Stream\Domain\User\Repository\UserCriteria;
use Dvlpm\Stream\Domain\User\Repository\UserRepositoryInterface;
use Ramsey\Uuid\UuidInterface;
use Doctrine\ORM\Query\Expr\Join;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements UserRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findAllByCriteria(UserCriteria $criteria): UserCollection
    {
        return new UserCollection(...$this->createFindAllByCriteriaQuery($criteria)->getResult());
    }

    private function createFindAllByCriteriaQuery(UserCriteria $criteria): Query
    {
        $qb = $this->createQueryBuilder('user');

        $qb->where('user.role = :role')
            ->setParameter('role', UserRole::USER());

        if ($criteria->getSearchQuery() !== null) {
            $qb
                ->leftJoin(Email::class, 'email', Join::WITH, 'email.user = user')
                ->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->like('LOWER(user.name)', ':searchQuery'),
                        $qb->expr()->like('LOWER(user.username)', ':searchQuery'),
                        $qb->expr()->like('LOWER(email.email)', ':searchQuery'),
                    )
                )
                ->setParameter('searchQuery', '%' . mb_strtolower($criteria->getSearchQuery()) . '%');
        }

        $qb
            ->orderBy('user.createdAt', 'DESC')
            ->setFirstResult($criteria->getOffset())
            ->setMaxResults($criteria->getLimit());

        return $qb->getQuery();
    }

    public function findOneById(UuidInterface $id): ?User
    {
        return $this->find($id);
    }

    public function findOneByIdOrFail(UuidInterface $id): User
    {
        $user = $this->findOneById($id);

        if ($user === null) {
            throw UndefinedUserException::create();
        }

        return $user;
    }

    public function findOneByExternalId(ExternalId $externalId): ?User
    {
        return $this->findOneBy(['externalId' => $externalId]);
    }

    public function findOneByUsername(Username $username): ?User
    {
        return $this->findOneBy(['username' => $username]);
    }

    public function save(User $user): void
    {
        $this->getEntityManager()->persist($user);
    }

    public function flush(): void
    {
        $this->getEntityManager()->flush();
    }
}
