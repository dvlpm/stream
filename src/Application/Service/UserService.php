<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Service;

use Dvlpm\Stream\Application\Event\EventDispatcherInterface;
use Dvlpm\Stream\Application\Repository\UserRepository;
use Dvlpm\Stream\Domain\User\Exception\UserException;
use Dvlpm\Stream\Domain\User\Model\ExternalId;
use Dvlpm\Stream\Domain\User\Model\User;
use Dvlpm\Stream\Domain\User\Payload\CreateGuestPayload;
use Dvlpm\Stream\Domain\User\Payload\RegisterPayload;
use Dvlpm\Stream\Domain\User\Payload\UpdateUserPayload;

final class UserService
{
    private UserRepository $userRepository;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        UserRepository $userRepository,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->userRepository = $userRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function createGuest(CreateGuestPayload $payload): User
    {
        $user = User::createGuest($payload);

        $this->saveAndDispatchFrom($user);

        return $user;
    }

    public function registerGuestUser(
        User $user,
        RegisterPayload $payload
    ): void {
        $this->assertUniqueExternalId($payload->getExternalId());

        $user->register($payload);

        $this->saveAndDispatchFrom($user);
    }

    public function createRegisteredUser(RegisterPayload $payload): User
    {
        $this->assertUniqueExternalId($payload->getExternalId());

        $user = User::createRegistered($payload);

        $this->saveAndDispatchFrom($user);

        return $user;
    }

    public function updateUser(
        User $user,
        UpdateUserPayload $updateDataPayload
    ): void {
        $user->update($updateDataPayload);

        $this->saveAndDispatchFrom($user);
    }

    private function assertUniqueExternalId(ExternalId $externalId): void
    {
        $existingWithExternalId = $this->userRepository->findOneByExternalId($externalId);

        if ($existingWithExternalId !== null) {
            throw UserException::notUniqueExternalId($externalId);
        }
    }

    private function saveAndDispatchFrom(User $user): void
    {
        $this->userRepository->save($user);
        $this->eventDispatcher->dispatchFrom($user);
        $this->userRepository->flush();
        $this->eventDispatcher->flush();
    }
}
