<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Validation\ConstaintValidator;

use DateTimeInterface;
use Dvlpm\Stream\Application\Validation\DataObject\InvalidDateImmutable;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

final class ValidDateTimeImmutableValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint): void
    {
        if ($value === null) {
            return;
        }

        if (!$value instanceof DateTimeInterface) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }

        if ($value instanceof InvalidDateImmutable) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}
