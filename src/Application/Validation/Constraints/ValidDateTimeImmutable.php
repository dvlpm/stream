<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Validation\Constraints;

use Dvlpm\Stream\Application\Validation\ConstaintValidator\ValidDateTimeImmutableValidator;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
final class ValidDateTimeImmutable extends Constraint
{
    public string $message = 'Invalid date.';

    public function validatedBy(): string
    {
        return ValidDateTimeImmutableValidator::class;
    }
}
