<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Application\Validation\DataObject;

use DateTimeImmutable;

final class InvalidDateImmutable extends DateTimeImmutable
{
    private const INVALID_DATE_STRING = '1970-01-01 15:14:12.001';

    private function __construct()
    {
        parent::__construct(self::INVALID_DATE_STRING);
    }

    public static function create(): self
    {
        return new static();
    }
}
