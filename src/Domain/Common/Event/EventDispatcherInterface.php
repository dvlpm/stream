<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Common\Event;

interface EventDispatcherInterface
{
    public function dispatchFrom(EventStoreInterface $eventStore): void;
}
