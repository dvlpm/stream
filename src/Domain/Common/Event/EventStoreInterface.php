<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Common\Event;

interface EventStoreInterface
{
    /** @return DomainEventInterface[] */
    public function flushEvents(): array;
}
