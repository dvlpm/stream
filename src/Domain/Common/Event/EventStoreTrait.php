<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Common\Event;

trait EventStoreTrait
{
    /** @var DomainEventInterface[] */
    private array $events = [];
    /** @var EventStoreInterface[] */
    private array $childrenStores = [];

    /** @return DomainEventInterface[] */
    public function flushEvents(): array
    {
        $storedEvents[] = $this->events;
        foreach ($this->childrenStores as $store) {
            $storedEvents[] = $store->flushEvents();
        }

        $this->events = [];

        return array_values(array_merge([], ...$storedEvents));
    }

    protected function addChildrenStore(EventStoreInterface $store): self
    {
        $this->childrenStores[spl_object_hash($store)] = $store;

        return $this;
    }

    protected function storeEvent(DomainEventInterface $event): self
    {
        $this->events[spl_object_hash($event)] = $event;

        return $this;
    }
}
