<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Common\Exception;

use Exception;

abstract class AbstractAccessException extends Exception implements AccessExceptionInterface
{
}
