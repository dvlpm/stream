<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Common\Exception;

interface AccessExceptionInterface extends DomainExceptionInterface
{
}
