<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Common\Exception;

use Throwable;

interface DomainExceptionInterface extends Throwable
{
}
