<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Common\Exception;

use Exception;

class NotEmptyStringWrapperException extends Exception implements DomainExceptionInterface
{
    public static function emptyValue(): self
    {
        return new static('Empty value provided');
    }
}
