<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Common\Exception;

final class UserRequiredException extends AbstractAccessException
{
    public static function create(): self
    {
        return new static('User required!');
    }
}
