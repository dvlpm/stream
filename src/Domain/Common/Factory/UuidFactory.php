<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Common\Factory;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

final class UuidFactory
{
    public static function createFromStringOrNull(?string $uuid): ?UuidInterface
    {
        return $uuid !== null && Uuid::isValid($uuid)
            ? Uuid::fromString($uuid)
            : null;
    }
}
