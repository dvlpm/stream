<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Common\Model;

use Paillechat\Enum\Exception\EnumException;

/**
 * @method static static UNRECOGNIZED()
 */
abstract class Enum extends \Paillechat\Enum\Enum
{
    public const UNRECOGNIZED = 'UNRECOGNIZED';

    public static function createByNameOrNull(?string $name): ?self
    {
        if (empty($name)) {
            return null;
        }

        try {
            $enum = parent::createByName(mb_strtoupper($name));
        } catch (EnumException $exception) {
            return null;
        }

        return $enum;
    }
}
