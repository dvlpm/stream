<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Common\Model;

final class Meta
{
    private array $data;

    private function __construct(array $data)
    {
        $this->data = $data;
    }

    public static function empty(): self
    {
        return new static([]);
    }

    public static function fromData(array $data): self
    {
        return new static($data);
    }

    public function withMeta(self $other): self
    {
        $meta = clone $this;
        $meta->data = array_merge($meta->data, $other->data);

        return $meta;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function get(string $key)
    {
        return $this->data[$key] ?? null;
    }
}
