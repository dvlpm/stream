<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Common\Model;

final class Name implements NotEmptyStringWrapperInterface
{
    use NotEmptyStringWrapperTrait;
}
