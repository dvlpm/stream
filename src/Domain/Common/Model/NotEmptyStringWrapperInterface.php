<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Common\Model;

interface NotEmptyStringWrapperInterface
{
    public static function fromValue(string $value);
    public function __toString(): string;
}
