<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Common\Model;

use Dvlpm\Stream\Domain\Common\Exception\NotEmptyStringWrapperException;

trait NotEmptyStringWrapperTrait
{
    private string $value;

    private function __construct(string $value)
    {
        $this->value = $value;
    }

    public static function fromValue(string $value): ?self
    {
        if (empty(trim($value))) {
            return null;
        }

        return new self($value);
    }

    public static function fromValueOrFail(string $value): self
    {
        if (empty(trim($value))) {
            throw NotEmptyStringWrapperException::emptyValue();
        }

        return new self($value);
    }

    public function equals(?self $other): bool
    {
        return $other !== null && $this->value === $other->value;
    }

    public function __toString(): string
    {
        return $this->value;
    }
}
