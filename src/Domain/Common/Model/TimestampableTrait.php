<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Common\Model;

use DateTimeImmutable;

trait TimestampableTrait
{
    private ?DateTimeImmutable $createdAt = null;
    private ?DateTimeImmutable $updatedAt = null;

    public function updateTimestamps(): void
    {
        $now = new DateTimeImmutable();

        if ($this->createdAt === null) {
            $this->createdAt = $now;
        }

        $this->updatedAt = $now;
    }
}
