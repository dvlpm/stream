<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Applier;

use Dvlpm\Stream\Domain\Content\Model\AbstractContent;
use Dvlpm\Stream\Domain\Content\Model\PlanningPoker;
use Dvlpm\Stream\Domain\Content\Model\YouTubeStream;
use Dvlpm\Stream\Domain\Content\Payload\UpdateContentPayloadInterface;
use Dvlpm\Stream\Domain\Content\Payload\UpdatePlanningPokerPayload;
use Dvlpm\Stream\Domain\Content\Payload\UpdateYouTubeStreamPayload;
use Dvlpm\Stream\Domain\User\Model\User;

final class UpdateContentApplier
{
    public function apply(User $user, AbstractContent $content, UpdateContentPayloadInterface $payload): void
    {
        if ($content instanceof YouTubeStream && $payload instanceof UpdateYouTubeStreamPayload) {
            $content->update($user, $payload);
        }

        if ($content instanceof PlanningPoker && $payload instanceof UpdatePlanningPokerPayload) {
            $content->update($user, $payload);
        }
    }
}
