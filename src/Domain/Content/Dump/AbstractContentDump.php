<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Dump;

use Dvlpm\Stream\Domain\Content\Model\ContentType;
use Dvlpm\Stream\Domain\User\Dump\UserDump;
use Ramsey\Uuid\UuidInterface;

abstract class AbstractContentDump
{
    private ?UuidInterface $id = null;
    private ?UserDump $owner = null;
    private ?ContentType $type = null;

    public function withId(?UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function withOwner(?UserDump $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function withType(?ContentType $type): self
    {
        $this->type = $type;

        return $this;
    }
}
