<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Dump;

use Dvlpm\Stream\Domain\Content\Model\PlanningPokerCard;

final class PlanningPokerDump extends AbstractContentDump
{
    /** @var PlanningPokerCard[] */
    private iterable $cards = [];
    /** @var PlanningPokerStoryDump[] */
    private iterable $stories = [];
    /** @var PlanningPokerPlayerDump[] */
    private iterable $players = [];

    public static function create(): self
    {
        return new static();
    }

    public function withCards(PlanningPokerCard ...$cards): self
    {
        $this->cards = $cards;

        return $this;
    }

    public function withStories(PlanningPokerStoryDump ...$stories): self
    {
        $this->stories = $stories;

        return $this;
    }

    public function withPlayers(PlanningPokerPlayerDump ...$players): self
    {
        $this->players = $players;

        return $this;
    }
}
