<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Dump;

use Dvlpm\Stream\Domain\Content\Model\PlanningPokerPlayerStatus;
use Dvlpm\Stream\Domain\User\Dump\UserDump;
use Ramsey\Uuid\UuidInterface;

final class PlanningPokerPlayerDump
{
    private ?UuidInterface $id = null;
    private ?UserDump $user = null;
    private ?PlanningPokerPlayerStatus $status = null;

    public static function create(): self
    {
        return new static();
    }

    public function withId(?UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function withUser(?UserDump $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function withStatus(?PlanningPokerPlayerStatus $status): self
    {
        $this->status = $status;

        return $this;
    }
}
