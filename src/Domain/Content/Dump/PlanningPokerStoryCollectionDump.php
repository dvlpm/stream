<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Dump;

use ArrayObject;

final class PlanningPokerStoryCollectionDump extends ArrayObject
{
    public static function create(PlanningPokerStoryDump ...$stories): self
    {
        return new static($stories);
    }
}
