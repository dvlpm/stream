<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Dump;

use Dvlpm\Stream\Domain\Common\Model\Name;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerStoryStatus;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerStoryVote;
use Ramsey\Uuid\UuidInterface;

final class PlanningPokerStoryDump
{
    private ?UuidInterface $id = null;
    private ?Name $name = null;
    private ?int $order = null;
    private ?PlanningPokerStoryStatus $status = null;
    private ?string $description = null;
    private ?string $score = null;
    private ?string $link = null;
    /** @var PlanningPokerStoryVoteDump[] */
    private iterable $votes = [];

    public static function create(): self
    {
        return new static();
    }

    public function withId(?UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function withName(?Name $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function withOrder(?int $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function withStatus(?PlanningPokerStoryStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function withDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function withScore(?string $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function withLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function withVotes(PlanningPokerStoryVoteDump ...$votes): self
    {
        $this->votes = $votes;

        return $this;
    }
}
