<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Dump;

use Dvlpm\Stream\Domain\Content\Model\PlanningPokerCard;
use Dvlpm\Stream\Domain\User\Dump\UserDump;

final class PlanningPokerStoryVoteDump
{
    private ?UserDump $user = null;
    private ?PlanningPokerCard $card = null;

    public static function create(): self
    {
        return new static();
    }

    public function withUser(?UserDump $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function withCard(?PlanningPokerCard $card): self
    {
        $this->card = $card;

        return $this;
    }
}
