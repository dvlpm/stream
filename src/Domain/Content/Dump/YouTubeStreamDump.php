<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Dump;

final class YouTubeStreamDump extends AbstractContentDump
{
    private ?string $link = null;

    public static function create(): self
    {
        return new static();
    }

    public function withLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }
}
