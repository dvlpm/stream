<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Exception;

use Dvlpm\Stream\Domain\Common\Exception\AbstractAccessException;
use Dvlpm\Stream\Domain\User\Model\User;
use Ramsey\Uuid\UuidInterface;

final class ContentCannotBeViewedByUserException extends AbstractAccessException
{
    public static function createForUserAndContentId(User $user, UuidInterface $contentId): self
    {
        return new static(sprintf(
            'Content with id %s cannot be viewed by user %s',
            $contentId,
            $user->getId()
        ));
    }
}
