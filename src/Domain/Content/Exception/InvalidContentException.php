<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Exception;

use Dvlpm\Stream\Domain\Common\Exception\DomainExceptionInterface;
use Exception;

final class InvalidContentException extends Exception implements DomainExceptionInterface
{
    public static function invalidContent(): self
    {
        return new static('Invalid content');
    }
}
