<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Exception;

use Dvlpm\Stream\Domain\Common\Exception\AbstractAccessException;
use Dvlpm\Stream\Domain\User\Model\User;
use Ramsey\Uuid\UuidInterface;

final class PlanningPokerPlayerCannotBeEditedByUserException extends AbstractAccessException
{
    public static function createForUserAndContentId(User $user, UuidInterface $planningPokerPlayerId): self
    {
        return new static(sprintf(
            'Planning poker player with id %s cannot be edited by user %s',
            $planningPokerPlayerId,
            $user->getId()
        ));
    }
}
