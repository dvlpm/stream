<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Exception;

use Dvlpm\Stream\Domain\Common\Exception\AbstractAccessException;
use Dvlpm\Stream\Domain\User\Model\User;
use Ramsey\Uuid\UuidInterface;

final class PlanningPokerStoryCannotBeViewedByUserException extends AbstractAccessException
{
    public static function createForUserAndPlanningPokerStoryId(User $user, UuidInterface $planningPokerStoryId): self
    {
        return new static(sprintf(
            'Planning poker story with id %s cannot be viewed by user %s',
            $planningPokerStoryId,
            $user->getId()
        ));
    }
}
