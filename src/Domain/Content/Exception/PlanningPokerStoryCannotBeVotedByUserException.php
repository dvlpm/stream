<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Exception;

use Dvlpm\Stream\Domain\Common\Exception\AbstractAccessException;
use Dvlpm\Stream\Domain\Common\Model\Name;
use Dvlpm\Stream\Domain\User\Model\User;
use Ramsey\Uuid\UuidInterface;

final class PlanningPokerStoryCannotBeVotedByUserException extends AbstractAccessException
{
    public static function createForUserAndPlanningPokerStoryName(User $user, Name $name): self
    {
        return new static(sprintf(
            'This story %s cannot be voted by user %s',
            $name,
            $user->getId()
        ));
    }
}
