<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Exception;

use Dvlpm\Stream\Domain\Common\Exception\AbstractUndefinedException;
use Ramsey\Uuid\UuidInterface;

final class UndefinedContentException extends AbstractUndefinedException
{
    public static function createWithId(UuidInterface $id): self
    {
        return new static(sprintf(
            'Content with id: %s not found',
            $id
        ));
    }
}
