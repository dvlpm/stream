<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Exception;

use Dvlpm\Stream\Domain\Common\Exception\AbstractUndefinedException;
use Ramsey\Uuid\UuidInterface;

final class UndefinedPlanningPokerStoryException extends AbstractUndefinedException
{
    public static function createWithId(UuidInterface $id): self
    {
        return new static(sprintf(
            'Story with id: %s not found',
            $id
        ));
    }
}
