<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Factory;

use Dvlpm\Stream\Domain\Content\Exception\InvalidContentException;
use Dvlpm\Stream\Domain\Content\Model\AbstractContent;
use Dvlpm\Stream\Domain\Content\Model\PlanningPoker;
use Dvlpm\Stream\Domain\Content\Model\YouTubeStream;
use Dvlpm\Stream\Domain\Content\Payload\CreateContentPayloadInterface;
use Dvlpm\Stream\Domain\Content\Payload\CreatePlanningPokerPayload;
use Dvlpm\Stream\Domain\Content\Payload\CreateYouTubeStreamPayload;
use Dvlpm\Stream\Domain\User\Model\User;

final class ContentFactory
{
    public function create(User $user, CreateContentPayloadInterface $payload): AbstractContent
    {
        $user->assertRegistered();

        if ($payload instanceof CreateYouTubeStreamPayload) {
            return YouTubeStream::create($user, $payload);
        }

        if ($payload instanceof CreatePlanningPokerPayload) {
            return PlanningPoker::create($user, $payload);
        }

        throw InvalidContentException::invalidContent();
    }
}
