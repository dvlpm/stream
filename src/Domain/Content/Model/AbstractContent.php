<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Model;

use Dvlpm\Stream\Domain\Common\Event\EventStoreInterface;
use Dvlpm\Stream\Domain\Common\Event\EventStoreTrait;
use Dvlpm\Stream\Domain\Common\Model\TimestampableTrait;
use Dvlpm\Stream\Domain\Content\Dump\AbstractContentDump;
use Dvlpm\Stream\Domain\Content\Exception\ContentCannotBeEditedByUserException;
use Dvlpm\Stream\Domain\Space\Model\Room;
use Dvlpm\Stream\Domain\User\Model\User;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

abstract class AbstractContent implements EventStoreInterface
{
    use TimestampableTrait, EventStoreTrait;

    protected UuidInterface $id;
    protected User $owner;
    /** @var Room[] */
    protected iterable $rooms;

    protected function __construct(User $owner, ?UuidInterface $id = null)
    {
        $this->owner = $owner;
        $this->id = $id ?? Uuid::uuid4();
    }

    public function sameAs(AbstractContent $content): bool
    {
        if ($this->id === null || $content->id === null) {
            return false;
        }

        return $this->id->equals($content->id);
    }

    public function remove(User $user): void
    {
        $this->assertCanBeEditedByUser($user);
    }

    public function assertCanBeEditedByUser(User $user): void
    {
        if ($this->canBeEditedByUser($user)) {
            return;
        }

        throw ContentCannotBeEditedByUserException::createForUserAndContentId($user, $this->id);
    }

    public function isVisibleBy(User $user): bool
    {
        if ($this->canBeEditedByUser($user)) {
            return true;
        }

        foreach ($this->rooms as $room) {
            if ($room->isVisibleBy($user)) {
                return true;
            }
        }

        return false;
    }

    public function canBeEditedByUser(User $user): bool
    {
        return $this->owner->equals($user);
    }

    abstract public function dumpBy(User $user): AbstractContentDump;
}
