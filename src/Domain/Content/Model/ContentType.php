<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Model;

use Dvlpm\Stream\Domain\Common\Model\Enum;

/**
 * @method static static YOUTUBE_STREAM()
 * @method static static PLANNING_POKER()
 */
class ContentType extends Enum
{
    public const YOUTUBE_STREAM = 'YOUTUBE_STREAM';
    public const PLANNING_POKER = 'PLANNING_POKER';

    public static function availableTypes(): array
    {
        return [
            self::YOUTUBE_STREAM(),
            self::PLANNING_POKER(),
        ];
    }
}
