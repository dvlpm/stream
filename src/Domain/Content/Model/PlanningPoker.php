<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Model;

use Dvlpm\Stream\Domain\Content\Dump\PlanningPokerDump;
use Dvlpm\Stream\Domain\Content\Exception\ContentCannotBeViewedByUserException;
use Dvlpm\Stream\Domain\Content\Payload\AddPlanningPokerStoryToPlanningPokerPayload;
use Dvlpm\Stream\Domain\Content\Payload\CreatePlanningPokerPayload;
use Dvlpm\Stream\Domain\Content\Payload\UpdatePlanningPokerPayload;
use Dvlpm\Stream\Domain\User\Model\User;
use Ramsey\Uuid\UuidInterface;

class PlanningPoker extends AbstractContent
{
    /** @var PlanningPokerCard[] */
    private iterable $cards;
    /** @var PlanningPokerStory[] */
    private iterable $stories = [];
    /** @var PlanningPokerPlayer[] */
    private iterable $players = [];

    private function __construct(
        User $owner,
        iterable $cards,
        ?UuidInterface $id = null
    ) {
        parent::__construct($owner, $id);
        $this->cards = $cards;
    }

    public static function create(
        User $user,
        CreatePlanningPokerPayload $payload
    ): self {
        return new static(
            $user,
            $payload->getCards(),
            $payload->getId()
        );
    }

    public function addStory(User $user, AddPlanningPokerStoryToPlanningPokerPayload $payload): void
    {
        $this->assertCanBeEditedByUser($user);

        $this->doAddStories(PlanningPokerStory::create(
            $this,
            $payload->getCreatePlanningPokerStoryPayload()
        ));
    }

    private function doAddStories(PlanningPokerStory ...$stories): self
    {
        foreach ($stories as $story) {
            $this->stories[] = $story;
            $this->addChildrenStore($story);
        }

        return $this;
    }

    public function addPlayer(User $user, PlanningPokerPlayer $player): void
    {
        $this->assertCanBeEditedByUser($user);

        $this->doAddPlayers($player);
    }

    private function doAddPlayers(PlanningPokerPlayer ...$players): self
    {
        foreach ($players as $player) {
            $this->players[] = $player;
            $this->addChildrenStore($player);
            $player->attachToPoker($this);
        }

        return $this;
    }

    public function update(
        User $user,
        UpdatePlanningPokerPayload $payload
    ): void {
        $this->assertCanBeEditedByUser($user);

        $this->cards = $payload->getCards();
    }

    public function dumpBy(User $user): PlanningPokerDump
    {
        if (!$this->isVisibleBy($user)) {
            throw ContentCannotBeViewedByUserException::createForUserAndContentId($user, $this->id);
        }

        return PlanningPokerDump::create()
            ->withType(ContentType::PLANNING_POKER())
            ->withId($this->id)
            ->withCards(...$this->cards)
            ->withPlayers(...array_map(
                fn(PlanningPokerPlayer $player) => $player->dumpBy($user),
                iterator_to_array($this->players)
            ));
    }
}
