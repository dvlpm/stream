<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Model;

use Dvlpm\Stream\Domain\Common\Model\Enum;

/**
* @method static static ZERO()
* @method static static HALF()
* @method static static ONE()
* @method static static TWO()
* @method static static THREE()
* @method static static FOUR()
* @method static static FIVE()
* @method static static SIX()
* @method static static SEVEN()
* @method static static EIGHT()
* @method static static NINE()
* @method static static TEN()
* @method static static THIRTEEN()
* @method static static TWENTY()
* @method static static TWENTY_ONE()
* @method static static THIRTY_FOUR()
* @method static static FORTY()
* @method static static FIFTY_FIVE()
* @method static static EIGHTY_NINE()
* @method static static HUNDRED()
* @method static static QUESTION()
* @method static static COFFEE()
* @method static static ACE()
* @method static static KING()
* @method static static XS()
* @method static static S()
* @method static static M()
* @method static static L()
* @method static static XL()
* @method static static XXL()
 */
final class PlanningPokerCard extends Enum
{
    private const ZERO = 'ZERO';
    private const HALF = 'HALF';
    private const ONE = 'ONE';
    private const TWO = 'TWO';
    private const THREE = 'THREE';
    private const FOUR = 'FOUR';
    private const FIVE = 'FIVE';
    private const SIX = 'SIX';
    private const SEVEN = 'SEVEN';
    private const EIGHT = 'EIGHT';
    private const NINE = 'NINE';
    private const TEN = 'TEN';
    private const THIRTEEN = 'THIRTEEN';
    private const TWENTY = 'TWENTY';
    private const TWENTY_ONE = 'TWENTY_ONE';
    private const THIRTY_FOUR = 'THIRTY_FOUR';
    private const FORTY = 'FORTY';
    private const FIFTY_FIVE = 'FIFTY_FIVE';
    private const EIGHTY_NINE = 'EIGHTY_NINE';
    private const HUNDRED = 'HUNDRED';
    private const QUESTION = 'QUESTION';
    private const COFFEE = 'COFFEE';
    private const ACE = 'ACE';
    private const KING = 'KING';
    private const XS = 'XS';
    private const S = 'S';
    private const M = 'M';
    private const L = 'L';
    private const XL = 'XL';
    private const XXL = 'XXL';
}
