<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Model;

use Dvlpm\Stream\Domain\Common\Event\EventStoreInterface;
use Dvlpm\Stream\Domain\Common\Event\EventStoreTrait;
use Dvlpm\Stream\Domain\Common\Model\TimestampableTrait;
use Dvlpm\Stream\Domain\Content\Dump\PlanningPokerPlayerDump;
use Dvlpm\Stream\Domain\Content\Exception\PlanningPokerPlayerCannotBeEditedByUserException;
use Dvlpm\Stream\Domain\User\Model\User;
use Ramsey\Uuid\UuidInterface;

class PlanningPokerPlayer implements EventStoreInterface
{
    use EventStoreTrait, TimestampableTrait;

    private UuidInterface $id;
    private User $user;
    private PlanningPoker $poker;
    private PlanningPokerPlayerStatus $status;

    private function __construct(
        UuidInterface $id,
        User $user,
        ?PlanningPokerPlayerStatus $status = null
    ) {
        $this->id = $id;
        $this->user = $user;
        $this->status = $status ?? PlanningPokerPlayerStatus::ACTIVE();
    }

    public static function draft(
        UuidInterface $id,
        User $user
    ): PlanningPokerPlayer {
        return new PlanningPokerPlayer($id, $user);
    }

    public function attachToPoker(PlanningPoker $poker): self
    {
        $this->poker = $poker;

        return $this;
    }

    public function update(User $user, PlanningPokerPlayerStatus $status): void
    {
        $this->assertCanBeUpdatedByUser($user);

        $this->status = $status;
    }

    private function assertCanBeUpdatedByUser(User $user): void
    {
        if ($this->canBeUpdatedByUser($user)) {
            return;
        }

        throw PlanningPokerPlayerCannotBeEditedByUserException::createForUserAndContentId(
            $user,
            $this->id
        );
    }

    private function canBeUpdatedByUser(User $user): bool
    {
        return $this->user->equals($user) || $this->poker->canBeEditedByUser($user);
    }

    public function remove(User $user): void
    {
        $this->assertCanBeRemovedByUser($user);
    }

    private function assertCanBeRemovedByUser(User $user): void
    {
        $this->poker->assertCanBeEditedByUser($user);
    }

    public function dumpBy(User $user): PlanningPokerPlayerDump
    {
        return (new PlanningPokerPlayerDump())
            ->withId($this->id)
            ->withStatus($this->status)
            ->withUser($this->user->dumpBy($user));
    }
}
