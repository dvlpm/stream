<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Model;

use Dvlpm\Stream\Domain\Common\Model\Enum;

/**
 * @method static static OBSERVER()
 * @method static static ACTIVE()
 */
final class PlanningPokerPlayerStatus extends Enum
{
    private const OBSERVER = 'OBSERVER';
    private const ACTIVE = 'ACTIVE';
}
