<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Model;

use Dvlpm\Stream\Domain\Common\Event\EventStoreInterface;
use Dvlpm\Stream\Domain\Common\Event\EventStoreTrait;
use Dvlpm\Stream\Domain\Common\Model\Name;
use Dvlpm\Stream\Domain\Common\Model\TimestampableTrait;
use Dvlpm\Stream\Domain\Content\Dump\PlanningPokerStoryDump;
use Dvlpm\Stream\Domain\Content\Exception\PlanningPokerStoryCannotBeViewedByUserException;
use Dvlpm\Stream\Domain\Content\Exception\PlanningPokerStoryCannotBeVotedByUserException;
use Dvlpm\Stream\Domain\Content\Payload\AddPlanningPokerStoryVoteToPlanningPokerStoryPayload;
use Dvlpm\Stream\Domain\Content\Payload\CreatePlanningPokerStoryPayload;
use Dvlpm\Stream\Domain\Content\Payload\UpdatePlanningPokerStoryPayload;
use Dvlpm\Stream\Domain\User\Model\User;
use Ramsey\Uuid\UuidInterface;

class PlanningPokerStory implements EventStoreInterface
{
    use EventStoreTrait, TimestampableTrait;

    private ?UuidInterface $id;
    private PlanningPoker $poker;
    private Name $name;
    private int $order;
    private PlanningPokerStoryStatus $status;
    private ?string $description;
    private string $score = '';
    private ?string $link;
    /** @var PlanningPokerStoryVote[] */
    private iterable $votes = [];

    private function __construct(
        PlanningPoker $poker,
        Name $name,
        ?int $order,
        ?string $link,
        ?string $description,
        ?UuidInterface $id = null
    ) {
        $this->poker = $poker;
        $this->status = PlanningPokerStoryStatus::BACKLOG();
        $this->name = $name;
        $this->order = $order ?? 0;
        $this->link = $link;
        $this->description = $description;
        $this->id = $id;
    }

    public static function create(
        PlanningPoker $poker,
        CreatePlanningPokerStoryPayload $payload
    ): self {
        return new static(
            $poker,
            $payload->getName(),
            $payload->getOrder(),
            $payload->getExternalLink(),
            $payload->getDescription(),
            $payload->getId()
        );
    }

    public function update(
        User $user,
        UpdatePlanningPokerStoryPayload $payload
    ): void {
        $this->assertCanBeEditedByUser($user);

        $this->name = $payload->getName();
        $this->status = $payload->getStatus();
        $this->order = $payload->getOrder();
        $this->score = $payload->getScore();
        $this->link = $payload->getExternalLink();
        $this->description = $payload->getDescription();
    }

    public function removeVotes(User $user): void
    {
        $this->assertCanBeEditedByUser($user);

        $this->votes = [];
    }

    public function addVote(
        User $user,
        AddPlanningPokerStoryVoteToPlanningPokerStoryPayload $payload
    ): void {
        if (!$this->poker->isVisibleBy($user)) {
            throw PlanningPokerStoryCannotBeVotedByUserException::createForUserAndPlanningPokerStoryName(
                $user,
                $this->name
            );
        }

        $this->doAddVotes(PlanningPokerStoryVote::create(
            $user,
            $this,
            $payload->getCreatePlanningPokerStoryVotePayload()
        ));
    }

    private function doAddVotes(PlanningPokerStoryVote ...$votes): self
    {
        foreach ($votes as $vote) {
            $existingVote = $this->getVoteFromSameUser($vote);
            if ($existingVote !== null) {
                $existingVote->updateFrom($vote);

                continue;
            }

            $this->votes[] = $vote;
            $this->addChildrenStore($vote);
        }

        return $this;
    }

    private function getVoteFromSameUser(PlanningPokerStoryVote $other): ?PlanningPokerStoryVote
    {
        foreach ($this->votes as $vote) {
            if ($vote->isFromSameUser($other)) {
                return $vote;
            }
        }

        return null;
    }

    public function remove(User $user): void
    {
        $this->assertCanBeEditedByUser($user);
    }

    private function assertCanBeEditedByUser(User $user): void
    {
        $this->poker->assertCanBeEditedByUser($user);
    }

    public function isCompleted(): bool
    {
        return $this->status === PlanningPokerStoryStatus::COMPLETED();
    }

    public function isVisibleBy(User $user): bool
    {
        return $this->poker->isVisibleBy($user);
    }

    public function dumpBy(User $user): PlanningPokerStoryDump
    {
        if (!$this->isVisibleBy($user)) {
            throw PlanningPokerStoryCannotBeViewedByUserException::createForUserAndPlanningPokerStoryId(
                $user,
                $this->id
            );
        }

        return PlanningPokerStoryDump::create()
            ->withId($this->id)
            ->withName($this->name)
            ->withOrder($this->order)
            ->withStatus($this->status)
            ->withScore($this->score)
            ->withDescription($this->description)
            ->withLink($this->link)
            ->withVotes(...array_map(
                fn(PlanningPokerStoryVote $vote) => $vote->dumpBy($user),
                iterator_to_array($this->votes)
            ));
    }
}
