<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Model;

use Dvlpm\Stream\Domain\Content\Dump\PlanningPokerStoryCollectionDump;
use Dvlpm\Stream\Domain\User\Model\User;

final class PlanningPokerStoryCollection
{
    /** @var PlanningPokerStory[] */
    private iterable $stories;

    public function __construct(PlanningPokerStory ...$stories)
    {
        $this->stories = $stories;
    }

    public function dumpBy(User $user): PlanningPokerStoryCollectionDump
    {
        $dumps = [];

        foreach ($this->stories as $story) {
            $dumps[] = $story->dumpBy($user);
        }

        return PlanningPokerStoryCollectionDump::create(...$dumps);
    }
}
