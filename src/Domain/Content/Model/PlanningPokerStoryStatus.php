<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Model;

use Dvlpm\Stream\Domain\Common\Model\Enum;

/**
* @method static static BACKLOG()
* @method static static ACTIVE()
* @method static static COMPLETED()
 */
final class PlanningPokerStoryStatus extends Enum
{
    private const BACKLOG = 'BACKLOG';
    private const ACTIVE = 'ACTIVE';
    private const COMPLETED = 'COMPLETED';
}
