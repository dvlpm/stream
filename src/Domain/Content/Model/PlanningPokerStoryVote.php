<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Model;

use Dvlpm\Stream\Domain\Common\Event\EventStoreInterface;
use Dvlpm\Stream\Domain\Common\Event\EventStoreTrait;
use Dvlpm\Stream\Domain\Common\Model\TimestampableTrait;
use Dvlpm\Stream\Domain\Content\Dump\PlanningPokerStoryVoteDump;
use Dvlpm\Stream\Domain\Content\Payload\CreatePlanningPokerStoryVotePayload;
use Dvlpm\Stream\Domain\User\Model\User;

class PlanningPokerStoryVote implements EventStoreInterface
{
    use TimestampableTrait, EventStoreTrait;

    private User $user;
    private PlanningPokerStory $story;
    private PlanningPokerCard $card;

    private function __construct(User $user, PlanningPokerStory $story, PlanningPokerCard $card)
    {
        $this->user = $user;
        $this->story = $story;
        $this->card = $card;
    }

    public static function create(
        User $user,
        PlanningPokerStory $story,
        CreatePlanningPokerStoryVotePayload $payload
    ): self {
        return new static(
            $user,
            $story,
            $payload->getCard()
        );
    }

    public function updateFrom(self $vote): void
    {
        $this->card = $vote->card;
    }

    public function isFromSameUser(self $vote): bool
    {
        return $this->user->equals($vote->user);
    }

    public function dumpBy(User $user): PlanningPokerStoryVoteDump
    {
        $dump = PlanningPokerStoryVoteDump::create();

        $dump->withUser($this->user->dumpBy($user));

        if ($this->story->isCompleted() || $this->user->equals($user)) {
            $dump->withCard($this->card);
        }

        return $dump;
    }
}
