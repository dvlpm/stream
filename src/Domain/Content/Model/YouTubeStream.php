<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Model;

use Dvlpm\Stream\Domain\Content\Dump\YouTubeStreamDump;
use Dvlpm\Stream\Domain\Content\Payload\CreateYouTubeStreamPayload;
use Dvlpm\Stream\Domain\Content\Payload\UpdateYouTubeStreamPayload;
use Dvlpm\Stream\Domain\User\Model\User;
use Ramsey\Uuid\UuidInterface;

class YouTubeStream extends AbstractContent
{
    private string $link;

    private function __construct(
        User $owner,
        string $link,
        ?UuidInterface $id = null
    ) {
        parent::__construct($owner, $id);
        $this->link = $link;
    }

    public static function create(
        User $user,
        CreateYouTubeStreamPayload $payload
    ): self {
        return new static($user, $payload->getLink(), $payload->getId());
    }

    public function update(
        User $user,
        UpdateYouTubeStreamPayload $payload
    ): void {
        $this->assertCanBeEditedByUser($user);

        $this->link = $payload->getLink();
    }

    public function dumpBy(User $user): YouTubeStreamDump
    {
        return YouTubeStreamDump::create()
            ->withId($this->id)
            ->withType(ContentType::YOUTUBE_STREAM())
            ->withLink($this->link);
    }
}
