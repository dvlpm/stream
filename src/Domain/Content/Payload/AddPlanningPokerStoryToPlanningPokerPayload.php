<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Payload;

final class AddPlanningPokerStoryToPlanningPokerPayload
{
    private CreatePlanningPokerStoryPayload $createPlanningPokerStoryPayload;

    public function __construct(CreatePlanningPokerStoryPayload $createPlanningPokerStoryPayload)
    {
        $this->createPlanningPokerStoryPayload = $createPlanningPokerStoryPayload;
    }

    public function getCreatePlanningPokerStoryPayload(): CreatePlanningPokerStoryPayload
    {
        return $this->createPlanningPokerStoryPayload;
    }
}
