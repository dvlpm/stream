<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Payload;

final class AddPlanningPokerStoryVoteToPlanningPokerStoryPayload
{
    private CreatePlanningPokerStoryVotePayload $createPlanningPokerStoryVotePayload;

    public function __construct(CreatePlanningPokerStoryVotePayload $createPlanningPokerStoryVotePayload)
    {
        $this->createPlanningPokerStoryVotePayload = $createPlanningPokerStoryVotePayload;
    }

    public function getCreatePlanningPokerStoryVotePayload(): CreatePlanningPokerStoryVotePayload
    {
        return $this->createPlanningPokerStoryVotePayload;
    }
}
