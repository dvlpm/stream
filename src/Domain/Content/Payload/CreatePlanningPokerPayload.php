<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Payload;

use Dvlpm\Stream\Domain\Content\Model\PlanningPokerCard;
use Ramsey\Uuid\UuidInterface;

final class CreatePlanningPokerPayload implements CreateContentPayloadInterface
{
    private ?UuidInterface $id;
    /** @var PlanningPokerCard[] */
    private iterable $cards;

    public function __construct(iterable $cards, ?UuidInterface $id = null)
    {
        $this->cards = $cards;
        $this->id = $id;
    }

    public function getCards(): array
    {
        return $this->cards;
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }
}
