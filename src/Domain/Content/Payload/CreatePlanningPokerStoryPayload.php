<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Payload;

use Dvlpm\Stream\Domain\Common\Model\Name;
use Ramsey\Uuid\UuidInterface;

final class CreatePlanningPokerStoryPayload
{
    private Name $name;
    private ?int $order;
    private ?string $link;
    private ?string $description;
    private ?UuidInterface $id;

    public function __construct(
        Name $name,
        ?int $order,
        ?string $link,
        ?string $description,
        ?UuidInterface $id = null
    ) {
        $this->name = $name;
        $this->order = $order;
        $this->link = $link;
        $this->description = $description;
        $this->id = $id;
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function getOrder(): ?int
    {
        return $this->order;
    }

    public function getExternalLink(): ?string
    {
        return $this->link;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }
}
