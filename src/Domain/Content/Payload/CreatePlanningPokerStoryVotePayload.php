<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Payload;

use Dvlpm\Stream\Domain\Content\Model\PlanningPokerCard;

final class CreatePlanningPokerStoryVotePayload
{
    private PlanningPokerCard $card;

    public function __construct(PlanningPokerCard $card)
    {
        $this->card = $card;
    }

    public function getCard(): PlanningPokerCard
    {
        return $this->card;
    }
}
