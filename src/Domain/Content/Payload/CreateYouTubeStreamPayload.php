<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Payload;

use Ramsey\Uuid\UuidInterface;

final class CreateYouTubeStreamPayload implements CreateContentPayloadInterface
{
    private ?UuidInterface $id;
    private string $link;

    public function __construct(string $link, ?UuidInterface $id = null)
    {
        $this->link = $link;
        $this->id = $id;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }
}
