<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Payload;

use Dvlpm\Stream\Domain\Content\Model\PlanningPokerCard;

final class UpdatePlanningPokerPayload implements UpdateContentPayloadInterface
{
    /** @var PlanningPokerCard[] */
    public iterable $cards = [];

    public function __construct(iterable $cards)
    {
        $this->cards = $cards;
    }

    public function getCards(): iterable
    {
        return $this->cards;
    }
}
