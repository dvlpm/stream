<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Payload;

use Dvlpm\Stream\Domain\Common\Model\Name;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerStoryStatus;

final class UpdatePlanningPokerStoryPayload
{
    private Name $name;
    private PlanningPokerStoryStatus $status;
    private int $order;
    private string $score;
    private ?string $link;
    private ?string $description;

    public function __construct(
        Name $name,
        PlanningPokerStoryStatus $status,
        int $order,
        string $score,
        ?string $link,
        ?string $description
    ) {
        $this->name = $name;
        $this->status = $status;
        $this->order = $order;
        $this->score = $score;
        $this->link = $link;
        $this->description = $description;
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function getStatus(): PlanningPokerStoryStatus
    {
        return $this->status;
    }

    public function getOrder(): int
    {
        return $this->order;
    }

    public function getScore(): string
    {
        return $this->score;
    }

    public function getExternalLink(): ?string
    {
        return $this->link;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }
}
