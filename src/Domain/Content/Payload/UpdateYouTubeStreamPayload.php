<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Payload;

final class UpdateYouTubeStreamPayload implements UpdateContentPayloadInterface
{
    private string $link;

    public function __construct(string $link)
    {
        $this->link = $link;
    }

    public function getLink(): string
    {
        return $this->link;
    }
}
