<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Repository;

use Dvlpm\Stream\Domain\Content\Exception\UndefinedContentException;
use Dvlpm\Stream\Domain\Content\Model\AbstractContent;
use Ramsey\Uuid\UuidInterface;

interface ContentRepositoryInterface
{
    /**
     * @param UuidInterface $id
     * @return AbstractContent
     * @throws UndefinedContentException
     */
    public function findOneByIdOrFail(UuidInterface $id): AbstractContent;
    public function findOneById(UuidInterface $id): ?AbstractContent;
    public function save(AbstractContent $content): void;
    public function remove(AbstractContent $content): void;
}
