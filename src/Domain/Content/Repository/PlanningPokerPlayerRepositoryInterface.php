<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Repository;

use Dvlpm\Stream\Domain\Content\Exception\UndefinedPlanningPokerPlayerException;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerPlayer;
use Ramsey\Uuid\UuidInterface;

interface PlanningPokerPlayerRepositoryInterface
{
    public function findOneById(UuidInterface $id): ?PlanningPokerPlayer;
    /**
     * @param UuidInterface $id
     * @return PlanningPokerPlayer
     * @throws UndefinedPlanningPokerPlayerException
     */
    public function findOneByIdOrFail(UuidInterface $id): PlanningPokerPlayer;
    public function remove(PlanningPokerPlayer $planningPokerPlayer): void;
}
