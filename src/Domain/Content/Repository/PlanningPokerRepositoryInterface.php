<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Repository;

use Dvlpm\Stream\Domain\Content\Exception\UndefinedContentException;
use Dvlpm\Stream\Domain\Content\Model\PlanningPoker;
use Ramsey\Uuid\UuidInterface;

interface PlanningPokerRepositoryInterface
{
    /**
     * @param UuidInterface $id
     * @return PlanningPoker
     * @throws UndefinedContentException
     */
    public function findOneByIdOrFail(UuidInterface $id): PlanningPoker;
}
