<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Repository;

use Dvlpm\Stream\Domain\Content\Model\PlanningPokerStoryStatus;
use Ramsey\Uuid\UuidInterface;

final class PlanningPokerStoryCriteria
{
    private UuidInterface $planningPokerId;
    private ?PlanningPokerStoryStatus $status;
    private int $limit;
    private int $offset;

    public function __construct(
        UuidInterface $planningPokerId,
        ?PlanningPokerStoryStatus $status = null,
        ?int $limit = null,
        ?int $offset = null
    ) {
        $this->planningPokerId = $planningPokerId;
        $this->status = $status;
        $this->limit = $limit ?? 10;
        $this->offset = $offset ?? 0;
    }

    public function getPlanningPokerId(): UuidInterface
    {
        return $this->planningPokerId;
    }

    public function getStatus(): ?PlanningPokerStoryStatus
    {
        return $this->status;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }
}
