<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Content\Repository;

use Dvlpm\Stream\Domain\Content\Exception\UndefinedPlanningPokerStoryException;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerStory;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerStoryCollection;
use Ramsey\Uuid\UuidInterface;

interface PlanningPokerStoryRepositoryInterface
{
    public function findAllByCriteria(PlanningPokerStoryCriteria $planningPokerStoryCriteria): PlanningPokerStoryCollection;
    public function findOneById(UuidInterface $id): ?PlanningPokerStory;
    /**
     * @param UuidInterface $id
     * @return PlanningPokerStory
     * @throws UndefinedPlanningPokerStoryException
     */
    public function findOneByIdOrFail(UuidInterface $id): PlanningPokerStory;
    public function remove(PlanningPokerStory $planningPokerStory): void;
}
