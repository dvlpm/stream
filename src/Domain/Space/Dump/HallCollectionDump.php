<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Dump;

use ArrayObject;

final class HallCollectionDump extends ArrayObject
{
    public static function create(HallDump ...$dumps): self
    {
        return new static($dumps);
    }
}
