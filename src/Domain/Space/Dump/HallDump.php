<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Dump;

use Dvlpm\Stream\Domain\Common\Model\Name;
use Dvlpm\Stream\Domain\Space\Model\Identifier;
use Dvlpm\Stream\Domain\Space\Model\Host;
use Ramsey\Uuid\UuidInterface;

final class HallDump
{
    private ?UuidInterface $id = null;
    private ?Identifier $identifier = null;
    /** @var HostDump[] */
    private iterable $hosts = [];
    /** @var RoomDump[] */
    private iterable $rooms = [];
    private ?Name $name = null;
    private ?string $description;

    public static function create(): self
    {
        return new static();
    }

    public function withId(?UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function withIdentifier(?Identifier $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function withHosts(HostDump ...$hosts): self
    {
        $this->hosts = $hosts;

        return $this;
    }

    public function withRooms(RoomDump ...$rooms): self
    {
        $this->rooms = $rooms;

        return $this;
    }

    public function withName(?Name $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function withDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
