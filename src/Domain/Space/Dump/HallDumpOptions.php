<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Dump;

final class HallDumpOptions
{
    private bool $showRooms;

    private function __construct(bool $showRooms = true)
    {
        $this->showRooms = $showRooms;
    }

    public static function default(): self
    {
        return new static();
    }

    public function showRooms(bool $showRooms): self
    {
        $this->showRooms = $showRooms;

        return $this;
    }

    public function isShowRooms(): bool
    {
        return $this->showRooms;
    }
}
