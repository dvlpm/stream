<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Dump;

use Dvlpm\Stream\Domain\Space\Model\HostRole;
use Dvlpm\Stream\Domain\User\Dump\UserDump;

final class HostDump
{
    private ?HallDump $hall = null;
    private ?UserDump $user = null;
    private ?HostRole $role = null;

    public static function create(): self
    {
        return new static();
    }

    public function withHall(?HallDump $hall): self
    {
        $this->hall = $hall;

        return $this;
    }

    public function withUser(?UserDump $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function withRole(?HostRole $role): self
    {
        $this->role = $role;

        return $this;
    }
}
