<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Dump;

use Dvlpm\Stream\Domain\Space\Model\ParticipantStatus;
use Dvlpm\Stream\Domain\User\Dump\UserDump;
use Ramsey\Uuid\UuidInterface;

final class ParticipantDump
{
    private ?UuidInterface $id = null;
    private ?RoomDump $room = null;
    private ?ParticipantStatus $status = null;
    private ?ParticipantIdentificationDump $identification = null;
    private ?UserDump $user = null;

    public static function create(): self
    {
        return new static();
    }

    public function withId(?UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function withRoom(?RoomDump $room): self
    {
        $this->room = $room;

        return $this;
    }

    public function withStatus(?ParticipantStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function withIdentification(?ParticipantIdentificationDump $identification): self
    {
        $this->identification = $identification;

        return $this;
    }

    public function withUser(?UserDump $user): self
    {
        $this->user = $user;

        return $this;
    }
}
