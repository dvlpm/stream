<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Dump;

use Ramsey\Uuid\UuidInterface;

final class ParticipantIdentificationDump
{
    private ?UuidInterface $userId = null;
    private ?string $email = null;

    public static function create(): self
    {
        return new static();
    }

    public function withUserId(?UuidInterface $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function withEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }
}
