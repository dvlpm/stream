<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Dump;

use Dvlpm\Stream\Domain\Common\Model\Name;
use Dvlpm\Stream\Domain\Content\Dump\AbstractContentDump;
use Dvlpm\Stream\Domain\Space\Model\AccessType;
use Dvlpm\Stream\Domain\Space\Model\Key;
use Ramsey\Uuid\UuidInterface;

final class RoomDump
{
    private ?UuidInterface $id = null;
    private ?HallDump $hall = null;
    private ?Name $name = null;
    private ?AccessType $accessType = null;
    /** @var ParticipantDump[] */
    private iterable $participants = [];
    private ?string $description = null;
    /** @var AbstractContentDump[] */
    private iterable $contents = [];
    private ?Key $key = null;

    public static function create(): self
    {
        return new static();
    }

    public function withId(?UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function withHall(?HallDump $hall): self
    {
        $this->hall = $hall;

        return $this;
    }

    public function withName(?Name $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function withAccessType(?AccessType $accessType): self
    {
        $this->accessType = $accessType;

        return $this;
    }

    public function withParticipants(array $participants): self
    {
        $this->participants = $participants;

        return $this;
    }

    public function withDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function withContents(array $contents): self
    {
        $this->contents = $contents;

        return $this;
    }

    public function withKey(?Key $key): self
    {
        $this->key = $key;

        return $this;
    }
}
