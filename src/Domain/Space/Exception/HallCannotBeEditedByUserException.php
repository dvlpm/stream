<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Exception;

use Dvlpm\Stream\Domain\Common\Exception\AbstractAccessException;
use Dvlpm\Stream\Domain\Space\Model\Identifier;
use Dvlpm\Stream\Domain\User\Model\User;

final class HallCannotBeEditedByUserException extends AbstractAccessException
{
    public static function createForUserAndHallIdentifier(User $user, Identifier $hallIdentifier): self
    {
        return new static(sprintf(
            'Hall with identifier: %s cannot be edited by user with id: %s',
            $hallIdentifier,
            $user->getId()
        ));
    }
}
