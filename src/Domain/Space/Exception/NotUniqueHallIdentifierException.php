<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Exception;

use Dvlpm\Stream\Domain\Common\Exception\DomainExceptionInterface;
use Dvlpm\Stream\Domain\Space\Model\Identifier;
use Exception;

final class NotUniqueHallIdentifierException extends Exception implements DomainExceptionInterface
{
    public static function createWithIdentifier(Identifier $identifier): self
    {
        return new static(sprintf(
            'Not unique hall identifier: %s!',
            $identifier
        ));
    }
}
