<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Exception;

use Dvlpm\Stream\Domain\Common\Exception\AbstractAccessException;
use Dvlpm\Stream\Domain\User\Model\User;
use Ramsey\Uuid\UuidInterface;

final class RoomCannotBeViewedByUserException extends AbstractAccessException
{
    public static function createForUserAndRoomId(User $user, UuidInterface $roomId): self
    {
        return new static(sprintf(
            'Room with id: %s cannot be viewed by user with id: %s',
            $roomId,
            $user->getId()
        ));
    }
}
