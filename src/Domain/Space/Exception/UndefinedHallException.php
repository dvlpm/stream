<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Exception;

use Dvlpm\Stream\Domain\Common\Exception\AbstractUndefinedException;
use Dvlpm\Stream\Domain\Space\Model\Identifier;

final class UndefinedHallException extends AbstractUndefinedException
{
    public static function createWithIdentifier(Identifier $identifier): self
    {
        return new static(sprintf(
            'Hall with identifier: %s not found',
            $identifier
        ));
    }
}
