<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Exception;

use Dvlpm\Stream\Domain\Common\Exception\AbstractUndefinedException;
use Ramsey\Uuid\UuidInterface;

final class UndefinedRoomException extends AbstractUndefinedException
{
    public static function createWithId(UuidInterface $id): self
    {
        return new static(sprintf(
            'Room with id: %s not found',
            $id
        ));
    }
}
