<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Model;

use Dvlpm\Stream\Domain\Common\Model\Enum;

/**
 * @method static static ANONYMOUS()
 * @method static static AUTHORIZED()
 * @method static static LISTED()
 * @method static static PAID()
 */
final class AccessType extends Enum
{
    private const ANONYMOUS = 'ANONYMOUS';
    private const AUTHORIZED = 'AUTHORIZED';
    private const LISTED = 'LISTED';
    private const PAID = 'PAID';

    public static function availableTypes(): array
    {
        return [
            self::AUTHORIZED(),
            self::ANONYMOUS(),
            self::LISTED(),
            self::PAID(),
        ];
    }
}
