<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Model;

use Dvlpm\Stream\Domain\Common\Event\EventStoreInterface;
use Dvlpm\Stream\Domain\Common\Event\EventStoreTrait;
use Dvlpm\Stream\Domain\Common\Model\Name;
use Dvlpm\Stream\Domain\Common\Model\TimestampableTrait;
use Dvlpm\Stream\Domain\Space\Dump\HallDump;
use Dvlpm\Stream\Domain\Space\Dump\HallDumpOptions;
use Dvlpm\Stream\Domain\Space\Exception\HallCannotBeEditedByUserException;
use Dvlpm\Stream\Domain\Space\Exception\NotUniqueHallIdentifierException;
use Dvlpm\Stream\Domain\Space\Payload\AddRoomToHallPayload;
use Dvlpm\Stream\Domain\Space\Payload\CreateHallPayload;
use Dvlpm\Stream\Domain\Space\Payload\UpdateHallPayload;
use Dvlpm\Stream\Domain\Space\Repository\HallRepositoryInterface;
use Dvlpm\Stream\Domain\User\Model\User;
use Ramsey\Uuid\UuidInterface;

class Hall implements EventStoreInterface
{
    use EventStoreTrait, TimestampableTrait;

    private ?UuidInterface $id = null;
    private Identifier $identifier;
    /** @var Host[] */
    private iterable $hosts = [];
    /** @var Room[] */
    private iterable $rooms = [];
    private ?Name $name;
    private ?string $description;

    private function __construct(
        Identifier $identifier,
        ?Name $name = null,
        ?string $description = null
    ) {
        $this->identifier = $identifier;
        $this->name = $name;
        $this->description = $description;
    }

    public function isUserHost(User $user): bool
    {
        $host = $this->getHostByUser($user);

        return $host !== null;
    }

    public static function create(
        HallRepositoryInterface $hallRepository,
        User $user,
        CreateHallPayload $payload
    ): self {
        $user->assertRegistered();

        $existingEvent = $hallRepository->findOneByIdentifier($payload->getIdentifier());

        if ($existingEvent !== null) {
            throw NotUniqueHallIdentifierException::createWithIdentifier($payload->getIdentifier());
        }

        $hall = new static(
            $payload->getIdentifier(),
            $payload->getName(),
            $payload->getDescription(),
        );

        $hall->addOwner(Host::createOwnerOfEventFromUser($hall, $user));

        return $hall;
    }

    private function addOwner(Host $host): self
    {
        if (!$host->isOwner()) {
            return $this;
        }

        $this->hosts[] = $host;

        return $this;
    }

    public function addRoom(
        User $user,
        AddRoomToHallPayload $payload
    ): void {
        $this->assertCanBeEditedByUser($user);

        $this->doAddRooms(Room::create(
            $this,
            $payload->getCreateRoomPayload()
        ));
    }

    public function assertCanBeEditedByUser(User $user): void
    {
        $user->assertRegistered();

        if (!$this->canBeEditedByUser($user)) {
            throw HallCannotBeEditedByUserException::createForUserAndHallIdentifier(
                $user,
                $this->identifier
            );
        }
    }

    public function remove(User $user): void
    {
        $this->assertCanBeEditedByUser($user);
        // TODO add store removed hall
    }

    public function update(
        User $user,
        UpdateHallPayload $payload
    ): void {
        $this->assertCanBeEditedByUser($user);

        $this->name = $payload->getName();
        $this->description = $payload->getDescription();
    }

    public function canBeEditedByUser(User $user): bool
    {
        $host = $this->getHostByUser($user);

        return $host !== null ? $host->isOwner() || $host->isAdministrator() : false;
    }

    private function getHostByUser(User $user): ?Host
    {
        foreach ($this->hosts as $host) {
            if ($host->isUser($user)) {
                return $host;
            }
        }

        return null;
    }

    private function doAddRooms(Room ...$rooms): self
    {
        foreach ($rooms as $room) {
            $this->rooms[] = $room;
            $this->addChildrenStore($room);
        }

        return $this;
    }

    public function dumpBy(User $user, ?HallDumpOptions $options = null): HallDump
    {
        $options = $options ?? HallDumpOptions::default();

        $dump = HallDump::create()
            ->withId($this->id)
            ->withDescription($this->description)
            ->withIdentifier($this->identifier)
            ->withName($this->name);

        $options
        && $options->isShowRooms()
        && $dump->withRooms(...array_map(
            fn(Room $room) => $room->dumpBy($user),
            array_filter(
                iterator_to_array($this->rooms),
                fn(Room $room) => $this->isUserHost($user)
                    || $room->isVisibleBy($user)
            )
        ));

        $dump->withHosts(...array_map(
            fn(Host $host) => $host->dumpBy($user),
            iterator_to_array($this->hosts)
        ));

        return $dump;
    }
}
