<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Model;

use Dvlpm\Stream\Domain\Space\Dump\HallCollectionDump;
use Dvlpm\Stream\Domain\Space\Dump\HallDumpOptions;
use Dvlpm\Stream\Domain\User\Model\User;

final class HallCollection
{
    /** @var Hall[] */
    private iterable $halls;

    public function __construct(Hall ...$halls)
    {
        $this->halls = $halls;
    }

    public function dumpBy(User $user, ?HallDumpOptions $options = null): HallCollectionDump
    {
        $hallDumps = [];

        foreach ($this->halls as $hall) {
            $hallDumps[] = $hall->dumpBy($user, $options);
        }

        return HallCollectionDump::create(...$hallDumps);
    }
}
