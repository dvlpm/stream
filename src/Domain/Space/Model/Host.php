<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Model;

use Dvlpm\Stream\Domain\Common\Model\TimestampableTrait;
use Dvlpm\Stream\Domain\Space\Dump\HostDump;
use Dvlpm\Stream\Domain\User\Model\User;

class Host
{
    use TimestampableTrait;

    private Hall $hall;
    private User $user;
    private HostRole $role;

    private function __construct(Hall $hall, User $user, HostRole $role)
    {
        $this->hall = $hall;
        $this->user = $user;
        $this->role = $role;
    }

    public static function createOwnerOfEventFromUser(Hall $hall, User $user): self
    {
        return new static($hall, $user, HostRole::OWNER());
    }

    public function isOwner(): bool
    {
        return $this->role === HostRole::OWNER();
    }

    public function isAdministrator(): bool
    {
        return $this->role === HostRole::ADMINISTRATOR();
    }

    public function isModerator(): bool
    {
        return $this->role === HostRole::MODERATOR();
    }

    public function isUser(User $user): bool
    {
        return $this->user->equals($user);
    }

    public function dumpBy(User $user): HostDump
    {
        return HostDump::create()
            ->withUser($this->user->dumpBy($user))
            ->withRole($this->role);
    }
}
