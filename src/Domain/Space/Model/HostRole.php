<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Model;

use Dvlpm\Stream\Domain\Common\Model\Enum;

/**
 * @method static static OWNER()
 * @method static static ADMINISTRATOR()
 * @method static static MODERATOR()
 */
final class HostRole extends Enum
{
    private const OWNER = 'OWNER';
    private const ADMINISTRATOR = 'ADMINISTRATOR';
    private const MODERATOR = 'MODERATOR';
}
