<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Model;

use Dvlpm\Stream\Domain\Common\Model\NotEmptyStringWrapperInterface;
use Dvlpm\Stream\Domain\Common\Model\NotEmptyStringWrapperTrait;

final class Identifier implements NotEmptyStringWrapperInterface
{
    use NotEmptyStringWrapperTrait;
}
