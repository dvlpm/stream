<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Model;

use Dvlpm\Stream\Domain\Common\Model\NotEmptyStringWrapperInterface;
use Dvlpm\Stream\Domain\Common\Model\NotEmptyStringWrapperTrait;

final class Key implements NotEmptyStringWrapperInterface
{
    use NotEmptyStringWrapperTrait;

    public static function generate(): self
    {
        return self::fromValue(static::doGenerate());
    }

    public function regenerate(): self
    {
        $this->value = static::doGenerate();

        return $this;
    }

    private static function doGenerate(): string
    {
        return base_convert(
            bin2hex(random_bytes(32)),
            16,
            36
        );
    }
}
