<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Model;

use Dvlpm\Stream\Domain\Common\Event\EventStoreInterface;
use Dvlpm\Stream\Domain\Common\Event\EventStoreTrait;
use Dvlpm\Stream\Domain\Common\Model\TimestampableTrait;
use Dvlpm\Stream\Domain\Space\Dump\ParticipantDump;
use Dvlpm\Stream\Domain\Space\Payload\CreateParticipantPayload;
use Dvlpm\Stream\Domain\User\Model\User;
use Ramsey\Uuid\UuidInterface;

class Participant implements EventStoreInterface
{
    use EventStoreTrait, TimestampableTrait;

    private ?UuidInterface $id;
    private Room $room;
    private ParticipantStatus $status;
    private ParticipantIdentification $identification;
    private ?User $user = null;

    private function __construct(Room $room, ParticipantIdentification $identification, ?UuidInterface $id = null)
    {
        $this->room = $room;
        $this->status = ParticipantStatus::PENDING();
        $this->identification = $identification;
        $this->id = $id;
    }

    public static function create(Room $room, CreateParticipantPayload $payload): self
    {
        return new static($room, $payload->getIdentification(), $payload->getId());
    }

    public function inviteUser(): void
    {
        // TODO https://jira.dvl.pm/browse/SPACE-24
    }

    public function assignUser(User $user): void
    {
        if ($this->user !== null || !$this->identification->matchesUser($user)) {
            return;
        }

        $this->status = ParticipantStatus::ACTIVE();
        $this->user = $user;
    }

    public function remove(User $user): void
    {
        $this->assertCanBeEditedByUser($user);
    }

    private function assertCanBeEditedByUser(User $user): void
    {
        $this->room->assertCanBeEditedByUser($user);
    }

    public function isVisibleBy(User $user): bool
    {
        if ($this->matchesUser($user)) {
            return true;
        }

        if ($this->isActive()) {
            return true;
        }

        return $this->canBeEditedByUser($user);
    }

    private function isActive(): bool
    {
        return $this->status === ParticipantStatus::ACTIVE();
    }

    private function canBeEditedByUser(User $user): bool
    {
        return $this->room->canBeEditedByUser($user);
    }

    public function hasSameIdentification(Participant $participant): bool
    {
        return $this->identification->equals($participant->identification);
    }

    public function matchesUser(User $user): bool
    {
        if ($this->user === null && $this->identification->matchesUser($user)) {
            return true;
        }

        return $this->isUser($user);
    }

    public function isUser(User $user): bool
    {
        return $this->user !== null ? $this->user->equals($user) : false;
    }

    public function dumpBy(User $user): ParticipantDump
    {
        return ParticipantDump::create()
            ->withId($this->id)
            ->withIdentification($this->identification->dumpBy($user))
            ->withStatus($this->status)
            ->withUser($this->user ? $this->user->dumpBy($user) : null);
    }
}
