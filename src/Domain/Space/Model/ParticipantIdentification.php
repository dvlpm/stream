<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Model;

use Dvlpm\Stream\Domain\Space\Dump\ParticipantIdentificationDump;
use Dvlpm\Stream\Domain\User\Model\User;
use Ramsey\Uuid\UuidInterface;

final class ParticipantIdentification
{
    private ?UuidInterface $userId = null;
    private ?string $email = null;

    private function __construct(?UuidInterface $userId = null, ?string $email = null)
    {
        $this->userId = $userId;
        $this->email = $email;
    }

    public static function fromUserId(UuidInterface $userId): self
    {
        return new static($userId);
    }

    public static function fromEmail(string $email): self
    {
        return new static(null, $email);
    }

    public function matchesUser(User $user): bool
    {
        if ($this->userId !== null) {
            return $this->userId->equals($user->getId());
        }

        if ($this->email !== null) {
            return $user->hasEmail($this->email);
        }

        return false;
    }

    public function equals(ParticipantIdentification $identification): bool
    {
        if ($this->userId !== null) {
            return $this->userId->equals($identification->userId);
        }

        if ($this->email !== null) {
            return $this->email === $identification->email;
        }

        return false;
    }

    public function __toString(): string
    {
        if ($this->userId !== null) {
            return (string) $this->userId;
        }

        if ($this->email !== null) {
            return (string) $this->email;
        }

        return '';
    }

    public function dumpBy(User $user): ParticipantIdentificationDump
    {
        return ParticipantIdentificationDump::create()
            ->withEmail($this->email)
            ->withUserId($this->userId);
    }
}
