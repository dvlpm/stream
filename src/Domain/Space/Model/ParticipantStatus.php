<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Model;

use Dvlpm\Stream\Domain\Common\Model\Enum;

/**
 * @method static static PENDING()
 * @method static static ACTIVE()
 */
final class ParticipantStatus extends Enum
{
    private const PENDING = 'PENDING';
    private const ACTIVE = 'ACTIVE';
}
