<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Model;

use Dvlpm\Stream\Domain\Common\Event\EventStoreInterface;
use Dvlpm\Stream\Domain\Common\Event\EventStoreTrait;
use Dvlpm\Stream\Domain\Common\Model\Name;
use Dvlpm\Stream\Domain\Common\Model\TimestampableTrait;
use Dvlpm\Stream\Domain\Content\Model\AbstractContent;
use Dvlpm\Stream\Domain\Space\Dump\RoomDump;
use Dvlpm\Stream\Domain\Space\Exception\RoomCannotBeViewedByUserException;
use Dvlpm\Stream\Domain\Space\Payload\AddParticipantToRoomPayload;
use Dvlpm\Stream\Domain\Space\Payload\CreateParticipantPayload;
use Dvlpm\Stream\Domain\Space\Payload\CreateRoomPayload;
use Dvlpm\Stream\Domain\Space\Payload\EnterRoomPayload;
use Dvlpm\Stream\Domain\Space\Payload\UpdateRoomPayload;
use Dvlpm\Stream\Domain\User\Model\User;
use Ramsey\Uuid\UuidInterface;

class Room implements EventStoreInterface
{
    use EventStoreTrait, TimestampableTrait;

    private ?UuidInterface $id;
    private Hall $hall;
    private Name $name;
    private AccessType $accessType;
    /** @var Participant[] */
    private iterable $participants = [];
    private ?string $description;
    private Key $key;
    /** @var AbstractContent[] */
    private iterable $contents = [];

    private function __construct(
        Hall $hall,
        Name $name,
        AccessType $accessType,
        ?string $description = null,
        ?UuidInterface $id = null
    ) {
        $this->hall = $hall;
        $this->name = $name;
        $this->accessType = $accessType;
        $this->description = $description;
        $this->id = $id;
        $this->key = Key::generate();
    }

    public static function create(
        Hall $hall,
        CreateRoomPayload $payload
    ): self {
        return new static(
            $hall,
            $payload->getName(),
            $payload->getAccessType(),
            $payload->getDescription(),
            $payload->getId()
        );
    }

    public function addParticipant(User $user, AddParticipantToRoomPayload $payload): void
    {
        $this->assertCanBeEditedByUser($user);

        $this->doAddParticipant(Participant::create($this, $payload->getCreateParticipantPayload()));
    }

    private function doAddParticipant(Participant ...$participants): self
    {
        foreach ($participants as $participant) {
            if ($this->hasParticipant($participant)) {
                continue;
            }
            $this->participants[] = $participant;
            $this->addChildrenStore($participant);
        }

        return $this;
    }

    public function addContent(User $user, AbstractContent $content): void
    {
        $this->assertCanBeEditedByUser($user);

        $this->doAddContent($content);
    }

    private function doAddContent(AbstractContent $content): self
    {
        if ($this->hasContent($content)) {
            return $this;
        }

        $this->contents[] = $content;
        $this->addChildrenStore($content);

        return $this;
    }

    public function removeContent(User $user, AbstractContent $content): void
    {
        $this->assertCanBeEditedByUser($user);

        $this->doRemoveContent($content);
    }

    private function doRemoveContent(AbstractContent $contentToRemove): self
    {
        foreach ($this->contents as $key => $content) {
            if ($content->sameAs($contentToRemove)) {
                unset($this->contents[$key]);
            }
        }

        return $this;
    }

    public function update(User $user, UpdateRoomPayload $payload): void
    {
        $this->assertCanBeEditedByUser($user);

        $this->name = $payload->getName();
        $this->accessType = $payload->getAccessType();
        $this->description = $payload->getDescription();
    }

    public function remove(User $user): void
    {
        $this->assertCanBeEditedByUser($user);
    }

    public function enter(User $user, EnterRoomPayload $payload): void
    {
        $participant = $this->findParticipantByUser($user);

        if ($participant !== null) {
            return;
        }

        $participant = $this->findMatchesParticipantByUser($user);

        if ($participant === null
            && (
                $this->isVisibleBy($user)
                || $this->key->equals($payload->getKey())
            )
        ) {
            $this->doAddParticipant($participant = Participant::create(
                $this,
                new CreateParticipantPayload(
                    ParticipantIdentification::fromUserId($user->getId())
                )
            ));
        }

        if ($participant === null) {
            return;
        }

        $participant->assignUser($user);
    }

    public function isUserParticipant(User $user): bool
    {
        return $this->findParticipantByUser($user) !== null;
    }

    private function findParticipantByUser(User $user): ?Participant
    {
        foreach ($this->participants as $participant) {
            if ($participant->isUser($user)) {
                return $participant;
            }
        }

        return null;
    }

    private function findMatchesParticipantByUser(User $user): ?Participant
    {
        foreach ($this->participants as $participant) {
            if ($participant->matchesUser($user)) {
                return $participant;
            }
        }

        return null;
    }

    private function hasContent(AbstractContent $contentToFind): bool
    {
        foreach ($this->contents as $key => $content) {
            if ($content->sameAs($contentToFind)) {
                return true;
            }
        }

        return false;
    }

    private function hasParticipant(Participant $otherParticipant): bool
    {
        foreach ($this->participants as $participant) {
            if ($participant->hasSameIdentification($otherParticipant)) {
                return true;
            }
        }

        return false;
    }

    public function assertCanBeEditedByUser(User $user): void
    {
        $this->hall->assertCanBeEditedByUser($user);
    }

    public function canBeEditedByUser(User $user): bool
    {
        return $this->hall->canBeEditedByUser($user);
    }

    public function isVisibleBy(User $user): bool
    {
        if ($this->canBeEditedByUser($user)) {
            return true;
        }

        if ($this->accessType === AccessType::ANONYMOUS()) {
            return true;
        }

        if ($this->accessType === AccessType::AUTHORIZED()
            && $user->isRegistered()) {
            return true;
        }

        return $this->isUserListed($user);
    }

    public function isUserListed(User $user): bool
    {
        foreach ($this->participants as $participant) {
            if ($participant->matchesUser($user)) {
                return true;
            }
        }

        return false;
    }

    public function dumpBy(User $user): RoomDump
    {
        if (!$this->isVisibleBy($user)) {
            throw RoomCannotBeViewedByUserException::createForUserAndRoomId($user, $this->id);
        }

        $dump = RoomDump::create();

        if ($this->canBeEditedByUser($user)) {
            $dump->withKey($this->key);
        }

        return $dump
            ->withId($this->id)
            ->withName($this->name)
            ->withAccessType($this->accessType)
            ->withDescription($this->description)
            ->withContents(array_map(
                fn(AbstractContent $content) => $content->dumpBy($user),
                iterator_to_array($this->contents)
            ))
            ->withParticipants(array_values(array_map(
                fn(Participant $participant) => $participant->dumpBy($user),
                array_filter(
                    iterator_to_array($this->participants),
                    fn(Participant $participant) => $participant->isVisibleBy($user)
                )
            )));
    }
}
