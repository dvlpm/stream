<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Payload;

final class AddParticipantToRoomPayload
{
    private CreateParticipantPayload $createParticipantPayload;

    public function __construct(CreateParticipantPayload $createParticipantPayload)
    {
        $this->createParticipantPayload = $createParticipantPayload;
    }

    public function getCreateParticipantPayload(): CreateParticipantPayload
    {
        return $this->createParticipantPayload;
    }
}
