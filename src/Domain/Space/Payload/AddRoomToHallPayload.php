<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Payload;

final class AddRoomToHallPayload
{
    private CreateRoomPayload $createRoomPayload;

    public function __construct(CreateRoomPayload $createRoomPayload)
    {
        $this->createRoomPayload = $createRoomPayload;
    }

    public function getCreateRoomPayload(): CreateRoomPayload
    {
        return $this->createRoomPayload;
    }
}
