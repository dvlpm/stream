<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Payload;

use Dvlpm\Stream\Domain\Common\Model\Name;
use Dvlpm\Stream\Domain\Space\Model\Identifier;

final class CreateHallPayload
{
    private Identifier $identifier;
    private ?Name $name;
    private ?string $description;

    public function __construct(
        Identifier $identifier,
        ?Name $name,
        ?string $description
    ) {
        $this->identifier = $identifier;
        $this->name = $name;
        $this->description = $description;
    }

    public function getIdentifier(): Identifier
    {
        return $this->identifier;
    }

    public function getName(): ?Name
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }
}
