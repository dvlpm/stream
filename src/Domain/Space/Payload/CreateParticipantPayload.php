<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Payload;

use Dvlpm\Stream\Domain\Space\Model\ParticipantIdentification;
use Ramsey\Uuid\UuidInterface;

final class CreateParticipantPayload
{
    private ParticipantIdentification $identification;
    private ?UuidInterface $id;

    public function __construct(ParticipantIdentification $identification, ?UuidInterface $id = null)
    {
        $this->identification = $identification;
        $this->id = $id;
    }

    public function getIdentification(): ParticipantIdentification
    {
        return $this->identification;
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }
}
