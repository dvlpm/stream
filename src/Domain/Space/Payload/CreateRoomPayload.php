<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Payload;

use Dvlpm\Stream\Domain\Common\Model\Name;
use Dvlpm\Stream\Domain\Space\Model\AccessType;
use Ramsey\Uuid\UuidInterface;

final class CreateRoomPayload
{
    private Name $name;
    private AccessType $accessType;
    private ?string $description;
    private ?UuidInterface $id;

    public function __construct(
        Name $name,
        AccessType $accessType,
        ?string $description,
        ?UuidInterface $id
    ) {
        $this->name = $name;
        $this->accessType = $accessType;
        $this->description = $description;
        $this->id = $id;
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function getAccessType(): AccessType
    {
        return $this->accessType;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }
}
