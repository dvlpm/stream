<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Payload;

use Dvlpm\Stream\Domain\Space\Model\Key;

final class EnterRoomPayload
{
    private ?Key $key;

    public function __construct(?Key $key = null)
    {
        $this->key = $key;
    }

    public function getKey(): ?Key
    {
        return $this->key;
    }
}
