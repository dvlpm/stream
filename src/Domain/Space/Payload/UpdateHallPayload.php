<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Payload;

use Dvlpm\Stream\Domain\Common\Model\Name;

final class UpdateHallPayload
{
    private ?Name $name;
    private ?string $description;

    public function __construct(
        ?Name $name,
        ?string $description
    ) {
        $this->name = $name;
        $this->description = $description;
    }

    public function getName(): ?Name
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }
}
