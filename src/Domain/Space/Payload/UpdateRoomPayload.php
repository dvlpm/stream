<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Payload;

use Dvlpm\Stream\Domain\Common\Model\Name;
use Dvlpm\Stream\Domain\Space\Model\AccessType;

final class UpdateRoomPayload
{
    private Name $name;
    private AccessType $accessType;
    private ?string $description;

    public function __construct(
        Name $name,
        AccessType $accessType,
        ?string $description
    ) {
        $this->name = $name;
        $this->accessType = $accessType;
        $this->description = $description;
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function getAccessType(): AccessType
    {
        return $this->accessType;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }
}
