<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Repository;

final class HallCriteria
{
    private ?HostCriteria $hostCriteria;

    public function __construct(?HostCriteria $hostCriteria = null)
    {
        $this->hostCriteria = $hostCriteria;
    }

    public function getHostCriteria(): ?HostCriteria
    {
        return $this->hostCriteria;
    }
}
