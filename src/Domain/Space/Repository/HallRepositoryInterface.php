<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Repository;

use Dvlpm\Stream\Domain\Space\Exception\UndefinedHallException;
use Dvlpm\Stream\Domain\Space\Model\Hall;
use Dvlpm\Stream\Domain\Space\Model\HallCollection;
use Dvlpm\Stream\Domain\Space\Model\Identifier;

interface HallRepositoryInterface
{
    public function findAllByCriteria(HallCriteria $hallCriteria): HallCollection;
    public function findOneByIdentifier(Identifier $identifier): ?Hall;
    /**
     * @param Identifier $identifier
     * @return Hall
     *@throws UndefinedHallException
     */
    public function findOneByIdentifierOrFail(Identifier $identifier): Hall;
    public function remove(Hall $hall): void;
    public function save(Hall $hall): void;
}
