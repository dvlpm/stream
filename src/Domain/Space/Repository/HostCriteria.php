<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Repository;

use Dvlpm\Stream\Domain\Space\Model\HostRole;
use Ramsey\Uuid\UuidInterface;

final class HostCriteria
{
    private ?UuidInterface $userId;
    /** @var HostRole[] */
    private iterable $roles;

    public function __construct(?UuidInterface $userId = null, iterable $roles = [])
    {
        $this->userId = $userId;
        $this->roles = $roles;
    }

    public function getUserId(): ?UuidInterface
    {
        return $this->userId;
    }

    public function getRoles(): iterable
    {
        return $this->roles;
    }
}
