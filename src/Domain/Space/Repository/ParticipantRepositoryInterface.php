<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Repository;

use Dvlpm\Stream\Domain\Space\Model\Participant;
use Ramsey\Uuid\UuidInterface;

interface ParticipantRepositoryInterface
{
    public function findOneById(UuidInterface $id): ?Participant;
    public function remove(Participant $participant): void;
}
