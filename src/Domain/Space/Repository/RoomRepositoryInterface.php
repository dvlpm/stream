<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\Space\Repository;

use Dvlpm\Stream\Domain\Space\Exception\UndefinedRoomException;
use Dvlpm\Stream\Domain\Space\Model\Room;
use Ramsey\Uuid\UuidInterface;

interface RoomRepositoryInterface
{
    public function findOneById(UuidInterface $id): ?Room;
    /**
     * @param UuidInterface $id
     * @return Room
     * @throws UndefinedRoomException
     */
    public function findOneByIdOrFail(UuidInterface $id): Room;
    public function remove(Room $room): void;
}
