<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\User\Dump;

use ArrayObject;

final class UserCollectionDump extends ArrayObject
{
    public static function create(UserDump ...$dumps): self
    {
        return new static($dumps);
    }
}
