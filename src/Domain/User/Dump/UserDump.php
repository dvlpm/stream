<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\User\Dump;

use Dvlpm\Stream\Domain\Common\Model\Meta;
use Dvlpm\Stream\Domain\Common\Model\Name;
use Dvlpm\Stream\Domain\User\Model\ExternalId;
use Dvlpm\Stream\Domain\User\Model\Username;
use Dvlpm\Stream\Domain\User\Model\UserRole;
use Ramsey\Uuid\UuidInterface;

final class UserDump
{
    private ?UuidInterface $id = null;
    private ?UserRole $role = null;
    private ?Meta $meta = null;
    private ?ExternalId $externalId = null;
    private ?Name $name = null;
    private ?Username $username = null;

    public static function create(): self
    {
        return new static();
    }

    public function withId(?UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function withRole(?UserRole $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function withMeta(?Meta $meta): self
    {
        $this->meta = $meta;

        return $this;
    }

    public function withExternalId(?ExternalId $externalId): self
    {
        $this->externalId = $externalId;

        return $this;
    }

    public function withName(?Name $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function withUsername(?Username $username): self
    {
        $this->username = $username;

        return $this;
    }
}
