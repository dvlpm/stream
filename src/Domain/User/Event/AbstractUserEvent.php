<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\User\Event;

use Dvlpm\Stream\Domain\User\Model\User;

abstract class AbstractUserEvent implements UserEventInterface
{
    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
