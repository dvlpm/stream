<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\User\Event;

final class UserCreatedEvent extends AbstractUserEvent
{
}
