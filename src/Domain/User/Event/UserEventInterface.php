<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\User\Event;

use Dvlpm\Stream\Domain\Common\Event\DomainEventInterface;

interface UserEventInterface extends DomainEventInterface
{
}
