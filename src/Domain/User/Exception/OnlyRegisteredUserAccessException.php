<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\User\Exception;

use Dvlpm\Stream\Domain\Common\Exception\AbstractAccessException;

final class OnlyRegisteredUserAccessException extends AbstractAccessException
{
    public static function create(): self
    {
        return new static('User not registered!');
    }
}
