<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\User\Exception;

use Dvlpm\Stream\Domain\Common\Exception\AbstractUndefinedException;

final class UndefinedUserException extends AbstractUndefinedException
{
    public static function create(): self
    {
        return new static('Undefined user');
    }
}
