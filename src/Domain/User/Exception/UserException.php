<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\User\Exception;

use Dvlpm\Stream\Domain\User\Model\ExternalId;
use Dvlpm\Stream\Domain\User\Model\Username;
use Exception;
use Ramsey\Uuid\UuidInterface;

final class UserException extends Exception
{
    public static function notUniqueUsername(Username $username): self
    {
        return new static("Not unique username: $username");
    }

    public static function notUniqueExternalId(ExternalId $externalId): self
    {
        return new static("Not unique external id: $externalId");
    }

    public static function invalidId(UuidInterface $id): self
    {
        return new static("Invalid id: $id");
    }
}
