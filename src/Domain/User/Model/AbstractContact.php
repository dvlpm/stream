<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\User\Model;

use Dvlpm\Stream\Domain\Common\Event\EventStoreInterface;
use Dvlpm\Stream\Domain\Common\Event\EventStoreTrait;
use Dvlpm\Stream\Domain\Common\Model\TimestampableTrait;
use Ramsey\Uuid\UuidInterface;

abstract class AbstractContact implements EventStoreInterface
{
    use EventStoreTrait, TimestampableTrait;

    protected ?UuidInterface $id = null;
    protected User $user;
    protected bool $isPreferable;

    protected function __construct(User $user, bool $isPreferable, ?UuidInterface $id = null)
    {
        $this->user = $user;
        $this->isPreferable = $isPreferable;
        $this->id = $id;
    }
}
