<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\User\Model;

use Dvlpm\Stream\Domain\User\Payload\CreateEmailPayload;
use Ramsey\Uuid\UuidInterface;

class Email extends AbstractContact
{
    private string $email;
    private bool $isConfirmed;

    private function __construct(
        User $user,
        string $email,
        bool $isConfirmed = false,
        bool $isPreferable = false,
        ?UuidInterface $id = null
    ) {
        parent::__construct($user, $isPreferable, $id);
        $this->email = $email;
        $this->isConfirmed = $isConfirmed;
    }

    public static function create(User $user, CreateEmailPayload $payload): self
    {
        return new static(
            $user,
            $payload->getEmail(),
            $payload->isConfirmed()
        );
    }

    public function isEqual(string $email): bool
    {
        return $this->email === $email;
    }

    public function isConfirmed(): bool
    {
        return $this->isConfirmed;
    }
}
