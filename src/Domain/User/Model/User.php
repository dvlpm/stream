<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\User\Model;

use Dvlpm\Stream\Domain\Common\Event\EventStoreInterface;
use Dvlpm\Stream\Domain\Common\Event\EventStoreTrait;
use Dvlpm\Stream\Domain\Common\Model\Meta;
use Dvlpm\Stream\Domain\Common\Model\Name;
use Dvlpm\Stream\Domain\Common\Model\TimestampableTrait;
use Dvlpm\Stream\Domain\User\Dump\UserDump;
use Dvlpm\Stream\Domain\User\Exception\OnlyRegisteredUserAccessException;
use Dvlpm\Stream\Domain\User\Exception\UserException;
use Dvlpm\Stream\Domain\User\Payload\CreateEmailPayload;
use Dvlpm\Stream\Domain\User\Payload\CreateGuestPayload;
use Dvlpm\Stream\Domain\User\Payload\RegisterPayload;
use Dvlpm\Stream\Domain\User\Payload\UpdateUserPayload;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class User implements EventStoreInterface
{
    use EventStoreTrait, TimestampableTrait;

    private UuidInterface $id;
    private UserRole $role;
    private Meta $meta;
    private ?ExternalId $externalId = null;
    /** @var AbstractContact[] */
    private iterable $contacts = [];
    private ?Name $name = null;
    private ?Username $username = null;

    private function __construct(UserRole $role, Meta $meta, ?UuidInterface $id = null)
    {
        $this->role = $role;
        $this->meta = $meta;
        $this->id = $id ?? Uuid::uuid4();
    }

    public static function createGuest(CreateGuestPayload $payload): self
    {
        return new static(
            UserRole::GUEST(),
            Meta::empty(),
            $payload->getGuestId(),
        );
    }

    public static function createRegistered(RegisterPayload $payload): self
    {
        $user = new static(
            UserRole::GUEST(),
            Meta::empty(),
            $payload->getId(),
        );

        $user->doRegister($payload);

        return $user;
    }

    public function register(RegisterPayload $payload): void
    {
        $this->doRegister($payload);
    }

    private function doRegister(RegisterPayload $payload): void
    {
        $this->assertSameId($payload->getId());

        $this->role = UserRole::USER();
        $this->externalId = $payload->getExternalId();
        $this->meta = $this->meta->withMeta($payload->getMeta());
    }

    private function assertSameId(UuidInterface $id): void
    {
        if (!$this->id->equals($id)) {
            throw UserException::invalidId($id);
        }
    }

    public function update(UpdateUserPayload $payload): void
    {
        $this->username = $payload->getUsername() ?? $this->username;
        $this->name = $payload->getName() ?? $this->name;
        $this->meta = $this->meta->withMeta($payload->getMeta());
    }

    public function addEmail(CreateEmailPayload $payload): void
    {
        $this->doAddContacts(Email::create(
            $this,
            $payload
        ));
    }

    private function doAddContacts(AbstractContact ...$contacts): self
    {
        foreach ($contacts as $contact) {
            $this->contacts[] = $contact;
            $this->addChildrenStore($contact);
        }

        return $this;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function isRegistered(): bool
    {
        return $this->role === UserRole::USER();
    }

    public function assertRegistered(): void
    {
        if ($this->isRegistered()) {
            return;
        }

        throw OnlyRegisteredUserAccessException::create();
    }

    public function equals(User $user): bool
    {
        return $this->hasId($user->id);
    }

    public function hasId(UuidInterface $id): bool
    {
        return $this->id->equals($id);
    }

    public function hasExternalId(ExternalId $externalId): bool
    {
        if ($this->externalId === null) {
            return false;
        }

        return $this->externalId->equals($externalId);
    }

    public function hasEmail(string $email): bool
    {
        $hasEmail = false;

        foreach ($this->contacts as $contact) {
            if ($hasEmail) {
                continue;
            }

            if ($contact instanceof Email && $contact->isConfirmed()) {
                $hasEmail = $contact->isEqual($email);
            }
        }

        return $hasEmail;
    }

    public function dumpBy(User $user): UserDump
    {
        $dump = UserDump::create()
            ->withId($this->id)
            ->withName($this->name)
            ->withUsername($this->username);

        if ($user->equals($user)) {
            $dump->withExternalId($this->externalId)
                ->withMeta($this->meta)
                ->withRole($this->role);
        }

        return $dump;
    }
}
