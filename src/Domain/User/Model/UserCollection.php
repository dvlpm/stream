<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\User\Model;

use Dvlpm\Stream\Domain\User\Dump\UserCollectionDump;

final class UserCollection
{
    /** @var User[] */
    private iterable $users;

    public function __construct(User ...$users)
    {
        $this->users = $users;
    }

    public function dumpBy(User $user): UserCollectionDump
    {
        $userDumps = [];

        foreach ($this->users as $userToDump) {
            $userDumps[] = $userToDump->dumpBy($user);
        }

        return UserCollectionDump::create(...$userDumps);
    }
}
