<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\User\Model;

use Dvlpm\Stream\Domain\Common\Model\Enum;

/**
 * @method static static GUEST()
 * @method static static USER()
 */
final class UserRole extends Enum
{
    private const GUEST = 'GUEST';
    private const USER = 'USER';
}
