<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\User\Model;

use Dvlpm\Stream\Domain\Common\Model\NotEmptyStringWrapperInterface;
use Dvlpm\Stream\Domain\Common\Model\NotEmptyStringWrapperTrait;

final class Username implements NotEmptyStringWrapperInterface
{
    use NotEmptyStringWrapperTrait;
}
