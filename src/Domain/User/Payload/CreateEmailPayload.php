<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\User\Payload;

final class CreateEmailPayload
{
    private string $email;
    private bool $isConfirmed;

    public function __construct(string $email, bool $isConfirmed = false)
    {
        $this->email = $email;
        $this->isConfirmed = $isConfirmed;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function isConfirmed(): bool
    {
        return $this->isConfirmed;
    }
}
