<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\User\Payload;

use Ramsey\Uuid\UuidInterface;

final class CreateGuestPayload
{
    private UuidInterface $guestId;

    public function __construct(UuidInterface $guestId)
    {
        $this->guestId = $guestId;
    }

    public function getGuestId(): UuidInterface
    {
        return $this->guestId;
    }
}
