<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\User\Payload;

use Dvlpm\Stream\Domain\Common\Model\Meta;
use Dvlpm\Stream\Domain\User\Model\ExternalId;
use Ramsey\Uuid\UuidInterface;

final class RegisterPayload
{
    private ExternalId $externalId;
    private Meta $meta;
    private ?UuidInterface $id;

    public function __construct(ExternalId $externalId, Meta $meta, ?UuidInterface $id = null)
    {
        $this->externalId = $externalId;
        $this->meta = $meta;
        $this->id = $id;
    }

    public function getExternalId(): ExternalId
    {
        return $this->externalId;
    }

    public function getMeta(): Meta
    {
        return $this->meta;
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }
}
