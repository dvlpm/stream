<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\User\Payload;

use Dvlpm\Stream\Domain\Common\Model\Meta;
use Dvlpm\Stream\Domain\Common\Model\Name;
use Dvlpm\Stream\Domain\User\Model\Username;

final class UpdateUserPayload
{
    private Meta $meta;
    private ?Name $name;
    private ?Username $username;

    public function __construct(Meta $meta, ?Name $name = null, ?Username $username = null)
    {
        $this->meta = $meta;
        $this->name = $name;
        $this->username = $username;
    }

    public function getMeta(): Meta
    {
        return $this->meta;
    }

    public function getName(): ?Name
    {
        return $this->name;
    }

    public function getUsername(): ?Username
    {
        return $this->username;
    }
}
