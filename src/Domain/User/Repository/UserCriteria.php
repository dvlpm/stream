<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\User\Repository;

final class UserCriteria
{
    private ?string $searchQuery;
    private int $limit;
    private int $offset;

    public function __construct(?string $searchQuery = null, ?int $limit = null, ?int $offset = null)
    {
        $this->searchQuery = $searchQuery;
        $this->limit = $limit ?? 10;
        $this->offset = $offset ?? 0;
    }

    public function getSearchQuery(): ?string
    {
        return $this->searchQuery;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }
}
