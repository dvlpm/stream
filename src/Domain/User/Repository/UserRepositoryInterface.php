<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Domain\User\Repository;

use Dvlpm\Stream\Domain\User\Exception\UndefinedUserException;
use Dvlpm\Stream\Domain\User\Model\ExternalId;
use Dvlpm\Stream\Domain\User\Model\User;
use Dvlpm\Stream\Domain\User\Model\UserCollection;
use Dvlpm\Stream\Domain\User\Model\Username;
use Ramsey\Uuid\UuidInterface;

interface UserRepositoryInterface
{
    public function findAllByCriteria(UserCriteria $criteria): UserCollection;
    public function findOneById(UuidInterface $id): ?User;
    /**
     * @param UuidInterface $id
     * @throws UndefinedUserException
     * @return User
     */
    public function findOneByIdOrFail(UuidInterface $id): User;
    public function findOneByExternalId(ExternalId $externalId): ?User;
    public function findOneByUsername(Username $username): ?User;
    public function save(User $user): void;
}
