<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\ArgumentValueResolver;

use Dvlpm\Stream\Infrastructure\Exception\DtoArgumentValueResolverException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class DtoArgumentValueResolver implements ArgumentValueResolverInterface
{
    private array $suitableDtoTypes;
    private SerializerInterface $serializer;
    private ValidatorInterface $validator;

    public function __construct(
        array $suitableDtoTypes,
        SerializerInterface $serializer,
        ValidatorInterface $validator
    ) {
        $this->suitableDtoTypes = $suitableDtoTypes;
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return in_array($argument->getType(), $this->suitableDtoTypes, true);
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        if ($argument->isVariadic()) {
            return $this->doResolveVariadic($request, $argument);
        }

        return $this->doResolve($request, $argument);
    }

    private function doResolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $dto = $this->doDeserialize($request->getContent(), $argument->getType());

        $this->doValidate($dto);

        yield $dto;
    }

    private function doResolveVariadic(Request $request, ArgumentMetadata $argument): iterable
    {
        $type = $argument->getType() . '[]';

        $dtos = $this->doDeserialize($request->getContent(), $type);

        foreach ($dtos as $dto) {
            $this->doValidate($dto);

            yield $dto;
        }
    }

    private function doDeserialize(string $content, string $type)
    {
        try {
            return $this->serializer->deserialize(
                $content,
                $type,
                JsonEncoder::FORMAT
            );
        } catch (\Throwable $throwable) {
            throw DtoArgumentValueResolverException::undeserializableData();
        }
    }

    private function doValidate($dto): void
    {
        $errors = $this->validator->validate($dto);
        if (count($errors) > 0) {
            throw DtoArgumentValueResolverException::withErrors($errors);
        }
    }
}
