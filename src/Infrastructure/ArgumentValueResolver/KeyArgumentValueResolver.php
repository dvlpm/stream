<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\ArgumentValueResolver;

use Dvlpm\Stream\Domain\Space\Model\Key;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

final class KeyArgumentValueResolver implements ArgumentValueResolverInterface
{
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return $argument->getType() === Key::class && !empty($request->get('key'));
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $keyString = $request->get('key');

        yield Key::fromValue($keyString);
    }
}
