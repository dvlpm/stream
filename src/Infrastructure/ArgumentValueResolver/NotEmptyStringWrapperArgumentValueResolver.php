<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\ArgumentValueResolver;

use Dvlpm\Stream\Domain\Common\Model\NotEmptyStringWrapperInterface;
use Dvlpm\Stream\Infrastructure\Serializer\Normalizer\NotEmptyStringWrapperNormalizer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

final class NotEmptyStringWrapperArgumentValueResolver implements ArgumentValueResolverInterface
{
    private NotEmptyStringWrapperNormalizer $normalizer;

    public function __construct(NotEmptyStringWrapperNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return is_subclass_of($argument->getType(), NotEmptyStringWrapperInterface::class);
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $normalizedValue = $request->get($argument->getName());

        yield $normalizedValue !== null ? $this->normalizer->denormalize(
            $normalizedValue,
            $argument->getType(),
            JsonEncoder::FORMAT
        ) : null;
    }
}
