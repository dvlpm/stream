<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\ArgumentValueResolver;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

final class RequestParamArgumentValueResolver implements ArgumentValueResolverInterface
{
    private const SUPPORTED_TYPES = [
        'string',
        'int',
        'float',
        'bool',
    ];

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return
            in_array($argument->getType(), self::SUPPORTED_TYPES, true)
            && $request->getMethod() === Request::METHOD_GET
            && !empty($request->get($argument->getName()));
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        yield $request->get($argument->getName());
    }
}
