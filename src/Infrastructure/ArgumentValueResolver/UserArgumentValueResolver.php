<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\ArgumentValueResolver;

use Dvlpm\Stream\Domain\User\Model\User;
use Dvlpm\Stream\Infrastructure\Authentication\UserResolver\UserResolverInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

final class UserArgumentValueResolver implements ArgumentValueResolverInterface
{
    private UserResolverInterface $userResolver;

    public function __construct(UserResolverInterface $userResolver)
    {
        $this->userResolver = $userResolver;
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return $argument->getType() === User::class;
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        yield $this->userResolver->resolve($request);
    }
}
