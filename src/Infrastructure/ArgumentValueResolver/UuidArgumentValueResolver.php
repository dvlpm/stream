<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\ArgumentValueResolver;

use Dvlpm\Stream\Infrastructure\Serializer\Normalizer\UuidNormalizer;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

final class UuidArgumentValueResolver implements ArgumentValueResolverInterface
{
    private UuidNormalizer $normalizer;

    public function __construct(UuidNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return $argument->getType() === UuidInterface::class;
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $normalizedValue = $request->get($argument->getName());

        yield $this->normalizer->denormalize(
            $normalizedValue,
            $argument->getType(),
            JsonEncoder::FORMAT
        );
    }
}
