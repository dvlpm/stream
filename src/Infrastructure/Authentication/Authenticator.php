<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Authentication;

use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\UserInfo;
use Dvlpm\Stream\Infrastructure\Authentication\UserInfoRetrievingStrategy\UserInfoRetrievingStrategyInterface;
use Throwable;

final class Authenticator
{
    private UserInfoRetrievingStrategyInterface $strategy;

    public function __construct(UserInfoRetrievingStrategyInterface $strategy)
    {
        $this->strategy = $strategy;
    }

    public function authenticateByAccessToken(AccessToken $accessToken): ?UserInfo
    {
        try {
            $userInfo = $this->strategy->retrieveUserInfo($accessToken);
        } catch (Throwable $throwable) {
            return null;
        }

        return $userInfo;
    }
}
