<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Authentication\Builder;

use Dvlpm\Stream\Domain\Common\Model\Meta;
use Dvlpm\Stream\Domain\User\Model\ExternalId;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\UserInfo;

final class UserInfoBuilder
{
    private array $userInfoArray;

    public static function create(): self
    {
        return new static();
    }

    public function withUserInfoArray(array $userInfoArray): self
    {
        $this->userInfoArray = $userInfoArray;

        return $this;
    }

    public function build(): UserInfo
    {
        if (empty($this->userInfoArray)) {
            throw UserInfoBuilderException::emptyData();
        }

        $externalIdValue = $this->userInfoArray['sub'] ?? null;
        if ($externalIdValue === null) {
            throw UserInfoBuilderException::emptyProperty('externalValueId');
        }

        $externalId = ExternalId::fromValue($externalIdValue);
        $meta = Meta::fromData($this->userInfoArray);

        return new UserInfo($externalId, $meta);
    }
}
