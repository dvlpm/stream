<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Authentication\Builder;

use Exception;

final class UserInfoBuilderException extends Exception
{
    public static function emptyData(): self
    {
        return new static('Empty data provided');
    }

    public static function emptyProperty(string $propertyName): self
    {
        return new static("Property '$propertyName' must not be empty");
    }
}
