<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Authentication\DataObject;

use Dvlpm\Stream\Domain\Common\Model\NotEmptyStringWrapperInterface;
use Dvlpm\Stream\Domain\Common\Model\NotEmptyStringWrapperTrait;

final class AccessToken implements NotEmptyStringWrapperInterface
{
    use NotEmptyStringWrapperTrait;
}
