<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Authentication\DataObject;

use Dvlpm\Stream\Domain\Common\Model\Meta;
use Dvlpm\Stream\Domain\User\Model\ExternalId;

final class UserInfo
{
    private ExternalId $externalId;
    private Meta $meta;

    public function __construct(ExternalId $externalId, Meta $meta)
    {
        $this->externalId = $externalId;
        $this->meta = $meta;
    }

    public function getExternalId(): ExternalId
    {
        return $this->externalId;
    }

    public function getMeta(): Meta
    {
        return $this->meta;
    }
}

