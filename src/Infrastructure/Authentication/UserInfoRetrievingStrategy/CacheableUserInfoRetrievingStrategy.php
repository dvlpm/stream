<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Authentication\UserInfoRetrievingStrategy;

use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\UserInfo;
use Symfony\Component\Cache\Adapter\AdapterInterface;

final class CacheableUserInfoRetrievingStrategy implements UserInfoRetrievingStrategyInterface
{
    private UserInfoRetrievingStrategyInterface $userInfoRetrievingStrategy;
    private AdapterInterface $cacheItemPool;
    private int $cacheTtlInSeconds;

    public function __construct(
        UserInfoRetrievingStrategyInterface $userInfoRetrievingStrategy,
        AdapterInterface $cacheItemPool,
        int $cacheTtlInSeconds
    ) {
        $this->userInfoRetrievingStrategy = $userInfoRetrievingStrategy;
        $this->cacheItemPool = $cacheItemPool;
        $this->cacheTtlInSeconds = $cacheTtlInSeconds;
    }

    public function retrieveUserInfo(AccessToken $accessToken): ?UserInfo
    {
        $cacheItem = $this->cacheItemPool->getItem((string) $accessToken);

        if ($cacheItem->isHit() === false) {
            $userInfo = $this->userInfoRetrievingStrategy->retrieveUserInfo($accessToken);

            $cacheItem->set($userInfo);
            $cacheItem->expiresAfter(\DateInterval::createFromDateString($this->cacheTtlInSeconds . ' sec'));

            $this->cacheItemPool->save($cacheItem);
        }

        return $cacheItem->get();
    }
}

