<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Authentication\UserInfoRetrievingStrategy;

use Auth0\SDK\API\Authentication;
use Dvlpm\Stream\Infrastructure\Authentication\Builder\UserInfoBuilder;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\UserInfo;

final class DirectUserInfoRetrievingStrategy implements UserInfoRetrievingStrategyInterface
{
    private Authentication $authentication;

    public function __construct(Authentication $authentication)
    {
        $this->authentication = $authentication;
    }

    public function retrieveUserInfo(AccessToken $accessToken): UserInfo
    {
        $userInfoArray = $this->authentication->userinfo((string) $accessToken);

        return UserInfoBuilder::create()
            ->withUserInfoArray($userInfoArray)
            ->build();
    }
}
