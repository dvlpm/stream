<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Authentication\UserInfoRetrievingStrategy;

use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\UserInfo;

interface UserInfoRetrievingStrategyInterface
{
    public function retrieveUserInfo(AccessToken $accessToken): ?UserInfo;
}
