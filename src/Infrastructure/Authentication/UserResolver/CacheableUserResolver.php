<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Authentication\UserResolver;

use Dvlpm\Stream\Domain\User\Model\User;
use Symfony\Component\HttpFoundation\Request;

final class CacheableUserResolver implements SpecifiableUserResolverInterface
{
    private ?bool $isSuitableFor = null;
    private ?User $user = null;

    private UserResolverInterface $userResolver;

    public function __construct(UserResolverInterface $userResolver)
    {
        $this->userResolver = $userResolver;
    }

    public function isSuitableFor(Request $request): bool
    {
        if ($this->isSuitableFor === null) {
            $this->isSuitableFor = $this->userResolver->isSuitableFor($request);
        }

        return $this->isSuitableFor;
    }

    public function resolve(Request $request): User
    {
        if ($this->user === null) {
            $this->user = $this->userResolver->resolve($request);
        }

        return $this->user;
    }
}
