<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Authentication\UserResolver;

use Dvlpm\Stream\Domain\User\Model\User;
use Symfony\Component\HttpFoundation\Request;

final class CompositeUserResolver implements UserResolverInterface
{
    /** @var SpecifiableUserResolverInterface[] */
    private iterable $userResolvers;

    public function __construct($userResolvers)
    {
        $this->userResolvers = $userResolvers;
    }

    public function resolve(Request $request): User
    {
        foreach ($this->userResolvers as $resolver) {
            if ($resolver->isSuitableFor($request)) {
                return $resolver->resolve($request);
            }
        }

        throw UserResolverException::unableToResolve();
    }
}
