<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Authentication\UserResolver;

use Dvlpm\Stream\Application\Service\UserService;
use Dvlpm\Stream\Domain\Common\Model\Meta;
use Dvlpm\Stream\Domain\User\Model\ExternalId;
use Dvlpm\Stream\Domain\User\Model\User;
use Dvlpm\Stream\Domain\User\Payload\CreateGuestPayload;
use Dvlpm\Stream\Domain\User\Payload\RegisterPayload;
use Dvlpm\Stream\Domain\User\Repository\UserRepositoryInterface;
use Dvlpm\Stream\Infrastructure\Request\IdExtractor\IdExtractorInterface;
use Dvlpm\Stream\Infrastructure\Request\TokenExtractor\AccessTokenExtractorInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;

final class FakeUserResolver implements SpecifiableUserResolverInterface
{
    private UserRepositoryInterface $userRepository;
    private IdExtractorInterface $idExtractor;
    private AccessTokenExtractorInterface $accessTokenExtractor;
    private UserService $userService;
    private bool $isEnabled;

    public function __construct(
        UserRepositoryInterface $userRepository,
        IdExtractorInterface $idExtractor,
        AccessTokenExtractorInterface $accessTokenExtractor,
        UserService $userService,
        bool $isEnabled
    ) {
        $this->userRepository = $userRepository;
        $this->idExtractor = $idExtractor;
        $this->accessTokenExtractor = $accessTokenExtractor;
        $this->userService = $userService;
        $this->isEnabled = $isEnabled;
    }

    public function isSuitableFor(Request $request): bool
    {
        return $this->isEnabled;
    }

    public function resolve(Request $request): User
    {
        $id = $this->idExtractor->extract($request);
        if ($id === null) {
            return User::createGuest(new CreateGuestPayload(Uuid::uuid4()));
        }

        $user = $this->userRepository->findOneById($id);

        if ($user !== null) {
            return $user;
        }

        $accessToken = $this->accessTokenExtractor->extract($request);

        if ($accessToken === null) {
            return User::createGuest(new CreateGuestPayload($id));
        }

        return $this->userService->createRegisteredUser(new RegisterPayload(
            ExternalId::fromValue('fake|external_id' . random_int(0, 9)),
            Meta::empty(),
            $id
        ));
    }
}
