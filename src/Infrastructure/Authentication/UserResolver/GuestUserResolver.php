<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Authentication\UserResolver;

use Dvlpm\Stream\Application\Service\UserService;
use Dvlpm\Stream\Domain\User\Model\User;
use Dvlpm\Stream\Domain\User\Payload\CreateGuestPayload;
use Dvlpm\Stream\Domain\User\Repository\UserRepositoryInterface;
use Dvlpm\Stream\Infrastructure\Request\IdExtractor\IdExtractorInterface;
use Dvlpm\Stream\Infrastructure\Request\TokenExtractor\AccessTokenExtractorInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\Request;

final class GuestUserResolver implements SpecifiableUserResolverInterface
{
    private IdExtractorInterface $idExtractor;
    private UserRepositoryInterface $userRepository;
    private UserService $userService;
    private AccessTokenExtractorInterface $accessTokenExtractor;

    public function __construct(
        IdExtractorInterface $idExtractor,
        UserRepositoryInterface $userRepository,
        UserService $userService,
        AccessTokenExtractorInterface $accessTokenExtractor
    ) {
        $this->idExtractor = $idExtractor;
        $this->userRepository = $userRepository;
        $this->userService = $userService;
        $this->accessTokenExtractor = $accessTokenExtractor;
    }

    public function isSuitableFor(Request $request): bool
    {
        $id = $this->idExtractor->extract($request);
        $accessToken = $this->accessTokenExtractor->extract($request);

        if ($accessToken !== null) {
            return false;
        }

        if ($id !== null) {
            return $this->isSuitableForExistingUuid($id);
        }

        return true;
    }

    private function isSuitableForExistingUuid(UuidInterface $id): bool
    {
        $user = $this->userRepository->findOneById($id);

        if ($user === null) {
            return true;
        }

        return !$user->isRegistered();
    }

    public function resolve(Request $request): User
    {
        $id = $this->idExtractor->extract($request);

        if ($id === null) {
            return User::createGuest(new CreateGuestPayload(Uuid::uuid4()));
        }

        return $this->resolveForExistingId($id);
    }

    private function resolveForExistingId(UuidInterface $id): User
    {
        $user = $this->userRepository->findOneById($id);

        if ($user !== null) {
            return $user;
        }

        return $this->userService->createGuest(
            new CreateGuestPayload($id)
        );
    }
}
