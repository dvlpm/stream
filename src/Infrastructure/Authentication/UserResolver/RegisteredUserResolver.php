<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Authentication\UserResolver;

use Dvlpm\Stream\Application\Service\UserService;
use Dvlpm\Stream\Domain\User\Model\User;
use Dvlpm\Stream\Domain\User\Payload\RegisterPayload;
use Dvlpm\Stream\Domain\User\Payload\UpdateUserPayload;
use Dvlpm\Stream\Domain\User\Repository\UserRepositoryInterface;
use Dvlpm\Stream\Infrastructure\Authentication\Authenticator;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\UserInfo;
use Dvlpm\Stream\Infrastructure\Request\IdExtractor\IdExtractorInterface;
use Dvlpm\Stream\Infrastructure\Request\TokenExtractor\AccessTokenExtractorInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;

final class RegisteredUserResolver implements SpecifiableUserResolverInterface
{
    private AccessTokenExtractorInterface $accessTokenExtractor;
    private IdExtractorInterface $idExtractor;
    private UserRepositoryInterface $userRepository;
    private Authenticator $authenticator;
    private UserService $userService;

    public function __construct(
        AccessTokenExtractorInterface $accessTokenExtractor,
        IdExtractorInterface $idExtractor,
        UserRepositoryInterface $userRepository,
        Authenticator $authenticator,
        UserService $userService
    ) {
        $this->accessTokenExtractor = $accessTokenExtractor;
        $this->idExtractor = $idExtractor;
        $this->userRepository = $userRepository;
        $this->authenticator = $authenticator;
        $this->userService = $userService;
    }

    public function isSuitableFor(Request $request): bool
    {
        $accessToken = $this->accessTokenExtractor->extract($request);

        return $accessToken !== null;
    }

    public function resolve(Request $request): User
    {
        $accessToken = $this->accessTokenExtractor->extract($request);
        assert($accessToken !== null);

        $userInfo = $this->authenticator->authenticateByAccessToken($accessToken);
        if ($userInfo === null) {
            throw UserResolverException::invalidAccessToken();
        }

        $user = $this->userRepository->findOneByExternalId($userInfo->getExternalId());

        if ($user !== null && $user->isRegistered()) {
            return $this->handleRegisteredUserWithUserInfo($user, $userInfo);
        }

        $id = $this->idExtractor->extract($request);
        if ($id !== null) {
            $existingUser = $this->userRepository->findOneById($id);
            $id = $existingUser !== null ? Uuid::uuid4() : $id;
        }

        $registerPayload = new RegisterPayload(
            $userInfo->getExternalId(),
            $userInfo->getMeta(),
            $id
        );

        if ($user !== null) {
            $this->userService->registerGuestUser($user, $registerPayload);
        } else {
            $user = $this->userService->createRegisteredUser($registerPayload);
        }

        return $user;
    }

    private function handleRegisteredUserWithUserInfo(User $user, UserInfo $userInfo): User
    {
        if (!$user->hasExternalId($userInfo->getExternalId())) {
            throw UserResolverException::externalIdMismatch();
        }

        $this->userService->updateUser(
            $user,
            new UpdateUserPayload($userInfo->getMeta())
        );

        return $user;
    }
}
