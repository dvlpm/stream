<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Authentication\UserResolver;

use Symfony\Component\HttpFoundation\Request;

interface SpecifiableUserResolverInterface extends UserResolverInterface
{
    public function isSuitableFor(Request $request): bool;
}
