<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Authentication\UserResolver;

final class UserResolverException extends \Exception
{
    public static function unableToResolve(): self
    {
        return new static('Unable to resolve user');
    }

    public static function externalIdMismatch(): self
    {
        return new static('External id mismatch');
    }

    public static function invalidAccessToken(): self
    {
        return new static('Invalid access token');
    }
}
