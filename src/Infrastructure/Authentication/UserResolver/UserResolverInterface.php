<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Authentication\UserResolver;

use Dvlpm\Stream\Domain\User\Model\User;
use Symfony\Component\HttpFoundation\Request;

interface UserResolverInterface
{
    public function resolve(Request $request): User;
}
