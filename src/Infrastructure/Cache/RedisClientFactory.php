<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Cache;

use Predis\Client;

final class RedisClientFactory
{
    public static function create(string $dsn, string $sentinelName): Client
    {
        if (!empty($sentinelName)) {
            $parameters = [$dsn];
            $options = [
                'replication' => 'sentinel',
                'service' => $sentinelName,
            ];
        } else {
            $parameters = $dsn;
            $options = null;
        }

        return new Client($parameters, $options);
    }
}
