<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\CommandBus;

use Dvlpm\Stream\Application\CommandBus\CommandBusInterface;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\MessageBusInterface;

final class CommandBusAdapter implements CommandBusInterface
{
    private MessageBusInterface $bus;

    public function __construct(MessageBusInterface $bus)
    {
        $this->bus = $bus;
    }

    /**
     * @param object $command
     * @throws \Exception
     */
    public function handle(object $command): void
    {
        try {
            $this->bus->dispatch($command);
        } catch (HandlerFailedException $e) {
            $nestedExceptions = $e->getNestedExceptions();
            throw $nestedExceptions !== [] ? reset($nestedExceptions) : $e;
        }
    }
}
