<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\CommandBus\Middleware;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;

final class ActivateOnInnerCalls implements MiddlewareInterface
{
    private bool $mainCallRunning = false;
    private MiddlewareInterface $inner;

    public function __construct(MiddlewareInterface $inner)
    {
        $this->inner = $inner;
    }

    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        if ($this->mainCallRunning) {
            return $this->inner->handle($envelope, $stack);
        }

        $this->mainCallRunning = true;
        try {
            return $stack->next()->handle($envelope, $stack);
        } finally {
            $this->mainCallRunning = false;
        }
    }
}
