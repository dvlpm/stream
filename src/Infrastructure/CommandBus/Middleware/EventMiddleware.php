<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\CommandBus\Middleware;

use Dvlpm\Stream\Infrastructure\Event\EventDispatcherAdapter;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;

final class EventMiddleware implements MiddlewareInterface
{
    private EventDispatcherAdapter $eventDispatcher;

    public function __construct(EventDispatcherAdapter $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        $envelope = $stack->next()->handle($envelope, $stack);

        $this->eventDispatcher->flush();

        return $envelope;
    }
}
