<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\CommandBus\Middleware;

use Dvlpm\Stream\Infrastructure\CommandBus\Transport\Fallback\FallbackReceivedStamp;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;
use Symfony\Component\Messenger\Stamp\ReceivedStamp;

final class FallbackTransportProcessingMiddleware implements MiddlewareInterface
{
    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        $receivedFromFallbackTransport = $envelope->last(FallbackReceivedStamp::class) !== null;
        if ($receivedFromFallbackTransport && $envelope->last(ReceivedStamp::class) !== null) {
            $envelope = $envelope->withoutAll(ReceivedStamp::class);
        }

        return $stack->next()->handle($envelope, $stack);
    }
}
