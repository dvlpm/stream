<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\CommandBus\Middleware;

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;
use Throwable;

final class LoggingMiddleware implements MiddlewareInterface
{
    private LoggerInterface $logger;
    private array $exceptionsToLogLevels;

    public function __construct(LoggerInterface $logger, $exceptionsToLogLevels = [])
    {
        $this->logger = $logger;
        $this->exceptionsToLogLevels = $exceptionsToLogLevels;
    }

    /**
     * @param Envelope $envelope
     * @param StackInterface $stack
     * @return Envelope
     * @throws Throwable
     */
    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        try {
            $envelope = $stack->next()->handle($envelope, $stack);
        } catch (Throwable $e) {
            $this->logException($envelope, $e);
            throw $e;
        }

        return $envelope;
    }

    private function logException(Envelope $envelope, Throwable $exception): void
    {
        $handledException = $exception;
        if ($exception instanceof HandlerFailedException) {
            $handledException = current($exception->getNestedExceptions());
        }

        $handledExceptionClass = get_class($handledException);

        $context = [
            'class' => get_class($envelope->getMessage()),
            'error' => $handledException->getMessage(),
            'exception' => $handledException,
        ];

        $logLevel = $this->exceptionsToLogLevels[$handledExceptionClass] ?? LogLevel::ERROR;

        $this->logger->log($logLevel, 'Error thrown while handling message {class}. Error: {error}', $context);
    }
}
