<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\CommandBus\Middleware\StampingMiddleware;

use Symfony\Component\Messenger\Envelope;

final class AllProducers implements StampProducerInterface
{
    /**
     * @var StampProducerInterface[]
     */
    private array $producers;

    public function __construct(StampProducerInterface ...$producers)
    {
        $this->producers = $producers;
    }

    public function produceStamps(Envelope $envelope): array
    {
        $stamps = [];
        foreach ($this->producers as $producer) {
            $stamps[] = $producer->produceStamps($envelope);
        }

        return array_merge(...$stamps);
    }
}
