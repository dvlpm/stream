<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\CommandBus\Middleware\StampingMiddleware;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Stamp\DelayStamp;

final class CommandToDelayStamps implements StampProducerInterface
{
    private array $commandsToDelay;

    public function __construct(array $commandsToDelay)
    {
        $this->commandsToDelay = $commandsToDelay;
    }

    public function produceStamps(Envelope $envelope): array
    {
        $command = $envelope->getMessage();

        foreach ($this->commandsToDelay as $commandClass => $delayMs) {
            if ($command instanceof $commandClass) {
                return [new DelayStamp($delayMs)];
            }
        }

        return [];
    }
}
