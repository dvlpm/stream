<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\CommandBus\Middleware\StampingMiddleware;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Stamp\StampInterface;

final class ConstantStamps implements StampProducerInterface
{
    /**
     * @var StampInterface[]
     */
    private array $stamps;

    public function __construct(StampInterface ...$stamps)
    {
        $this->stamps = $stamps;
    }

    public function produceStamps(Envelope $envelope): array
    {
        return $this->stamps;
    }
}
