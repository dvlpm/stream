<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\CommandBus\Transport\Audit;

use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Stamp\DelayStamp;
use Symfony\Component\Messenger\Stamp\RedeliveryStamp;
use Symfony\Component\Messenger\Transport\TransportInterface;

final class AuditTransport implements TransportInterface
{
    private Connection $connection;

    public function __construct(Connection $connection, LoggerInterface $logger = null)
    {
        $this->connection = $connection;
    }

    public function get(): iterable
    {
        foreach ($this->connection->getOriginal()->get() as $envelope) {
            // Always ack messages immediately
            $this->connection->getOriginal()->ack($envelope);
        }

        return [];
    }

    public function ack(Envelope $envelope): void
    {
        // do nothing
    }

    public function reject(Envelope $envelope): void
    {
        // do nothing
    }

    public function send(Envelope $envelope): Envelope
    {
        if ($envelope->last(RedeliveryStamp::class) !== null) {
            // If message is redelivered, then it should be published to audit queue
            return $envelope;
        }

        $originalEnvelope = $envelope;

        // Remove original DelayStamp
        $envelope = $envelope->withoutAll(DelayStamp::class);

        if ($this->connection->getStorePeriodMs() > 0) {
            // Add delay stamp indicating how long message must be stored in the queue
            $envelope = $envelope->with(new DelayStamp($this->connection->getStorePeriodMs()));
        }

        $this->connection->getOriginal()->send($envelope);

        return $originalEnvelope;
    }
}
