<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\CommandBus\Transport\Audit;

use Psr\Container\ContainerInterface;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use Symfony\Component\Messenger\Transport\TransportFactoryInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;

final class AuditTransportFactory implements TransportFactoryInterface
{
    private ContainerInterface $transports;

    public function __construct(ContainerInterface $transports)
    {
        $this->transports = $transports;
    }

    public function createTransport(string $dsn, array $options, SerializerInterface $serializer): TransportInterface
    {
        $connection = Connection::fromDsn($dsn, $options, $this->transports);

        return new AuditTransport($connection);
    }

    public function supports(string $dsn, array $options): bool
    {
        return strpos($dsn, 'audit://') === 0;
    }
}
