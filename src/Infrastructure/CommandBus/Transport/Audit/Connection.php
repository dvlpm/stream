<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\CommandBus\Transport\Audit;

use Psr\Container\ContainerInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;

final class Connection
{
    private string $originalName;
    private ?TransportInterface $original = null;
    private int $storePeriodMs;
    private ContainerInterface $transports;

    public function __construct(string $originalName, int $storeIntervalMs, ContainerInterface $transports)
    {
        $this->originalName = $originalName;
        $this->storePeriodMs = $storeIntervalMs;
        $this->transports = $transports;
    }

    public static function fromDsn(string $dsn, array $options, ContainerInterface $transports): self
    {
        $parsedUrl = parse_url($dsn);
        if ($parsedUrl === false) {
            $parsedUrl = [];
        }

        parse_str($parsedUrl['query'] ?? '', $parsedQuery);

        $connectionOptions = array_merge($parsedQuery, $options);

        $originalName = $connectionOptions['original'] ?? $parsedUrl['host'] ?? null;

        if ($originalName === null) {
            throw new \InvalidArgumentException('Missing parameter "original" for Audit DSN');
        }

        $storePeriodMs = $connectionOptions['period'] ?? 0;

        return new static((string) $originalName, (int) $storePeriodMs, $transports);
    }

    public function getOriginal(): TransportInterface
    {
        if ($this->original === null) {
            $this->original = $this->getTransport($this->originalName);
        }

        return $this->original;
    }

    public function getOriginalName(): string
    {
        return $this->originalName;
    }

    public function getStorePeriodMs(): int
    {
        return $this->storePeriodMs;
    }

    private function getTransport(string $name): TransportInterface
    {
        $namesToTry = [$name, 'messenger.transport.' . $name];

        foreach ($namesToTry as $testedName) {
            if (!$this->transports->has($testedName)) {
                continue;
            }

            $transport = $this->transports->get($testedName);
            assert($transport instanceof TransportInterface);

            return $transport;
        }

        throw new \InvalidArgumentException(sprintf(
            'Specified transport "%s" for Audit transport does not exist',
            $name
        ));
    }
}
