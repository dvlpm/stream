<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\CommandBus\Transport\DoctrinePostgres;

use Doctrine\DBAL\Connection as DBALConnection;
use Doctrine\DBAL\Driver\ResultStatement;
use Doctrine\DBAL\Exception\TableNotFoundException;
use Doctrine\DBAL\Schema\Synchronizer\SchemaSynchronizer;
use Doctrine\DBAL\Types\Type;

/**
 * This connection is different from the original in a way it publishes messages.
 * It uses Postgres INSERT INTO ... RETURNING to get id.
 */
final class Connection extends \Symfony\Component\Messenger\Transport\Doctrine\Connection
{
    /** @var DBALConnection */
    protected $driverConnection;

    public function __construct(
        array $configuration,
        DBALConnection $driverConnection,
        SchemaSynchronizer $schemaSynchronizer = null
    ) {
        parent::__construct($configuration, $driverConnection, $schemaSynchronizer);
        $this->driverConnection = $driverConnection;
    }

    public function send(string $body, array $headers, int $delay = 0): string
    {
        $now = new \DateTime();
        $availableAt = (clone $now)->modify(sprintf('+%d seconds', $delay / 1000));

        $sql = /* @lang PostgreSQL */
            <<<SQL
INSERT INTO %s (body, headers, queue_name, created_at, available_at)
VALUES (?, ?, ?, ?, ?)
RETURNING id;
SQL;

        $result = $this->executeQuery(sprintf($sql, $this->getConfiguration()['table_name']), [
            $body,
            json_encode($headers),
            $this->getConfiguration()['queue_name'],
            $now,
            $availableAt,
        ], [
            null,
            null,
            null,
            Type::DATETIME,
            Type::DATETIME,
        ]);

        return (string) $result->fetchColumn();
    }

    private function executeQuery(string $sql, array $parameters = [], array $types = []): ResultStatement
    {
        try {
            $stmt = $this->driverConnection->executeQuery($sql, $parameters, $types);
        } catch (TableNotFoundException $e) {
            if ($this->driverConnection->isTransactionActive()) {
                throw $e;
            }

            // create table
            if ($this->getConfiguration()['auto_setup']) {
                $this->setup();
            }
            $stmt = $this->driverConnection->executeQuery($sql, $parameters, $types);
        }

        return $stmt;
    }
}
