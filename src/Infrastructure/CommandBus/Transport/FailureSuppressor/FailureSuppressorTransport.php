<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\CommandBus\Transport\FailureSuppressor;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Transport\TransportInterface;

final class FailureSuppressorTransport implements TransportInterface
{
    private Connection $connection;
    private LoggerInterface $logger;

    public function __construct(Connection $connection, LoggerInterface $logger = null)
    {
        $this->connection = $connection;
        $this->logger = $logger ?: new NullLogger();
    }

    /**
     * {@inheritdoc}
     */
    public function get(): iterable
    {
        try {
            return $this->connection->getOriginal()->get();
        } catch (\Throwable $exception) {
            $this->logger->error('Transport {transport} failed to get messages', [
                'transport' => $this->connection->getOriginalName(),
                'error' => $exception->getMessage(),
                'exception' => $exception,
            ]);
        }

        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function ack(Envelope $envelope): void
    {
        try {
            $this->connection->getOriginal()->ack($envelope);
        } catch (\Throwable $exception) {
            $this->logger->error('Transport {transport} failed to ack message {class}', [
                'class' => get_class($envelope->getMessage()),
                'transport' => $this->connection->getOriginalName(),
                'error' => $exception->getMessage(),
                'exception' => $exception,
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function reject(Envelope $envelope): void
    {
        try {
            $this->connection->getOriginal()->reject($envelope);
        } catch (\Throwable $exception) {
            $this->logger->error('Transport {transport} failed to reject message {class}', [
                'class' => get_class($envelope->getMessage()),
                'transport' => $this->connection->getOriginalName(),
                'error' => $exception->getMessage(),
                'exception' => $exception,
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function send(Envelope $envelope): Envelope
    {
        $message = $envelope->getMessage();
        $context = [
            'message' => $message,
            'class' => \get_class($message),
        ];

        try {
            return $this->connection->getOriginal()->send($envelope);
        } catch (\Throwable $exception) {
            $this->logger->error(
                'Transport "{transport} failed to send message {class}',
                $context + [
                    'transport' => $this->connection->getOriginalName(),
                    'error' => $exception->getMessage(),
                    'exception' => $exception,
                ]);
        }

        return $envelope;
    }
}
