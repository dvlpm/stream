<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\CommandBus\Transport\Fallback;

use Psr\Container\ContainerInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;

final class Connection
{
    private string $originalName;
    private ?TransportInterface $original = null;
    private string $fallbackName;
    private ?TransportInterface $fallback = null;
    private ContainerInterface $transports;

    public function __construct(string $originalName, string $fallbackName, ContainerInterface $transports)
    {
        $this->originalName = $originalName;
        $this->fallbackName = $fallbackName;
        $this->transports = $transports;
    }

    public static function fromDsn(string $dsn, array $options, ContainerInterface $transports): self
    {
        $parsedUrl = parse_url($dsn);
        if ($parsedUrl === false) {
            $parsedUrl = [];
        }

        parse_str($parsedUrl['query'] ?? '', $parsedQuery);

        $connectionOptions = array_merge($parsedQuery, $options);

        $original = $connectionOptions['original'] ?? $parsedUrl['host'] ?? null;

        if ($original === null) {
            throw new \InvalidArgumentException('Missing parameter "original" for FallbackTransport DSN');
        }

        $fallback = $connectionOptions['fallback'] ?? null;
        if ($fallback === null) {
            throw new \InvalidArgumentException('Missing parameter "fallback" for FallbackTransport DSN');
        }

        return new static((string) $original, (string) $fallback, $transports);
    }

    public function getOriginal(): TransportInterface
    {
        if ($this->original === null) {
            $this->original = $this->getTransport($this->originalName);
        }

        return $this->original;
    }

    public function getFallback(): TransportInterface
    {
        if ($this->fallback === null) {
            $this->fallback = $this->getTransport($this->fallbackName);
        }

        return $this->fallback;
    }

    public function getOriginalName(): string
    {
        return $this->originalName;
    }

    public function getFallbackName(): string
    {
        return $this->fallbackName;
    }

    private function getTransport(string $name): TransportInterface
    {
        $namesToTry = [$name, 'messenger.transport.' . $name];

        foreach ($namesToTry as $testedName) {
            if (!$this->transports->has($testedName)) {
                continue;
            }

            $transport = $this->transports->get($testedName);
            assert($transport instanceof TransportInterface);

            return $transport;
        }

        throw new \InvalidArgumentException(sprintf(
            'Specified transport "%s" for FallbackTransport does not exists',
            $name
        ));
    }
}
