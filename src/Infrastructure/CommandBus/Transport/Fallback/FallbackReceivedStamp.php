<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\CommandBus\Transport\Fallback;

use Symfony\Component\Messenger\Stamp\StampInterface;

final class FallbackReceivedStamp implements StampInterface
{
}
