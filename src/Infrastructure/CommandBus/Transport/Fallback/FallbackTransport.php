<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\CommandBus\Transport\Fallback;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Transport\TransportInterface;

final class FallbackTransport implements TransportInterface
{
    private Connection $connection;
    private LoggerInterface $logger;

    public function __construct(Connection $connection, LoggerInterface $logger = null)
    {
        $this->connection = $connection;
        $this->logger = $logger ?? new NullLogger();
    }

    public function get(): iterable
    {
        foreach ($this->connection->getFallback()->get() as $envelope) {
            yield $this->createEnvelope($envelope);
        }
    }

    public function ack(Envelope $envelope): void
    {
        $this->connection->getFallback()->ack($envelope);
    }

    public function reject(Envelope $envelope): void
    {
        $this->connection->getFallback()->reject($envelope);
    }

    public function send(Envelope $envelope): Envelope
    {
        try {
            return $this->connection->getOriginal()->send($envelope);
        } catch (\Throwable $e) {
            $this->logger->notice('Original transport "{original}" failed with exception. Sending envelope to fallback "{fallback}"', [
                'original' => $this->connection->getOriginalName(),
                'fallback' => $this->connection->getFallbackName(),
                'exception' => $e,
            ]);

            return $this->connection->getFallback()->send($envelope);
        }
    }

    private function createEnvelope(Envelope $envelope): Envelope
    {
        return $envelope->with(new FallbackReceivedStamp());
    }
}
