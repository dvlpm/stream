<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\CommandBus\Transport\OneOf;

use Psr\Container\ContainerInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\InvalidArgumentException;
use Symfony\Component\Messenger\Transport\TransportInterface;

final class Connection
{
    private SpecificationAndTransportCollection $specAndTransports;
    private ?string $defaultTransportName;
    private ContainerInterface $transports;

    public function __construct(SpecificationAndTransportCollection $specAndTransports, ?string $defaultTransportName, ContainerInterface $transports)
    {
        $this->specAndTransports = $specAndTransports;
        $this->defaultTransportName = $defaultTransportName;
        $this->transports = $transports;
    }

    public static function fromDsn(
        string $dsn,
        array $options,
        ContainerInterface $transports,
        ContainerInterface $specAndTransportsCollections
    ): self {
        $parsedUrl = parse_url($dsn);
        if ($parsedUrl === false) {
            $parsedUrl = [];
        }

        parse_str($parsedUrl['query'] ?? '', $parsedQuery);

        $connectionOptions = array_merge($parsedQuery, $options);

        $transportCollectionName = $connectionOptions['transports'] ?? $parsedUrl['host'] ?? null;
        if ($transportCollectionName === null) {
            throw new \InvalidArgumentException(sprintf('Missing parameter "transports" for OneOf DSN'));
        }
        $defaultTransportName = $connectionOptions['default'] ?? null;

        $specAndTransports = self::getSpecAndTransportsCollection(
            (string) $transportCollectionName,
            $specAndTransportsCollections
        );

        return new self($specAndTransports, $defaultTransportName, $transports);
    }

    private static function getSpecAndTransportsCollection(
        string $name,
        ContainerInterface $container
    ): SpecificationAndTransportCollection {
        $namesToTry = ['one_of_transport.collection.' . $name, $name];

        foreach ($namesToTry as $nameToTry) {
            if (!$container->has($nameToTry)) {
                continue;
            }

            return $container->get($nameToTry);
        }

        throw new InvalidArgumentException(sprintf(
            'Specified transports collection "%s" for OneOfTransport does not exists',
            $name
        ));
    }

    /**
     * @return TransportInterface[]
     */
    public function getTransportsFor(Envelope $envelope): iterable
    {
        $noSuitableTransports = true;

        foreach ($this->specAndTransports->elements() as $specAndTransport) {
            if ($specAndTransport->getSpecification()->isSuitableFor($envelope)) {
                $noSuitableTransports = false;
                yield $this->getTransportByName($specAndTransport->getTransportName());
            }
        }

        if ($noSuitableTransports && $this->defaultTransportName !== null) {
            yield $this->getTransportByName($this->defaultTransportName);
        }
    }

    private function getTransportByName(string $name): TransportInterface
    {
        $namesToTry = ['messenger.transport.' . $name, $name];

        foreach ($namesToTry as $nameToTry) {
            if (!$this->transports->has($nameToTry)) {
                continue;
            }

            return $this->transports->get($nameToTry);
        }

        throw new InvalidArgumentException(sprintf(
            'Specified transport "%s" for OneOfTransport does not exists',
            $name
        ));
    }
}
