<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\CommandBus\Transport\OneOf;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\InvalidArgumentException;
use Symfony\Component\Messenger\Transport\TransportInterface;

final class OneOfTransport implements TransportInterface
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function get(): iterable
    {
        throw new InvalidArgumentException('You cannot receive messages from the OneOfTransport.');
    }

    public function ack(Envelope $envelope): void
    {
        throw new InvalidArgumentException('You cannot ack messages from the OneOfTransport.');
    }

    public function reject(Envelope $envelope): void
    {
        throw new InvalidArgumentException('You cannot reject messages from the OneOfTransport.');
    }

    public function send(Envelope $envelope): Envelope
    {
        foreach ($this->connection->getTransportsFor($envelope) as $transport) {
            $transport->send($envelope);
        }

        return $envelope;
    }
}
