<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\CommandBus\Transport\OneOf;

use Psr\Container\ContainerInterface;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use Symfony\Component\Messenger\Transport\TransportFactoryInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;

final class OneOfTransportFactory implements TransportFactoryInterface
{
    private ContainerInterface $transports;
    private ContainerInterface $specAndTransportsCollections;

    public function __construct(ContainerInterface $transports, ContainerInterface $specAndTransportsCollections)
    {
        $this->transports = $transports;
        $this->specAndTransportsCollections = $specAndTransportsCollections;
    }

    public function createTransport(string $dsn, array $options, SerializerInterface $serializer): TransportInterface
    {
        $connection = Connection::fromDsn($dsn, $options, $this->transports, $this->specAndTransportsCollections);

        return new OneOfTransport($connection);
    }

    public function supports(string $dsn, array $options): bool
    {
        return strpos($dsn, 'one-of://') === 0;
    }
}
