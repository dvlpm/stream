<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\CommandBus\Transport\OneOf\Specification;

use Symfony\Component\Messenger\Envelope;

final class MatchSpecification implements SpecificationInterface
{
    public function isSuitableFor(Envelope $candidate): bool
    {
        return true;
    }
}
