<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\CommandBus\Transport\OneOf;

use Dvlpm\Stream\Infrastructure\CommandBus\Transport\OneOf\Specification\SpecificationInterface;

final class SpecificationAndTransport
{
    private SpecificationInterface $specification;
    private string $transportName;

    public function __construct(SpecificationInterface $specification, string $transportName)
    {
        $this->specification = $specification;
        $this->transportName = $transportName;
    }

    public function getSpecification(): SpecificationInterface
    {
        return $this->specification;
    }

    public function getTransportName(): string
    {
        return $this->transportName;
    }
}
