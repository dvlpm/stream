<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\CommandBus\Transport\OneOf;

final class SpecificationAndTransportCollection
{
    /** @var SpecificationAndTransport[] */
    private array $collection;

    public function __construct(SpecificationAndTransport ...$collection)
    {
        $this->collection = $collection;
    }

    /**
     * @return SpecificationAndTransport[]
     */
    public function elements(): iterable
    {
        return $this->collection;
    }
}
