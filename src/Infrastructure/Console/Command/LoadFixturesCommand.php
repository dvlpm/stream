<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Console\Command;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class LoadFixturesCommand extends Command
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct('fixtures:load');
        $this->entityManager = $entityManager;
    }

    protected function configure(): void
    {
        $this->addArgument('fixture', InputArgument::REQUIRED, 'Fixture class');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('<info>Start...</info>');

        $purger = new ORMPurger();
        $purger->setPurgeMode(ORMPurger::PURGE_MODE_DELETE);
        $executor = new ORMExecutor(
            $this->entityManager,
            $purger
        );

        $fixture = $input->getArgument('fixture');

        $loader = new Loader();
        $loader->addFixture(new $fixture());
        $executor->execute($loader->getFixtures());

        $output->writeln('<bg=green;fg=white>Fixtures loaded</>');

        return 0;
    }
}
