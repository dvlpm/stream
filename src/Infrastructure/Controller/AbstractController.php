<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Controller;

use Dvlpm\Stream\Application\Message\MessageBag;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;

abstract class AbstractController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    protected function dump(object $dump): JsonResponse
    {
        return $this->json($dump, JsonResponse::HTTP_OK, [], [
            AbstractNormalizer::GROUPS => get_class($dump),
            AbstractObjectNormalizer::SKIP_NULL_VALUES => true,
        ]);
    }

    protected function messageBag(MessageBag $messageBag): JsonResponse
    {
        return $this->json($messageBag, JsonResponse::HTTP_OK);
    }
}
