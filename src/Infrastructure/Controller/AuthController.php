<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Controller;

use Auth0\SDK\Auth0;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * TODO https://jira.dvl.pm/browse/USER-8
 */
final class AuthController extends AbstractController
{
    /**
     * @Route("/auth/login")
     * @param Auth0 $auth0
     */
    public function login(Auth0 $auth0): void
    {
        $auth0->login();
    }

    /**
     * @Route("/auth/info")
     * @param Auth0 $auth0
     * @return JsonResponse
     * @throws \Auth0\SDK\Exception\ApiException
     * @throws \Auth0\SDK\Exception\CoreException
     */
    public function info(Auth0 $auth0): JsonResponse
    {
        return $this->json([
            'user' => $auth0->getUser(),
            'access_token' =>  $auth0->getAccessToken()
        ]);
    }
}
