<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Controller;

use Dvlpm\Stream\Application\Command\CreateContentCommand;
use Dvlpm\Stream\Application\Command\RemoveContentCommand;
use Dvlpm\Stream\Application\Command\UpdateContentCommand;
use Dvlpm\Stream\Application\CommandBus\CommandBusInterface;
use Dvlpm\Stream\Application\Dto\AbstractCreateContentDto;
use Dvlpm\Stream\Application\Dto\AbstractUpdateContentDto;
use Dvlpm\Stream\Application\Message\Message;
use Dvlpm\Stream\Application\Message\MessageBag;
use Dvlpm\Stream\Application\PayloadFactory\CreateContentPayloadFactory;
use Dvlpm\Stream\Application\PayloadFactory\Exception\ContentPayloadFactoryException;
use Dvlpm\Stream\Application\PayloadFactory\UpdateContentPayloadFactory;
use Dvlpm\Stream\Domain\Content\Exception\UndefinedContentException;
use Dvlpm\Stream\Domain\Content\Repository\ContentRepositoryInterface;
use Dvlpm\Stream\Domain\User\Model\User;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final class ContentController extends AbstractController
{
    /**
     * @Route("/content", methods={"POST"})
     * @param AbstractCreateContentDto $dto
     * @param CreateContentPayloadFactory $payloadFactory
     * @param CommandBusInterface $commandBus
     * @param ContentRepositoryInterface $contentRepository
     * @param User $user
     * @return JsonResponse
     * @throws ContentPayloadFactoryException|UndefinedContentException
     */
    public function create(
        CreateContentPayloadFactory $payloadFactory,
        CommandBusInterface $commandBus,
        ContentRepositoryInterface $contentRepository,
        User $user,
        AbstractCreateContentDto $dto
    ): JsonResponse {
        $dto->id = $dto->id ?? Uuid::uuid4();

        $command = new CreateContentCommand(
            $user->getId(),
            $payloadFactory->createFromDto($dto)
        );

        $commandBus->handle($command);

        return $this->dump($contentRepository->findOneByIdOrFail($dto->id)->dumpBy($user));
    }

    /**
     * @Route("/content/{contentId}", methods={"PUT"})
     * @param UpdateContentPayloadFactory $payloadFactory
     * @param CommandBusInterface $commandBus
     * @param ContentRepositoryInterface $contentRepository
     * @param User $user
     * @param UuidInterface $contentId
     * @param AbstractUpdateContentDto $dto
     * @return JsonResponse
     * @throws ContentPayloadFactoryException
     * @throws UndefinedContentException
     */
    public function update(
        UpdateContentPayloadFactory $payloadFactory,
        CommandBusInterface $commandBus,
        ContentRepositoryInterface $contentRepository,
        User $user,
        UuidInterface $contentId,
        AbstractUpdateContentDto $dto
    ): JsonResponse {
        $command = new UpdateContentCommand(
            $user->getId(),
            $contentId,
            $payloadFactory->createFromDto($dto)
        );

        $commandBus->handle($command);

        return $this->dump($contentRepository->findOneByIdOrFail($contentId)->dumpBy($user));
    }

    /**
     * @Route("/content/{contentId}", methods={"GET"})
     * @param ContentRepositoryInterface $contentRepository
     * @param UuidInterface $contentId
     * @param User $user
     * @return JsonResponse
     * @throws UndefinedContentException
     */
    public function show(
        ContentRepositoryInterface $contentRepository,
        UuidInterface $contentId,
        User $user
    ): JsonResponse {
        return $this->dump($contentRepository->findOneByIdOrFail($contentId)->dumpBy($user));
    }

    /**
     * @Route("/content/{contentId}", methods={"DELETE"})
     * @param CommandBusInterface $commandBus
     * @param User $user
     * @param UuidInterface $contentId
     * @return JsonResponse
     */
    public function remove(
        CommandBusInterface $commandBus,
        User $user,
        UuidInterface $contentId
    ): JsonResponse {
        $command = new RemoveContentCommand(
            $user->getId(),
            $contentId
        );

        $commandBus->handle($command);

        return $this->messageBag(MessageBag::withMessages(
            Message::notification(sprintf('Content %s removed.', $contentId))
        ));
    }
}
