<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Controller;

use Dvlpm\Stream\Application\Command\AddRoomToHallCommand;
use Dvlpm\Stream\Application\Command\CreateHallCommand;
use Dvlpm\Stream\Application\Command\RemoveHallCommand;
use Dvlpm\Stream\Application\Command\UpdateHallCommand;
use Dvlpm\Stream\Application\CommandBus\CommandBusInterface;
use Dvlpm\Stream\Application\Dto\CreateHallDto;
use Dvlpm\Stream\Application\Dto\CreateRoomDto;
use Dvlpm\Stream\Application\Dto\UpdateHallDto;
use Dvlpm\Stream\Application\Message\Message;
use Dvlpm\Stream\Application\Message\MessageBag;
use Dvlpm\Stream\Application\PayloadFactory\AddRoomToHallPayloadFactory;
use Dvlpm\Stream\Application\PayloadFactory\CreateHallPayloadFactory;
use Dvlpm\Stream\Application\PayloadFactory\UpdateHallPayloadFactory;
use Dvlpm\Stream\Domain\Common\Factory\UuidFactory;
use Dvlpm\Stream\Domain\Space\Dump\HallDumpOptions;
use Dvlpm\Stream\Domain\Space\Exception\UndefinedHallException;
use Dvlpm\Stream\Domain\Space\Model\HostRole;
use Dvlpm\Stream\Domain\Space\Model\Identifier;
use Dvlpm\Stream\Domain\Space\Repository\HallCriteria;
use Dvlpm\Stream\Domain\Space\Repository\HallRepositoryInterface;
use Dvlpm\Stream\Domain\Space\Repository\HostCriteria;
use Dvlpm\Stream\Domain\User\Model\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

final class HallController extends AbstractController
{
    /**
     * @Route("/hall", methods={"POST"})
     * @param CreateHallPayloadFactory $payloadFactory
     * @param CommandBusInterface $commandBus
     * @param HallRepositoryInterface $hallRepository
     * @param User $user
     * @param CreateHallDto $dto
     * @return JsonResponse
     * @throws UndefinedHallException
     */
    public function create(
        CreateHallPayloadFactory $payloadFactory,
        CommandBusInterface $commandBus,
        HallRepositoryInterface $hallRepository,
        User $user,
        CreateHallDto $dto
    ): JsonResponse {
        $command = new CreateHallCommand(
            $user->getId(),
            $payloadFactory->createFromDto($dto)
        );

        $commandBus->handle($command);

        return $this->dump($hallRepository->findOneByIdentifierOrFail($dto->identifier)->dumpBy($user));
    }

    /**
     * @Route("/hall/{hallIdentifier}", methods={"PUT"})
     * @param UpdateHallPayloadFactory $payloadFactory
     * @param CommandBusInterface $commandBus
     * @param HallRepositoryInterface $hallRepository
     * @param User $user
     * @param UpdateHallDto $dto
     * @param Identifier $hallIdentifier
     * @return JsonResponse
     * @throws UndefinedHallException
     */
    public function update(
        UpdateHallPayloadFactory $payloadFactory,
        CommandBusInterface $commandBus,
        HallRepositoryInterface $hallRepository,
        User $user,
        UpdateHallDto $dto,
        Identifier $hallIdentifier
    ): JsonResponse {
        $command = new UpdateHallCommand(
            $user->getId(),
            $hallIdentifier,
            $payloadFactory->createFromDto($dto)
        );

        $commandBus->handle($command);

        return $this->dump($hallRepository->findOneByIdentifierOrFail($hallIdentifier)->dumpBy($user));
    }

    /**
     * @Route("/hall/{hallIdentifier}", methods={"GET"})
     * @param HallRepositoryInterface $hallRepository
     * @param User $user
     * @param Identifier $hallIdentifier
     * @return JsonResponse
     * @throws UndefinedHallException
     */
    public function show(
        HallRepositoryInterface $hallRepository,
        User $user,
        Identifier $hallIdentifier
    ): JsonResponse {
        return $this->dump($hallRepository->findOneByIdentifierOrFail($hallIdentifier)->dumpBy($user));
    }

    /**
     * @Route("/halls", methods={"GET"})
     * @param HallRepositoryInterface $hallRepository
     * @param User $user
     * @param Request $request
     * @return JsonResponse
     */
    public function list(
        HallRepositoryInterface $hallRepository,
        User $user,
        Request $request
    ): JsonResponse {
        return $this->dump($hallRepository->findAllByCriteria(new HallCriteria(
            new HostCriteria(
                UuidFactory::createFromStringOrNull($request->get('host_user_id')),
                array_filter(
                    array_map(
                        fn(string $role) => HostRole::createByNameOrNull($role),
                        $request->get('host_roles', [])
                    ),
                    fn(?HostRole $role) => $role !== null
                ),
            )
        ))->dumpBy($user, HallDumpOptions::default()->showRooms(false)));
    }

    /**
     * @Route("/hall/{hallIdentifier}/room", methods={"POST"})
     * @param AddRoomToHallPayloadFactory $payloadFactory
     * @param CommandBusInterface $commandBus
     * @param HallRepositoryInterface $hallRepository
     * @param User $user
     * @param Identifier $hallIdentifier
     * @param CreateRoomDto $dto
     * @return JsonResponse
     * @throws UndefinedHallException
     */
    public function addRoom(
        AddRoomToHallPayloadFactory $payloadFactory,
        CommandBusInterface $commandBus,
        HallRepositoryInterface $hallRepository,
        User $user,
        Identifier $hallIdentifier,
        CreateRoomDto $dto
    ): JsonResponse {
        $command = new AddRoomToHallCommand(
            $user->getId(),
            $hallIdentifier,
            $payloadFactory->createFromDto($dto)
        );

        $commandBus->handle($command);

        return $this->dump($hallRepository->findOneByIdentifierOrFail($hallIdentifier)->dumpBy($user));
    }

    /**
     * @Route("/hall/{hallIdentifier}", methods={"DELETE"})
     * @param CommandBusInterface $commandBus
     * @param User $user
     * @param Identifier $hallIdentifier
     * @return JsonResponse
     */
    public function remove(
        CommandBusInterface $commandBus,
        User $user,
        Identifier $hallIdentifier
    ): JsonResponse {
        $command = new RemoveHallCommand(
            $user->getId(),
            $hallIdentifier
        );

        $commandBus->handle($command);

        return $this->messageBag(MessageBag::withMessages(Message::notification(
            sprintf('Hall %s removed', $hallIdentifier)
        )));
    }
}
