<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Controller;

use Dvlpm\Stream\Application\Command\RemoveParticipantCommand;
use Dvlpm\Stream\Application\CommandBus\CommandBusInterface;
use Dvlpm\Stream\Application\Message\Message;
use Dvlpm\Stream\Application\Message\MessageBag;
use Dvlpm\Stream\Domain\User\Model\User;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final class ParticipantController extends AbstractController
{
    /**
     * @Route("/participant/{participantId}", methods={"DELETE"})
     * @param CommandBusInterface $commandBus
     * @param User $user
     * @param UuidInterface $participantId
     * @return JsonResponse
     */
    public function remove(
        CommandBusInterface $commandBus,
        User $user,
        UuidInterface $participantId
    ): JsonResponse {
        $command = new RemoveParticipantCommand(
            $user->getId(),
            $participantId
        );

        $commandBus->handle($command);

        return $this->messageBag(MessageBag::withMessages(
            Message::notification(sprintf('Participant %s removed.', $participantId))
        ));
    }
}
