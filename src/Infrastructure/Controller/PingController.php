<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final class PingController extends AbstractController
{
    /**
     * @Route("/ping", methods={"GET"})
     */
    public function update(): JsonResponse {
        return $this->json('Hello world');
    }
}
