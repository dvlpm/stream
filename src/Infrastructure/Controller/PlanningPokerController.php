<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Controller;

use Dvlpm\Stream\Application\Command\AddPlanningPokerPlayerToPlanningPokerCommand;
use Dvlpm\Stream\Application\Command\AddPlanningPokerStoryToPlanningPokerCommand;
use Dvlpm\Stream\Application\CommandBus\CommandBusInterface;
use Dvlpm\Stream\Application\Dto\CreatePlanningPokerPlayerDto;
use Dvlpm\Stream\Application\Dto\CreatePlanningPokerStoryDto;
use Dvlpm\Stream\Application\PayloadFactory\AddPlanningPokerStoryToPlanningPokerPayloadFactory;
use Dvlpm\Stream\Domain\Content\Exception\UndefinedContentException;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerStoryStatus;
use Dvlpm\Stream\Domain\Content\Repository\ContentRepositoryInterface;
use Dvlpm\Stream\Domain\Content\Repository\PlanningPokerStoryCriteria;
use Dvlpm\Stream\Domain\Content\Repository\PlanningPokerStoryRepositoryInterface;
use Dvlpm\Stream\Domain\User\Model\User;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final class PlanningPokerController extends AbstractController
{
    /**
     * @Route("/planning-poker/{planningPokerId}/player", methods={"POST"})
     * @param CommandBusInterface $commandBus
     * @param ContentRepositoryInterface $contentRepository
     * @param User $user
     * @param UuidInterface $planningPokerId
     * @param CreatePlanningPokerPlayerDto $dto
     * @return JsonResponse
     * @throws UndefinedContentException
     */
    public function addPlayer(
        CommandBusInterface $commandBus,
        ContentRepositoryInterface $contentRepository,
        User $user,
        UuidInterface $planningPokerId,
        CreatePlanningPokerPlayerDto $dto
    ): JsonResponse {
        $command = new AddPlanningPokerPlayerToPlanningPokerCommand(
            $user->getId(),
            $dto->id ?? Uuid::uuid4(),
            $dto->user->id,
            $planningPokerId,
        );

        $commandBus->handle($command);

        return $this->dump($contentRepository->findOneByIdOrFail($planningPokerId)->dumpBy($user));
    }

    /**
     * @Route("/planning-poker/{planningPokerId}/story", methods={"POST"})
     * @param AddPlanningPokerStoryToPlanningPokerPayloadFactory $payloadFactory
     * @param CommandBusInterface $commandBus
     * @param ContentRepositoryInterface $contentRepository
     * @param User $user
     * @param UuidInterface $planningPokerId
     * @param CreatePlanningPokerStoryDto $dto
     * @return JsonResponse
     * @throws UndefinedContentException
     */
    public function addStory(
        AddPlanningPokerStoryToPlanningPokerPayloadFactory $payloadFactory,
        CommandBusInterface $commandBus,
        ContentRepositoryInterface $contentRepository,
        User $user,
        UuidInterface $planningPokerId,
        CreatePlanningPokerStoryDto $dto
    ): JsonResponse {
        $command = new AddPlanningPokerStoryToPlanningPokerCommand(
            $user->getId(),
            $planningPokerId,
            $payloadFactory->createFromDto($dto)
        );

        $commandBus->handle($command);

        return $this->dump($contentRepository->findOneByIdOrFail($planningPokerId)->dumpBy($user));
    }

    /**
     * @Route("/planning-poker/{planningPokerId}/stories", methods={"GET"})
     * @param PlanningPokerStoryRepositoryInterface $storyRepository
     * @param User $user
     * @param UuidInterface $planningPokerId
     * @param string|null $status
     * @param int|null $limit
     * @param int|null $offset
     * @return JsonResponse
     */
    public function listStories(
        PlanningPokerStoryRepositoryInterface $storyRepository,
        User $user,
        UuidInterface $planningPokerId,
        ?string $status,
        ?int $limit,
        ?int $offset
    ): JsonResponse {
        return $this->dump(
            $storyRepository->findAllByCriteria(new PlanningPokerStoryCriteria(
                $planningPokerId,
                PlanningPokerStoryStatus::createByNameOrNull($status),
                $limit,
                $offset
            ))->dumpBy($user)
        );
    }
}
