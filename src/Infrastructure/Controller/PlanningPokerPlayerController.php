<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Controller;

use Dvlpm\Stream\Application\Command\RemovePlanningPokerPlayerCommand;
use Dvlpm\Stream\Application\Command\UpdatePlanningPokerPlayerCommand;
use Dvlpm\Stream\Application\CommandBus\CommandBusInterface;
use Dvlpm\Stream\Application\Dto\UpdatePlanningPokerPlayerDto;
use Dvlpm\Stream\Application\Message\Message;
use Dvlpm\Stream\Application\Message\MessageBag;
use Dvlpm\Stream\Domain\Content\Exception\UndefinedPlanningPokerPlayerException;
use Dvlpm\Stream\Domain\Content\Repository\PlanningPokerPlayerRepositoryInterface;
use Dvlpm\Stream\Domain\User\Model\User;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final class PlanningPokerPlayerController extends AbstractController
{
    /**
     * @Route("/planning-poker-player/{planningPokerPlayerId}", methods={"PUT"})
     * @param CommandBusInterface $commandBus
     * @param PlanningPokerPlayerRepositoryInterface $planningPokerPlayerRepository
     * @param User $user
     * @param UuidInterface $planningPokerPlayerId
     * @param UpdatePlanningPokerPlayerDto $dto
     * @return JsonResponse
     * @throws UndefinedPlanningPokerPlayerException
     */
    public function update(
        CommandBusInterface $commandBus,
        PlanningPokerPlayerRepositoryInterface $planningPokerPlayerRepository,
        User $user,
        UuidInterface $planningPokerPlayerId,
        UpdatePlanningPokerPlayerDto $dto
    ): JsonResponse {
        $commandBus->handle(new UpdatePlanningPokerPlayerCommand(
            $user->getId(),
            $planningPokerPlayerId,
            $dto->status,
        ));

        return $this->dump($planningPokerPlayerRepository->findOneByIdOrFail($planningPokerPlayerId)->dumpBy($user));
    }

    /**
     * @Route("/planning-poker-player/{planningPokerPlayerId}", methods={"DELETE"})
     * @param CommandBusInterface $commandBus
     * @param User $user
     * @param UuidInterface $planningPokerPlayerId
     * @return JsonResponse
     */
    public function remove(
        CommandBusInterface $commandBus,
        User $user,
        UuidInterface $planningPokerPlayerId
    ): JsonResponse {
        $commandBus->handle(new RemovePlanningPokerPlayerCommand(
            $user->getId(),
            $planningPokerPlayerId,
        ));

        return $this->messageBag(MessageBag::withMessages(Message::notification(
            sprintf('Player %s removed', $planningPokerPlayerId)
        )));
    }
}
