<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Controller;

use Dvlpm\Stream\Application\Command\AddPlanningPokerStoryVoteToPlanningPokerStoryCommand;
use Dvlpm\Stream\Application\Command\RemovePlanningPokerStoryCommand;
use Dvlpm\Stream\Application\Command\RemovePlanningPokerStoryVotesCommand;
use Dvlpm\Stream\Application\Command\UpdatePlanningPokerStoryCommand;
use Dvlpm\Stream\Application\CommandBus\CommandBusInterface;
use Dvlpm\Stream\Application\Dto\CreatePlanningPokerStoryVoteDto;
use Dvlpm\Stream\Application\Dto\UpdatePlanningPokerStoryDto;
use Dvlpm\Stream\Application\Message\Message;
use Dvlpm\Stream\Application\Message\MessageBag;
use Dvlpm\Stream\Application\PayloadFactory\AddPlanningPokerStoryVoteToPlanningPokerStoryPayloadFactory;
use Dvlpm\Stream\Application\PayloadFactory\UpdatePlanningPokerStoryPayloadFactory;
use Dvlpm\Stream\Domain\Content\Exception\PlanningPokerStoryCannotBeViewedByUserException;
use Dvlpm\Stream\Domain\Content\Exception\UndefinedPlanningPokerStoryException;
use Dvlpm\Stream\Domain\Content\Repository\PlanningPokerStoryRepositoryInterface;
use Dvlpm\Stream\Domain\User\Model\User;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final class PlanningPokerStoryController extends AbstractController
{
    /**
     * @Route("/planning-poker-story/{planningPokerStoryId}", methods={"GET"})
     * @param PlanningPokerStoryRepositoryInterface $planningPokerStoryRepository
     * @param User $user
     * @param UuidInterface $planningPokerStoryId
     * @return JsonResponse
     * @throws PlanningPokerStoryCannotBeViewedByUserException
     * @throws UndefinedPlanningPokerStoryException
     */
    public function show(
        PlanningPokerStoryRepositoryInterface $planningPokerStoryRepository,
        User $user,
        UuidInterface $planningPokerStoryId
    ): JsonResponse {
        return $this->dump($planningPokerStoryRepository->findOneByIdOrFail($planningPokerStoryId)->dumpBy($user));
    }

    /**
     * @Route("/planning-poker-story/{planningPokerStoryId}/vote", methods={"POST"})
     * @param AddPlanningPokerStoryVoteToPlanningPokerStoryPayloadFactory $payloadFactory
     * @param CommandBusInterface $commandBus
     * @param PlanningPokerStoryRepositoryInterface $planningPokerStoryRepository
     * @param User $user
     * @param UuidInterface $planningPokerStoryId
     * @param CreatePlanningPokerStoryVoteDto $dto
     * @return JsonResponse
     * @throws UndefinedPlanningPokerStoryException|PlanningPokerStoryCannotBeViewedByUserException
     */
    public function addVote(
        AddPlanningPokerStoryVoteToPlanningPokerStoryPayloadFactory $payloadFactory,
        CommandBusInterface $commandBus,
        PlanningPokerStoryRepositoryInterface $planningPokerStoryRepository,
        User $user,
        UuidInterface $planningPokerStoryId,
        CreatePlanningPokerStoryVoteDto $dto
    ): JsonResponse {
        $commandBus->handle(new AddPlanningPokerStoryVoteToPlanningPokerStoryCommand(
            $user->getId(),
            $planningPokerStoryId,
            $payloadFactory->createFromDto($dto)
        ));

        return $this->dump($planningPokerStoryRepository->findOneByIdOrFail($planningPokerStoryId)->dumpBy($user));
    }

    /**
     * @Route("/planning-poker-story/{planningPokerStoryId}", methods={"PUT"})
     * @param UpdatePlanningPokerStoryPayloadFactory $payloadFactory
     * @param CommandBusInterface $commandBus
     * @param PlanningPokerStoryRepositoryInterface $planningPokerStoryRepository
     * @param User $user
     * @param UuidInterface $planningPokerStoryId
     * @param UpdatePlanningPokerStoryDto $dto
     * @return JsonResponse
     * @throws UndefinedPlanningPokerStoryException|PlanningPokerStoryCannotBeViewedByUserException
     */
    public function update(
        UpdatePlanningPokerStoryPayloadFactory $payloadFactory,
        CommandBusInterface $commandBus,
        PlanningPokerStoryRepositoryInterface $planningPokerStoryRepository,
        User $user,
        UuidInterface $planningPokerStoryId,
        UpdatePlanningPokerStoryDto $dto
    ): JsonResponse {
        $commandBus->handle(new UpdatePlanningPokerStoryCommand(
            $user->getId(),
            $planningPokerStoryId,
            $payloadFactory->createFromDto($dto)
        ));

        return $this->dump($planningPokerStoryRepository->findOneByIdOrFail($planningPokerStoryId)->dumpBy($user));
    }

    /**
     * @Route("/planning-poker-story/{planningPokerStoryId}/votes", methods={"DELETE"})
     * @param CommandBusInterface $commandBus
     * @param PlanningPokerStoryRepositoryInterface $planningPokerStoryRepository
     * @param User $user
     * @param UuidInterface $planningPokerStoryId
     * @return JsonResponse
     * @throws PlanningPokerStoryCannotBeViewedByUserException
     * @throws UndefinedPlanningPokerStoryException
     */
    public function removeVotes(
        CommandBusInterface $commandBus,
        PlanningPokerStoryRepositoryInterface $planningPokerStoryRepository,
        User $user,
        UuidInterface $planningPokerStoryId
    ): JsonResponse {
        $commandBus->handle(new RemovePlanningPokerStoryVotesCommand(
            $user->getId(),
            $planningPokerStoryId
        ));

        return $this->dump($planningPokerStoryRepository->findOneByIdOrFail($planningPokerStoryId)->dumpBy($user));
    }

    /**
     * @Route("/planning-poker-story/{planningPokerStoryId}", methods={"DELETE"})
     * @param CommandBusInterface $commandBus
     * @param User $user
     * @param UuidInterface $planningPokerStoryId
     * @return JsonResponse
     */
    public function remove(
        CommandBusInterface $commandBus,
        User $user,
        UuidInterface $planningPokerStoryId
    ): JsonResponse {
        $commandBus->handle(new RemovePlanningPokerStoryCommand(
            $user->getId(),
            $planningPokerStoryId,
        ));

        return $this->messageBag(MessageBag::withMessages(Message::notification(
            sprintf('Story %s removed', $planningPokerStoryId)
        )));
    }
}
