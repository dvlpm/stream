<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Controller;

use Dvlpm\Stream\Application\Command\AddContentToRoomCommand;
use Dvlpm\Stream\Application\Command\AddParticipantToRoomCommand;
use Dvlpm\Stream\Application\Command\EnterRoomCommand;
use Dvlpm\Stream\Application\Command\RemoveContentFromRoomCommand;
use Dvlpm\Stream\Application\Command\RemoveRoomCommand;
use Dvlpm\Stream\Application\Command\UpdateRoomCommand;
use Dvlpm\Stream\Application\CommandBus\CommandBusInterface;
use Dvlpm\Stream\Application\Dto\AddContentToRoomDto;
use Dvlpm\Stream\Application\Dto\CreateParticipantDto;
use Dvlpm\Stream\Application\Dto\UpdateRoomDto;
use Dvlpm\Stream\Application\Message\Message;
use Dvlpm\Stream\Application\Message\MessageBag;
use Dvlpm\Stream\Application\PayloadFactory\AddParticipantsToRoomPayloadFactory;
use Dvlpm\Stream\Application\PayloadFactory\UpdateRoomPayloadFactory;
use Dvlpm\Stream\Domain\Space\Exception\RoomCannotBeViewedByUserException;
use Dvlpm\Stream\Domain\Space\Exception\UndefinedRoomException;
use Dvlpm\Stream\Domain\Space\Model\Key;
use Dvlpm\Stream\Domain\Space\Payload\EnterRoomPayload;
use Dvlpm\Stream\Domain\Space\Repository\RoomRepositoryInterface;
use Dvlpm\Stream\Domain\User\Model\User;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final class RoomController extends AbstractController
{
    /**
     * @Route("/room/{roomId}", methods={"GET"})
     * @param CommandBusInterface $commandBus
     * @param RoomRepositoryInterface $roomRepository
     * @param UuidInterface $roomId
     * @param User $user
     * @param Key|null $key
     * @return JsonResponse
     * @throws RoomCannotBeViewedByUserException
     * @throws UndefinedRoomException
     */
    public function show(
        CommandBusInterface $commandBus,
        RoomRepositoryInterface $roomRepository,
        UuidInterface $roomId,
        User $user,
        ?Key $key = null
    ): JsonResponse {
        $room = $roomRepository->findOneByIdOrFail($roomId);

        if (!$room->isUserParticipant($user)) {
            $commandBus->handle(new EnterRoomCommand(
                $user->getId(),
                $roomId,
                new EnterRoomPayload($key)
            ));
        }

        return $this->dump($room->dumpBy($user));
    }

    /**
     * @Route("/room/{roomId}/content", methods={"POST"})
     * @param CommandBusInterface $commandBus
     * @param RoomRepositoryInterface $roomRepository
     * @param User $user
     * @param UuidInterface $roomId
     * @param AddContentToRoomDto $dto
     * @return JsonResponse
     * @throws UndefinedRoomException
     */
    public function addContent(
        CommandBusInterface $commandBus,
        RoomRepositoryInterface $roomRepository,
        User $user,
        UuidInterface $roomId,
        AddContentToRoomDto $dto
    ): JsonResponse {
        $command = new AddContentToRoomCommand(
            $user->getId(),
            $roomId,
            $dto->id
        );

        $commandBus->handle($command);

        return $this->dump($roomRepository->findOneByIdOrFail($roomId)->dumpBy($user));
    }

    /**
     * @Route("/room/{roomId}/content/{contentId}", methods={"DELETE"})
     * @param CommandBusInterface $commandBus
     * @param RoomRepositoryInterface $roomRepository
     * @param User $user
     * @param UuidInterface $roomId
     * @param UuidInterface $contentId
     * @return JsonResponse
     * @throws UndefinedRoomException
     */
    public function removeContent(
        CommandBusInterface $commandBus,
        RoomRepositoryInterface $roomRepository,
        User $user,
        UuidInterface $roomId,
        UuidInterface $contentId
    ): JsonResponse {
        $command = new RemoveContentFromRoomCommand(
            $user->getId(),
            $roomId,
            $contentId
        );

        $commandBus->handle($command);

        return $this->dump($roomRepository->findOneByIdOrFail($roomId)->dumpBy($user));
    }

    /**
     * @Route("/room/{roomId}/participant", methods={"POST"})
     * @param AddParticipantsToRoomPayloadFactory $payloadFactory
     * @param CommandBusInterface $commandBus
     * @param RoomRepositoryInterface $roomRepository
     * @param User $user
     * @param UuidInterface $roomId
     * @param CreateParticipantDto $dto
     * @return JsonResponse
     * @throws UndefinedRoomException
     */
    public function addParticipant(
        AddParticipantsToRoomPayloadFactory $payloadFactory,
        CommandBusInterface $commandBus,
        RoomRepositoryInterface $roomRepository,
        User $user,
        UuidInterface $roomId,
        CreateParticipantDto $dto
    ): JsonResponse {
        $command = new AddParticipantToRoomCommand(
            $user->getId(),
            $roomId,
            $payloadFactory->createFromDto($dto)
        );

        $commandBus->handle($command);

        return $this->dump($roomRepository->findOneByIdOrFail($roomId)->dumpBy($user));
    }

    /**
     * @Route("/room/{roomId}", methods={"PUT"})
     * @param CommandBusInterface $commandBus
     * @param UpdateRoomPayloadFactory $payloadFactory
     * @param RoomRepositoryInterface $roomRepository
     * @param UuidInterface $roomId
     * @param User $user
     * @param UpdateRoomDto $dto
     * @return JsonResponse
     * @throws UndefinedRoomException
     */
    public function update(
        CommandBusInterface $commandBus,
        UpdateRoomPayloadFactory $payloadFactory,
        RoomRepositoryInterface $roomRepository,
        UuidInterface $roomId,
        User $user,
        UpdateRoomDto $dto
    ): JsonResponse {
        $command = new UpdateRoomCommand(
            $user->getId(),
            $roomId,
            $payloadFactory->createFromDto($dto)
        );

        $commandBus->handle($command);

        return $this->dump($roomRepository->findOneByIdOrFail($roomId)->dumpBy($user));
    }

    /**
     * @Route("/room/{roomId}", methods={"DELETE"})
     * @param User $user
     * @param CommandBusInterface $commandBus
     * @param UuidInterface $roomId
     * @return JsonResponse
     */
    public function remove(
        CommandBusInterface $commandBus,
        User $user,
        UuidInterface $roomId
    ): JsonResponse {
        $command = new RemoveRoomCommand(
            $user->getId(),
            $roomId
        );

        $commandBus->handle($command);

        return $this->messageBag(MessageBag::withMessages(
            Message::notification(sprintf('Room %s removed.', $roomId))
        ));
    }
}
