<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Controller;

use Dvlpm\Stream\Application\Command\UpdateUserCommand;
use Dvlpm\Stream\Application\CommandBus\CommandBusInterface;
use Dvlpm\Stream\Application\Dto\UpdateUserDto;
use Dvlpm\Stream\Application\PayloadFactory\UpdateUserPayloadFactory;
use Dvlpm\Stream\Domain\User\Exception\UndefinedUserException;
use Dvlpm\Stream\Domain\User\Model\User;
use Dvlpm\Stream\Domain\User\Repository\UserCriteria;
use Dvlpm\Stream\Domain\User\Repository\UserRepositoryInterface;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final class UserController extends AbstractController
{
    /**
     * @Route("/user/me", methods={"GET"})
     * @param User $user
     * @return JsonResponse
     */
    public function me(
        User $user
    ): JsonResponse {
        return $this->dump($user->dumpBy($user));
    }

    /**
     * @Route("/user/me", methods={"PUT"})
     * @param UpdateUserPayloadFactory $payloadFactory
     * @param CommandBusInterface $commandBus
     * @param User $user
     * @param UpdateUserDto $dto
     * @return JsonResponse
     */
    public function updateMe(
        UpdateUserPayloadFactory $payloadFactory,
        CommandBusInterface $commandBus,
        User $user,
        UpdateUserDto $dto
    ): JsonResponse {
        $command = new UpdateUserCommand(
            $user->getId(),
            $payloadFactory->createFromDto($dto)
        );

        $commandBus->handle($command);

        return $this->dump($user->dumpBy($user));
    }

    /**
     * @Route("/user/{userId}")
     * @param UserRepositoryInterface $userRepository
     * @param User $user
     * @param UuidInterface $userId
     * @return JsonResponse
     * @throws UndefinedUserException
     */
    public function show(
        UserRepositoryInterface $userRepository,
        User $user,
        UuidInterface $userId
    ): JsonResponse {
        return $this->dump($userRepository->findOneByIdOrFail($userId)->dumpBy($user));
    }

    /**
     * @Route("/users")
     * @param UserRepositoryInterface $userRepository
     * @param User $user
     * @param string|null $searchQuery
     * @param int|null $limit
     * @param int|null $offset
     * @return JsonResponse
     */
    public function list(
        UserRepositoryInterface $userRepository,
        User $user,
        ?string $searchQuery = null,
        ?int $limit = null,
        ?int $offset = null
    ): JsonResponse {
        return $this->dump($userRepository->findAllByCriteria(new UserCriteria(
            $searchQuery,
            $limit,
            $offset
        ))->dumpBy($user));
    }
}
