<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\DependencyInjection\EnvVarProcessor;

use Symfony\Component\DependencyInjection\EnvVarProcessorInterface;

final class UrlEncodeEnvVarProcessor implements EnvVarProcessorInterface
{
    public function getEnv($prefix, $name, \Closure $getEnv): string
    {
        $env = $getEnv($name);

        return urlencode($env);
    }

    public static function getProvidedTypes(): array
    {
        return [
            'urlencode' => 'string',
        ];
    }
}
