<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Doctrine\DBAL\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;
use Dvlpm\Stream\Infrastructure\Doctrine\EventListener\SerializerAccessEventListener;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

abstract class AbstractSerializableType extends JsonType
{
    protected ?SerializerInterface $serializer = null;

    abstract protected function getSerializableType(): string;

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $this->ensureInitialized($platform);

        return $this->serializer->deserialize($value, $this->getSerializableType(), JsonEncoder::FORMAT);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        $this->ensureInitialized($platform);

        return $this->serializer->serialize(
            $value,
            JsonEncoder::FORMAT,
            [
                AbstractObjectNormalizer::SKIP_NULL_VALUES => true,
            ]
        );
    }

    private function ensureInitialized(AbstractPlatform $platform): void
    {
        if ($this->serializer !== null) {
            return;
        }

        // This is a very dirty hack to access serializer, but this is a way...
        $listeners = $platform->getEventManager()->getListeners('serializer.access');
        /** @var SerializerAccessEventListener $serializerAccessEventListener */
        $serializerAccessEventListener = array_shift($listeners);

        $this->serializer = $serializerAccessEventListener->getSerializer();
    }
}
