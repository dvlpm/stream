<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Doctrine\DBAL\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Dvlpm\Stream\Domain\User\Model\ExternalId;

final class ExternalIdType extends StringType
{
    public function getName(): string
    {
        return 'external_id';
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?ExternalId
    {
        return $value !== null
            ? ExternalId::fromValue($value)
            : null;
    }
}
