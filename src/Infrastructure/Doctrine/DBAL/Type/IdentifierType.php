<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Doctrine\DBAL\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Dvlpm\Stream\Domain\Space\Model\Identifier;

final class IdentifierType extends StringType
{
    public function getName(): string
    {
        return 'identifier';
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?Identifier
    {
        return $value !== null
            ? Identifier::fromValue($value)
            : null;
    }
}
