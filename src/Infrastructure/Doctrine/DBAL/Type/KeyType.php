<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Doctrine\DBAL\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Dvlpm\Stream\Domain\Space\Model\Key;

final class KeyType extends StringType
{
    public function getName(): string
    {
        return 'key';
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?Key
    {
        return $value !== null
            ? Key::fromValue($value)
            : null;
    }
}
