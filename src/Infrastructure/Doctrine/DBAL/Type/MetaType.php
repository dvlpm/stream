<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Doctrine\DBAL\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;
use Dvlpm\Stream\Domain\Common\Model\Meta;

final class MetaType extends JsonType
{
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return Meta::fromData(parent::convertToPHPValue($value, $platform));
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return parent::convertToDatabaseValue($value->getData(), $platform);
    }
}
