<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Doctrine\DBAL\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Dvlpm\Stream\Domain\Common\Model\Name;

final class NameType extends StringType
{
    public function getName(): string
    {
        return 'name';
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?Name
    {
        return $value !== null
            ? Name::fromValue($value)
            : null;
    }
}
