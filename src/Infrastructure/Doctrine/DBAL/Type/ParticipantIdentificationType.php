<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Doctrine\DBAL\Type;

use Dvlpm\Stream\Domain\Space\Model\ParticipantIdentification;

final class ParticipantIdentificationType extends AbstractSerializableType
{
    protected function getSerializableType(): string
    {
        return ParticipantIdentification::class;
    }
}
