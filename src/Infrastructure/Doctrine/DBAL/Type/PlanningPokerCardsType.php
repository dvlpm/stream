<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Doctrine\DBAL\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerCard;

final class PlanningPokerCardsType extends JsonType
{
    public function convertToPHPValue($value, AbstractPlatform $platform): array
    {
        $cards = [];
        $cardValues = parent::convertToPHPValue($value, $platform);

        if ($cardValues === null) {
            return [];
        }

        foreach ($cardValues as $cardValue) {
            $cards[] = PlanningPokerCard::createByName($cardValue);
        }

        return $cards;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        $cardValues = [];

        if ($value === null) {
            return null;
        }

        foreach ($value as $card) {
            $cardValues[] = (string) $card;
        }

        return parent::convertToDatabaseValue($cardValues, $platform);
    }
}
