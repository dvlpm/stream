<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Doctrine\DBAL\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Dvlpm\Stream\Domain\User\Model\Username;

final class UsernameType extends StringType
{
    public function getName(): string
    {
        return 'username';
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?Username
    {
        return $value !== null
            ? Username::fromValue($value)
            : null;
    }
}
