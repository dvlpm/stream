<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Doctrine\EventListener;

use Symfony\Component\Serializer\SerializerInterface;

final class SerializerAccessEventListener
{
    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function getSerializer(): SerializerInterface
    {
        return $this->serializer;
    }
}
