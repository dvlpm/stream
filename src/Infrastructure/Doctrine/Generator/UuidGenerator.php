<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Doctrine\Generator;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;
use Ramsey\Uuid\Uuid;
use ReflectionClass;

final class UuidGenerator extends AbstractIdGenerator
{
    public function generate(EntityManager $em, $entity): string
    {
        $reflectionEntityClass = new ReflectionClass($entity);

        $idProperty = $reflectionEntityClass->getProperty('id');
        $idProperty->setAccessible(true);
        $id = $idProperty->getValue($entity);

        return $id !== null ? (string) $id : (string) Uuid::uuid4();
    }
}
