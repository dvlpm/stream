<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Event;

use Dvlpm\Stream\Application\Event\EventDispatcherInterface;
use Dvlpm\Stream\Application\ProcessManager\ProcessManager;
use Dvlpm\Stream\Domain\Common\Event\DomainEventInterface;
use Dvlpm\Stream\Domain\Common\Event\EventStoreInterface;
use Symfony\Contracts\Service\ServiceSubscriberInterface;
use Symfony\Contracts\Service\ServiceSubscriberTrait;

final class EventDispatcherAdapter implements EventDispatcherInterface, ServiceSubscriberInterface
{
    use ServiceSubscriberTrait;

    /** @var DomainEventInterface[] */
    private array $events = [];

    public function dispatchFrom(EventStoreInterface $eventStore): void
    {
        $this->events = [...$this->events, ...$eventStore->flushEvents()];
    }

    public function flush(): void
    {
        $this->processManager()->handleEvents(...$this->events);
        $this->events = [];
    }

    private function processManager(): ProcessManager
    {
        return $this->container->get(__METHOD__);
    }
}
