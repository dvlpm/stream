<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Exception;

use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;

final class DtoArgumentValueResolverException extends \Exception
{
    private ConstraintViolationListInterface $errors;

    private function __construct(?ConstraintViolationListInterface $errors = null)
    {
        parent::__construct();
        $this->errors = $errors ?? new ConstraintViolationList();
    }

    public static function withErrors(ConstraintViolationListInterface $errors): self
    {
        return new static($errors);
    }

    public static function undeserializableData(): self
    {
        return new static();
    }

    public function hasErrors(): bool
    {
        return count($this->errors) > 0;
    }

    /**
     * @return ConstraintViolationInterface[]
     */
    public function getErrors(): iterable
    {
        return $this->errors;
    }
}
