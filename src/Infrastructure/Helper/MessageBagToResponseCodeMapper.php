<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Helper;

use Dvlpm\Stream\Application\Message\MessageBag;
use Symfony\Component\HttpFoundation\Response;

final class MessageBagToResponseCodeMapper
{
    public static function map(MessageBag $messageBag): int
    {
        if (!empty($messageBag->getErrors())) {
            return Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        if (!empty($messageBag->getWarnings())) {
            return Response::HTTP_BAD_REQUEST;
        }

        return Response::HTTP_OK;
    }
}
