<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200719143755 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE planning_poker_player (id UUID NOT NULL, poker_id UUID DEFAULT NULL, user_id UUID DEFAULT NULL, status VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A38908ECDF5B0164 ON planning_poker_player (poker_id)');
        $this->addSql('CREATE INDEX IDX_A38908ECA76ED395 ON planning_poker_player (user_id)');
        $this->addSql('COMMENT ON COLUMN planning_poker_player.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN planning_poker_player.poker_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN planning_poker_player.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN planning_poker_player.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN planning_poker_player.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE planning_poker_story (id UUID NOT NULL, poker_id UUID DEFAULT NULL, name VARCHAR(255) NOT NULL, _order INT NOT NULL, score VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, link VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_15044518DF5B0164 ON planning_poker_story (poker_id)');
        $this->addSql('COMMENT ON COLUMN planning_poker_story.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN planning_poker_story.poker_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN planning_poker_story.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN planning_poker_story.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE room (id UUID NOT NULL, hall_id UUID DEFAULT NULL, name VARCHAR(255) NOT NULL, access_type VARCHAR(255) NOT NULL, key VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_729F519B52AFCFD6 ON room (hall_id)');
        $this->addSql('COMMENT ON COLUMN room.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN room.hall_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN room.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN room.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE content_room (room_id UUID NOT NULL, content_id UUID NOT NULL, PRIMARY KEY(room_id, content_id))');
        $this->addSql('CREATE INDEX IDX_BFFDBC5E54177093 ON content_room (room_id)');
        $this->addSql('CREATE INDEX IDX_BFFDBC5E84A0A3ED ON content_room (content_id)');
        $this->addSql('COMMENT ON COLUMN content_room.room_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN content_room.content_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE participant (id UUID NOT NULL, room_id UUID DEFAULT NULL, user_id UUID DEFAULT NULL, status VARCHAR(255) NOT NULL, identification JSON NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D79F6B1154177093 ON participant (room_id)');
        $this->addSql('CREATE INDEX IDX_D79F6B11A76ED395 ON participant (user_id)');
        $this->addSql('COMMENT ON COLUMN participant.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN participant.room_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN participant.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN participant.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN participant.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE planning_poker (id UUID NOT NULL, cards JSON NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN planning_poker.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE email (id UUID NOT NULL, email VARCHAR(255) NOT NULL, is_confirmed BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN email.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE content (id UUID NOT NULL, owner_id UUID DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FEC530A97E3C61F9 ON content (owner_id)');
        $this->addSql('COMMENT ON COLUMN content.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN content.owner_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN content.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN content.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE "user" (id UUID NOT NULL, role VARCHAR(255) NOT NULL, external_id VARCHAR(255) DEFAULT NULL, meta JSON NOT NULL, name VARCHAR(255) DEFAULT NULL, username VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN "user".id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "user".created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN "user".updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE host (hall_id UUID NOT NULL, user_id UUID NOT NULL, role VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(hall_id, user_id))');
        $this->addSql('CREATE INDEX IDX_CF2713FD52AFCFD6 ON host (hall_id)');
        $this->addSql('CREATE INDEX IDX_CF2713FDA76ED395 ON host (user_id)');
        $this->addSql('COMMENT ON COLUMN host.hall_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN host.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN host.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN host.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE youtube_stream (id UUID NOT NULL, link VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN youtube_stream.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE contact (id UUID NOT NULL, user_id UUID DEFAULT NULL, is_preferable BOOLEAN NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_4C62E638A76ED395 ON contact (user_id)');
        $this->addSql('COMMENT ON COLUMN contact.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN contact.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN contact.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN contact.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE planning_poker_story_vote (user_id UUID NOT NULL, story_id UUID NOT NULL, card VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(user_id, story_id))');
        $this->addSql('CREATE INDEX IDX_CBD3CFFBA76ED395 ON planning_poker_story_vote (user_id)');
        $this->addSql('CREATE INDEX IDX_CBD3CFFBAA5D4036 ON planning_poker_story_vote (story_id)');
        $this->addSql('COMMENT ON COLUMN planning_poker_story_vote.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN planning_poker_story_vote.story_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN planning_poker_story_vote.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN planning_poker_story_vote.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE hall (id UUID NOT NULL, identifier VARCHAR(255) NOT NULL, name VARCHAR(255) DEFAULT NULL, description TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1B8FA83F772E836A ON hall (identifier)');
        $this->addSql('COMMENT ON COLUMN hall.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN hall.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN hall.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE planning_poker_player ADD CONSTRAINT FK_A38908ECDF5B0164 FOREIGN KEY (poker_id) REFERENCES planning_poker (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE planning_poker_player ADD CONSTRAINT FK_A38908ECA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE planning_poker_story ADD CONSTRAINT FK_15044518DF5B0164 FOREIGN KEY (poker_id) REFERENCES planning_poker (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE room ADD CONSTRAINT FK_729F519B52AFCFD6 FOREIGN KEY (hall_id) REFERENCES hall (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE content_room ADD CONSTRAINT FK_BFFDBC5E54177093 FOREIGN KEY (room_id) REFERENCES room (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE content_room ADD CONSTRAINT FK_BFFDBC5E84A0A3ED FOREIGN KEY (content_id) REFERENCES content (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE participant ADD CONSTRAINT FK_D79F6B1154177093 FOREIGN KEY (room_id) REFERENCES room (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE participant ADD CONSTRAINT FK_D79F6B11A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE planning_poker ADD CONSTRAINT FK_2F1AE12BBF396750 FOREIGN KEY (id) REFERENCES content (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE email ADD CONSTRAINT FK_E7927C74BF396750 FOREIGN KEY (id) REFERENCES contact (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE content ADD CONSTRAINT FK_FEC530A97E3C61F9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE host ADD CONSTRAINT FK_CF2713FD52AFCFD6 FOREIGN KEY (hall_id) REFERENCES hall (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE host ADD CONSTRAINT FK_CF2713FDA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE youtube_stream ADD CONSTRAINT FK_205B22FCBF396750 FOREIGN KEY (id) REFERENCES content (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE contact ADD CONSTRAINT FK_4C62E638A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE planning_poker_story_vote ADD CONSTRAINT FK_CBD3CFFBA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE planning_poker_story_vote ADD CONSTRAINT FK_CBD3CFFBAA5D4036 FOREIGN KEY (story_id) REFERENCES planning_poker_story (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE planning_poker_story_vote DROP CONSTRAINT FK_CBD3CFFBAA5D4036');
        $this->addSql('ALTER TABLE content_room DROP CONSTRAINT FK_BFFDBC5E54177093');
        $this->addSql('ALTER TABLE participant DROP CONSTRAINT FK_D79F6B1154177093');
        $this->addSql('ALTER TABLE planning_poker_player DROP CONSTRAINT FK_A38908ECDF5B0164');
        $this->addSql('ALTER TABLE planning_poker_story DROP CONSTRAINT FK_15044518DF5B0164');
        $this->addSql('ALTER TABLE content_room DROP CONSTRAINT FK_BFFDBC5E84A0A3ED');
        $this->addSql('ALTER TABLE planning_poker DROP CONSTRAINT FK_2F1AE12BBF396750');
        $this->addSql('ALTER TABLE youtube_stream DROP CONSTRAINT FK_205B22FCBF396750');
        $this->addSql('ALTER TABLE planning_poker_player DROP CONSTRAINT FK_A38908ECA76ED395');
        $this->addSql('ALTER TABLE participant DROP CONSTRAINT FK_D79F6B11A76ED395');
        $this->addSql('ALTER TABLE content DROP CONSTRAINT FK_FEC530A97E3C61F9');
        $this->addSql('ALTER TABLE host DROP CONSTRAINT FK_CF2713FDA76ED395');
        $this->addSql('ALTER TABLE contact DROP CONSTRAINT FK_4C62E638A76ED395');
        $this->addSql('ALTER TABLE planning_poker_story_vote DROP CONSTRAINT FK_CBD3CFFBA76ED395');
        $this->addSql('ALTER TABLE email DROP CONSTRAINT FK_E7927C74BF396750');
        $this->addSql('ALTER TABLE room DROP CONSTRAINT FK_729F519B52AFCFD6');
        $this->addSql('ALTER TABLE host DROP CONSTRAINT FK_CF2713FD52AFCFD6');
        $this->addSql('DROP TABLE planning_poker_player');
        $this->addSql('DROP TABLE planning_poker_story');
        $this->addSql('DROP TABLE room');
        $this->addSql('DROP TABLE content_room');
        $this->addSql('DROP TABLE participant');
        $this->addSql('DROP TABLE planning_poker');
        $this->addSql('DROP TABLE email');
        $this->addSql('DROP TABLE content');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE host');
        $this->addSql('DROP TABLE youtube_stream');
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE planning_poker_story_vote');
        $this->addSql('DROP TABLE hall');
    }
}
