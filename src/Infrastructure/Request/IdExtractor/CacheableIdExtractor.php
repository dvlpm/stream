<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Request\IdExtractor;

use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\Request;

final class CacheableIdExtractor implements IdExtractorInterface
{
    private IdExtractorInterface $idExtractor;
    private array $requestHashesToIdMap = [];

    public function __construct(IdExtractorInterface $idExtractor)
    {
        $this->idExtractor = $idExtractor;
    }

    public function extract(Request $request): ?UuidInterface
    {
        $requestHash = spl_object_hash($request);

        if (isset($this->requestHashesToIdMap[$requestHash])) {
            return $this->requestHashesToIdMap[$requestHash];
        }

        return $this->requestHashesToIdMap[$requestHash] = $this->idExtractor->extract($request);
    }
}
