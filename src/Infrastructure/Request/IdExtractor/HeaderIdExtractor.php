<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Request\IdExtractor;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\Request;

final class HeaderIdExtractor implements IdExtractorInterface
{
    private const HEADER_NAME = 'user-id';

    public function extract(Request $request): ?UuidInterface
    {
        $idString = $request->headers->get(self::HEADER_NAME);

        return $idString !== null ? Uuid::fromString($idString) : null;
    }
}
