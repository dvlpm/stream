<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Request\IdExtractor;

use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\Request;

interface IdExtractorInterface
{
    public function extract(Request $request): ?UuidInterface;
}
