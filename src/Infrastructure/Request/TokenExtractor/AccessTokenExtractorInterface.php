<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Request\TokenExtractor;

use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Symfony\Component\HttpFoundation\Request;

interface AccessTokenExtractorInterface
{
    public function extract(Request $request): ?AccessToken;
}
