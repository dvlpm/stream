<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Request\TokenExtractor;

use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Symfony\Component\HttpFoundation\Request;

final class BearerAccessTokenExtractor implements AccessTokenExtractorInterface
{
    private const AUTHORIZATION_HEADER = 'authorization';
    private const TOKEN_TYPE = 'bearer';

    public function extract(Request $request): ?AccessToken
    {
        /** @var string $authorizationHeaderString */
        $authorizationHeaderString = $request->headers->get(self::AUTHORIZATION_HEADER);

        if ($authorizationHeaderString === null) {
            return null;
        }

        $chunks = array_values(array_filter(explode(' ', $authorizationHeaderString)));

        $accessTokenString = ((count($chunks) === 2) && (strtolower($chunks[0]) === self::TOKEN_TYPE))
            ? $chunks[1]
            : null;

        return $accessTokenString !== null ? AccessToken::fromValue($accessTokenString) : null;
    }
}
