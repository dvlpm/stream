<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Request\TokenExtractor;

use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Symfony\Component\HttpFoundation\Request;

final class CacheableAccessTokenExtractor implements AccessTokenExtractorInterface
{
    private ?AccessToken $accessToken = null;
    private bool $isCacheInitialized = false;

    private AccessTokenExtractorInterface $accessTokenExtractor;

    public function __construct(AccessTokenExtractorInterface $accessTokenExtractor)
    {
        $this->accessTokenExtractor = $accessTokenExtractor;
    }

    public function extract(Request $request): ?AccessToken
    {
        if (!$this->isCacheInitialized) {
            $this->accessToken = $this->accessTokenExtractor->extract($request);
            $this->isCacheInitialized = true;
        }

        return $this->accessToken;
    }
}
