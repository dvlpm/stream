<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Serializer\Normalizer;

use DateTimeInterface;
use Dvlpm\Stream\Application\Validation\DataObject\InvalidDateImmutable;
use Throwable;

use const DATE_ATOM;

final class DateTimeNormalizer extends \Symfony\Component\Serializer\Normalizer\DateTimeNormalizer
{
    public const INVALID_DATE_STRING = '1970-01-01';
    public const INVALID_DATE_FORMAT = DATE_ATOM;

    public function denormalize($data, string $type, string $format = null, array $context = []): ?DateTimeInterface
    {
        try {
            return parent::denormalize($data, $type, $format, $context);
        } catch (Throwable $exception) {
            return InvalidDateImmutable::create();
        }
    }
}

