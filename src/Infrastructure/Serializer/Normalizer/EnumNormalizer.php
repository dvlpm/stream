<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Serializer\Normalizer;

use Dvlpm\Stream\Domain\Common\Model\Enum;
use Paillechat\Enum\Exception\EnumException;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class EnumNormalizer implements NormalizerInterface, DenormalizerInterface
{
    public function denormalize($data, string $type, string $format = null, array $context = []): Enum
    {
        if (!is_string($data)) {
            throw new InvalidArgumentException(
                sprintf(
                    'Data expected to be string, %s given.',
                    gettype($data)
                )
            );
        }
        if (!is_subclass_of($type, Enum::class)) {
            throw new InvalidArgumentException('Unsupported class: ' . $type);
        }

        try {
            return $type::createByName($data);
        } catch (EnumException $throwable) {
            return $type::createByName(Enum::UNRECOGNIZED);
        }
    }

    public function supportsDenormalization($data, $type, string $format = null, array $context = []): bool
    {
        return is_subclass_of($type, Enum::class);
    }

    public function normalize($object, string $format = null, array $context = []): string
    {
        return (string) $object;
    }

    public function supportsNormalization($data, string $format = null): bool
    {
        return $data instanceof Enum;
    }
}
