<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Serializer\Normalizer;

use Dvlpm\Stream\Domain\Common\Model\Meta;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class MetaNormalizer implements NormalizerInterface, DenormalizerInterface
{
    public function supportsDenormalization($data, $type, string $format = null): bool
    {
        return $type === Meta::class;
    }

    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        return Meta::fromData($data);
    }

    public function normalize($object, string $format = null, array $context = [])
    {
        /** @var Meta $object */
        return $object->getData();
    }

    public function supportsNormalization($data, string $format = null): bool
    {
        return $data instanceof Meta;
    }
}
