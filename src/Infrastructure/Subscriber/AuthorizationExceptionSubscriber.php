<?php

namespace Dvlpm\Stream\Infrastructure\Subscriber;

use Dvlpm\Stream\Application\Message\Message;
use Dvlpm\Stream\Application\Message\MessageBag;
use Dvlpm\Stream\Domain\Common\Exception\AccessExceptionInterface;
use Dvlpm\Stream\Infrastructure\Subscriber\Converter\MessageBagToJsonResponseConverter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class AuthorizationExceptionSubscriber implements EventSubscriberInterface
{
    private MessageBagToJsonResponseConverter $messageBagToResponseConverter;

    public function __construct(MessageBagToJsonResponseConverter $messageBagToResponseConverter)
    {
        $this->messageBagToResponseConverter = $messageBagToResponseConverter;
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        if (!(
           $exception instanceof AccessExceptionInterface
        )) {
            return;
        }

        $event->setResponse($this->messageBagToResponseConverter->convert(
            MessageBag::withMessages(
                Message::warning($exception->getMessage())
            ),
            Response::HTTP_FORBIDDEN
        ));

        $event->stopPropagation();
    }

    public static function getSubscribedEvents(): iterable
    {
        return [
            KernelEvents::EXCEPTION => ['onKernelException', 220],
        ];
    }
}
