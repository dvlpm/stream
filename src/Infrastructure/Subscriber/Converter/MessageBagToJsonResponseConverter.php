<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Infrastructure\Subscriber\Converter;

use Dvlpm\Stream\Application\Message\MessageBag;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

final class MessageBagToJsonResponseConverter
{
    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function convert(
        MessageBag $messageBag,
        int $code = JsonResponse::HTTP_BAD_REQUEST
    ): JsonResponse {
        $jsonMessageBag = $this->serializer->serialize($messageBag, JsonEncoder::FORMAT);

        return new JsonResponse($jsonMessageBag, $code, [], true);
    }
}
