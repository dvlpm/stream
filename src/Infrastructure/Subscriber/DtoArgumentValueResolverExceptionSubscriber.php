<?php

namespace Dvlpm\Stream\Infrastructure\Subscriber;

use Dvlpm\Stream\Application\Message\Message;
use Dvlpm\Stream\Application\Message\MessageBag;
use Dvlpm\Stream\Infrastructure\Exception\DtoArgumentValueResolverException;
use Dvlpm\Stream\Infrastructure\Subscriber\Converter\MessageBagToJsonResponseConverter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class DtoArgumentValueResolverExceptionSubscriber implements EventSubscriberInterface
{
    private MessageBagToJsonResponseConverter $messageBagToResponseConverter;

    public function __construct(MessageBagToJsonResponseConverter $messageBagToResponseConverter)
    {
        $this->messageBagToResponseConverter = $messageBagToResponseConverter;
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        if (!$exception instanceof DtoArgumentValueResolverException) {
            return;
        }

        $warnings = [];

        if (!$exception->hasErrors()) {
            $warnings[] = Message::warning('Invalid payload');
        }

        foreach ($exception->getErrors() as $error) {
            $warnings[] = Message::violation(
                $error->getPropertyPath(),
                $error->getMessage()
            );
        }

        $event->setResponse($this->messageBagToResponseConverter->convert(
            MessageBag::withMessages(...$warnings)
        ));

        $event->stopPropagation();
    }

    public static function getSubscribedEvents(): iterable
    {
        return [
            KernelEvents::EXCEPTION => ['onKernelException', 200],
        ];
    }
}
