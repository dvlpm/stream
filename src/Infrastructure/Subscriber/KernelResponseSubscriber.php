<?php

namespace Dvlpm\Stream\Infrastructure\Subscriber;

use Dvlpm\Stream\Infrastructure\Authentication\UserResolver\UserResolverInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class KernelResponseSubscriber implements EventSubscriberInterface
{
    private const USER_ID_HEADER = 'User-Id';

    private UserResolverInterface $userResolver;

    public function __construct(UserResolverInterface $userResolver)
    {
        $this->userResolver = $userResolver;
    }

    public function onKernelResponse(ResponseEvent $event): void
    {
        $response = $event->getResponse();

        $user = $this->userResolver->resolve($event->getRequest());

        $response->headers->set(self::USER_ID_HEADER, $user->getId());
    }

    public static function getSubscribedEvents(): iterable
    {
        return [
            KernelEvents::RESPONSE => 'onKernelResponse',
        ];
    }
}
