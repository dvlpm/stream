<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\Content;

use Codeception\Example;
use Dvlpm\Stream\Domain\Content\Model\AbstractContent;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Builder\UserBuilder;
use Dvlpm\Stream\Tests\Support\Generator\CreatePlanningPokerDataGenerator;
use Dvlpm\Stream\Tests\Support\Generator\CreateYouTubeStreamDataGenerator;
use Dvlpm\Stream\Tests\Support\Stub\AuthenticationStub;
use Dvlpm\Stream\Tests\Support\Stub\ContentStub;
use Dvlpm\Stream\Tests\Support\Stub\PlanningPokerStub;
use Dvlpm\Stream\Tests\Support\Stub\YouTubeStreamStub;
use Symfony\Component\HttpFoundation\Response;

final class CreateContentCest
{
    /**
     * @dataProvider provideTestCreateData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testCreate(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([]);
        $I->wantToTest($example['test']);
        $I->amUser($example['user']);
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendCreateContentApiRequest($example['request']);
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs($example['code']);
        isset($example['assert']) && $example['assert']($I);
    }

    private function provideTestCreateData(): iterable
    {
        yield [
            'test' => 'Create planning poker',
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'request' => CreatePlanningPokerDataGenerator::valid(),
            'response' => [
                'cards' => [
                    PlanningPokerStub::CARD_HALF,
                    PlanningPokerStub::CARD_ONE,
                    PlanningPokerStub::CARD_QUESTION,
                ],
            ],
            'code' => Response::HTTP_OK,
            'assert' => static function (AcceptanceTester $I): void {
                $I->seeInRepository(AbstractContent::class, [
                    'id' => ContentStub::PLANNING_POKER_ID,
                ]);
            },
        ];

        yield [
            'test' => 'Create youtube stream',
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'request' => CreateYouTubeStreamDataGenerator::valid(),
            'response' => [
                'link' => YouTubeStreamStub::LINK,
            ],
            'code' => Response::HTTP_OK,
            'assert' => static function (AcceptanceTester $I): void {
                $I->seeInRepository(AbstractContent::class, [
                    'id' => ContentStub::YOUTUBE_STREAM_ID,
                ]);
            },
        ];
    }
}
