<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\Content;

use Codeception\Example;
use Dvlpm\Stream\Domain\Content\Model\AbstractContent;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Builder\UserBuilder;
use Dvlpm\Stream\Tests\Support\Fixture\YouTubeStreamFixture;
use Dvlpm\Stream\Tests\Support\Stub\AuthenticationStub;
use Dvlpm\Stream\Tests\Support\Stub\ContentStub;
use Symfony\Component\HttpFoundation\Response;

final class RemoveContentCest
{
    /**
     * @dataProvider provideTestRemoveData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testRemove(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([new YouTubeStreamFixture()]);
        $I->wantToTest($example['test']);
        $I->amUser($example['user']);
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendRemoveContentApiRequest($example['id']);
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs($example['code']);
        $example['assert']($I);
    }

    private function provideTestRemoveData(): iterable
    {
        yield [
            'test' => 'Remove by random user',
            'id' => ContentStub::YOUTUBE_STREAM_ID,
            'user' => UserBuilder::create()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'response' => 'cannot be edited by user',
            'code' => Response::HTTP_FORBIDDEN,
            'assert' => static function (AcceptanceTester $I): void {
                $I->seeInRepository(AbstractContent::class, [
                    'id' => ContentStub::YOUTUBE_STREAM_ID,
                ]);
            },
        ];

        yield [
            'test' => 'Remove by owner',
            'id' => ContentStub::YOUTUBE_STREAM_ID,
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'response' => sprintf('Content %s removed', ContentStub::YOUTUBE_STREAM_ID),
            'code' => Response::HTTP_OK,
            'assert' => static function (AcceptanceTester $I): void {
                $I->dontSeeInRepository(AbstractContent::class, [
                    'id' => ContentStub::YOUTUBE_STREAM_ID,
                ]);
            },
        ];
    }
}
