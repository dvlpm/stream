<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\Content;

use Codeception\Example;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Builder\UserBuilder;
use Dvlpm\Stream\Tests\Support\Fixture\YouTubeStreamFixture;
use Dvlpm\Stream\Tests\Support\Stub\AuthenticationStub;
use Dvlpm\Stream\Tests\Support\Stub\ContentStub;
use Dvlpm\Stream\Tests\Support\Stub\YouTubeStreamStub;
use Symfony\Component\HttpFoundation\Response;

final class ShowContentCest
{
    /**
     * @dataProvider provideTestRemoveData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testRemove(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([new YouTubeStreamFixture()]);
        $I->wantToTest($example['test']);
        $I->amUser($example['user']);
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendShowContentApiRequest($example['id']);
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs($example['code']);
        isset($example['hidden']) && $I->dontSeeInApiResponse($example['hidden']);
    }

    private function provideTestRemoveData(): iterable
    {
        yield [
            'test' => 'Show for random user',
            'id' => ContentStub::YOUTUBE_STREAM_ID,
            'user' => UserBuilder::create()->build(),
            'response' => [
                'id' => ContentStub::YOUTUBE_STREAM_ID,
                'link' => YouTubeStreamStub::LINK
                ,
            ],
            'code' => Response::HTTP_OK,
        ];

        yield [
            'test' => 'Show for owner',
            'id' => ContentStub::YOUTUBE_STREAM_ID,
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'response' => [
                'id' => ContentStub::YOUTUBE_STREAM_ID,
                'link' => YouTubeStreamStub::LINK,
            ],
            'code' => Response::HTTP_OK,
        ];
    }
}
