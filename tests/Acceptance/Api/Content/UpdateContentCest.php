<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\Content;

use Codeception\Example;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Builder\UserBuilder;
use Dvlpm\Stream\Tests\Support\Fixture\PlanningPokerFixture;
use Dvlpm\Stream\Tests\Support\Fixture\YouTubeStreamFixture;
use Dvlpm\Stream\Tests\Support\Generator\UpdatePlanningPokerDataGenerator;
use Dvlpm\Stream\Tests\Support\Generator\UpdateYouTubeStreamDataGenerator;
use Dvlpm\Stream\Tests\Support\Stub\AuthenticationStub;
use Dvlpm\Stream\Tests\Support\Stub\ContentStub;
use Dvlpm\Stream\Tests\Support\Stub\PlanningPokerStub;
use Symfony\Component\HttpFoundation\Response;

final class UpdateContentCest
{
    /**
     * @dataProvider provideTestUpdateData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testUpdate(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([new YouTubeStreamFixture(), new PlanningPokerFixture()]);
        $I->wantToTest($example['test']);
        $I->amUser($example['user']);
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendUpdateContentApiRequest($example['id'], $example['request']);
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs($example['code']);
    }

    private function provideTestUpdateData(): iterable
    {
        yield [
            'test' => 'Update youtube stream',
            'id' => ContentStub::YOUTUBE_STREAM_ID,
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'request' => UpdateYouTubeStreamDataGenerator::valid([
                'link' => 'https://youtube.come/updated',
            ]),
            'response' => [
                'link' => 'https://youtube.come/updated',
            ],
            'code' => Response::HTTP_OK,
        ];

        yield [
            'test' => 'Update planning poker',
            'id' => ContentStub::PLANNING_POKER_ID,
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'request' => UpdatePlanningPokerDataGenerator::valid(),
            'response' => [
                'cards' => PlanningPokerStub::CARDS_FOR_UPDATE,
            ],
            'code' => Response::HTTP_OK,
        ];
    }
}
