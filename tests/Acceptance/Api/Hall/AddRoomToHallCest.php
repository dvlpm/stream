<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\Hall;

use Codeception\Example;
use Dvlpm\Stream\Domain\Space\Model\Room;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Builder\UserBuilder;
use Dvlpm\Stream\Tests\Support\Fixture\HallFixture;
use Dvlpm\Stream\Tests\Support\Generator\CreateRoomDataGenerator;
use Dvlpm\Stream\Tests\Support\Generator\MessageDataGenerator;
use Dvlpm\Stream\Tests\Support\Stub\AuthenticationStub;
use Dvlpm\Stream\Tests\Support\Stub\HallStub;
use Dvlpm\Stream\Tests\Support\Stub\RoomStub;
use Symfony\Component\HttpFoundation\Response;

final class AddRoomToHallCest
{
    /**
     * @dataProvider provideTestAddRoomData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testAddRoom(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([new HallFixture()]);
        $I->wantToTest($example['test']);
        $I->amUser($example['user']);
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendAddRoomToHallApiRequest($example['identifier'], $example['request']);
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs($example['code']);

        isset($example['assert']) && $example['assert']($I);
    }

    private function provideTestAddRoomData(): iterable
    {
        yield [
            'test' => 'Send valid',
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
            'request' => CreateRoomDataGenerator::valid([
                'name' => RoomStub::NAME_FOR_CREATE,
                'id' => RoomStub::ID_FOR_CREATE,
            ]),
            'response' => [
                'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
                'rooms' => [
                    'id' => RoomStub::ID_FOR_CREATE,
                ],
            ],
            'assert' => static function (AcceptanceTester $I) {
                $I->seeInRepository(Room::class, [
                    'id' => RoomStub::ID_FOR_CREATE,
                ]);
            },
            'code' => Response::HTTP_OK,
        ];

        yield [
            'test' => 'Valid but user not registered',
            'request' => CreateRoomDataGenerator::valid(),
            'user' => UserBuilder::create()->build(),
            'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
            'response' => [
                MessageDataGenerator::warning('User required!'),
            ],
            'code' => Response::HTTP_FORBIDDEN,
        ];
    }

    /**
     * @dataProvider provideTestValidationData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testValidation(AcceptanceTester $I, Example $example): void
    {
        $I->wantToTest($example['test']);
        $I->amUser(UserBuilder::create()->asRegistered()->build());
        $I->sendAddRoomToHallApiRequest($example['identifier'], $example['request']);
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs(Response::HTTP_BAD_REQUEST);
    }

    private function provideTestValidationData(): iterable
    {
        yield [
            'test' => 'Send empty',
            'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
            'request' => [],
            'response' => [
                MessageDataGenerator::violation('name', 'This value should not be blank.'),
                MessageDataGenerator::violation('accessType', 'This value should not be blank.'),
            ],
        ];
    }
}
