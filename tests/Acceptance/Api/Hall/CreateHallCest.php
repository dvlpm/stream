<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\Hall;

use Codeception\Example;
use Dvlpm\Stream\Domain\Space\Model\Hall;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Builder\UserBuilder;
use Dvlpm\Stream\Tests\Support\Generator\CreateHallDataGenerator;
use Dvlpm\Stream\Tests\Support\Generator\MessageDataGenerator;
use Dvlpm\Stream\Tests\Support\Stub\AuthenticationStub;
use Dvlpm\Stream\Tests\Support\Stub\HallStub;
use Symfony\Component\HttpFoundation\Response;

final class CreateHallCest
{
    /**
     * @dataProvider provideTestCreateData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testCreate(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([]);
        $I->wantToTest($example['test']);
        $I->amUser(UserBuilder::create()->asRegistered()->build());
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendCreateHallApiRequest($example['request']);
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs($example['code']);

        isset($example['assert']) && $example['assert']($I);
    }

    private function provideTestCreateData(): iterable
    {
        yield [
            'test' => 'Send valid',
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'request' => CreateHallDataGenerator::valid(),
            'response' => [
                'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
            ],
            'assert' => static function (AcceptanceTester $I) {
                $I->seeInRepository(Hall::class, [
                    'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
                ]);
            },
            'code' => Response::HTTP_OK,
        ];

        yield [
            'test' => 'Valid but user not registered',
            'request' => CreateHallDataGenerator::valid(),
            'response' => [
                MessageDataGenerator::warning('User required!'),
            ],
            'code' => Response::HTTP_FORBIDDEN,
        ];
    }

    /**
     * @dataProvider provideTestValidationData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testValidation(AcceptanceTester $I, Example $example): void
    {
        $I->wantToTest($example['test']);
        $I->amUser(UserBuilder::create()->asRegistered()->build());
        $I->sendCreateHallApiRequest($example['request']);
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs(Response::HTTP_BAD_REQUEST);
    }

    private function provideTestValidationData(): iterable
    {
        yield [
            'test' => 'Send empty',
            'request' => [],
            'response' => [
                MessageDataGenerator::violation('identifier', 'This value should not be blank.'),
            ],
        ];

        yield [
            'test' => 'Send not enough symbols in identifier',
            'request' => [
                'identifier' => '1',
            ],
            'response' => [
                MessageDataGenerator::violation(
                    'identifier',
                    'This value is too short. It should have 6 characters or more.'
                ),
            ],
        ];

        yield [
            'test' => 'Send invalid identifier',
            'request' => [
                'identifier' => 'invalid identifier',
            ],
            'response' => [
                MessageDataGenerator::violation(
                    'identifier',
                    'This value is not valid.'
                ),
            ],
        ];
    }
}
