<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\Hall;

use Codeception\Example;
use Dvlpm\Stream\Domain\Space\Model\HostRole;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Fixture\HallFixture;
use Dvlpm\Stream\Tests\Support\Stub\HallStub;
use Dvlpm\Stream\Tests\Support\Stub\UserStub;
use Symfony\Component\HttpFoundation\Response;

final class ListHallsCest
{
    /**
     * @dataProvider provideTestListData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testList(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([new HallFixture()]);
        $I->wantToTest($example['test']);
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendListHallsApiRequest($example['hostUserId'] ?? null, $example['hostRoles'] ?? []);
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs(Response::HTTP_OK);
        (isset($example['hidden'])) && $I->dontSeeInApiResponse($example['hidden']);
    }

    private function provideTestListData(): iterable
    {
        yield [
            'test' => 'Listed with no params',
            'response' => [
                [
                    'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
                ],
                [
                    'identifier' => HallStub::ADMINISTRATED_BY_REGISTERED_USER_IDENTIFIER,
                ],
                [
                    'identifier' => HallStub::OWNED_BY_SECOND_REGISTERED_USER_IDENTIFIER,
                ],
            ],
        ];

        yield [
            'test' => 'Listed by user id and owner role',
            'hostUserId' => UserStub::REGISTERED_USER_ID,
            'hostRoles' => [
                HostRole::OWNER(),
            ],
            'response' => [
                'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
            ],
            'hidden' => [
                [
                    'identifier' => HallStub::ADMINISTRATED_BY_REGISTERED_USER_IDENTIFIER,
                ],
                [
                    'identifier' => HallStub::OWNED_BY_SECOND_REGISTERED_USER_IDENTIFIER,
                ],
            ],
        ];

        yield [
            'test' => 'Listed by user id and owner and administrator roles',
            'hostUserId' => UserStub::REGISTERED_USER_ID,
            'hostRoles' => [
                HostRole::OWNER(),
                HostRole::ADMINISTRATOR(),
            ],
            'response' => [
                [
                    'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
                ],
                [
                    'identifier' => HallStub::ADMINISTRATED_BY_REGISTERED_USER_IDENTIFIER,
                ],
            ],
            'hidden' => [
                [
                    'identifier' => HallStub::OWNED_BY_SECOND_REGISTERED_USER_IDENTIFIER,
                ],
            ],
        ];

        yield [
            'test' => 'Listed by user id and owner and administrator roles',
            'hostUserId' => UserStub::REGISTERED_WITH_EMAIL_USER_ID,
            'hostRoles' => [
                HostRole::OWNER(),
                HostRole::ADMINISTRATOR(),
            ],
            'response' => [
                [
                    'identifier' => HallStub::OWNED_BY_SECOND_REGISTERED_USER_IDENTIFIER,
                ],
            ],
            'hidden' => [
                [
                    'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
                ],
                [
                    'identifier' => HallStub::ADMINISTRATED_BY_REGISTERED_USER_IDENTIFIER,
                ],
            ],
        ];
    }
}
