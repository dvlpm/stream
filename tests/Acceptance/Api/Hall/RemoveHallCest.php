<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\Hall;

use Codeception\Example;
use Dvlpm\Stream\Domain\Space\Model\Hall;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Builder\UserBuilder;
use Dvlpm\Stream\Tests\Support\Fixture\HallFixture;
use Dvlpm\Stream\Tests\Support\Stub\AuthenticationStub;
use Dvlpm\Stream\Tests\Support\Stub\HallStub;
use Symfony\Component\HttpFoundation\Response;

final class RemoveHallCest
{
    /**
     * @dataProvider provideTestRemoveData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testRemove(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([new HallFixture()]);
        $I->wantToTest($example['test']);
        $I->amUser($example['user']);
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendRemoveHallApiRequest($example['identifier']);
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs($example['code']);
        $example['assert']($I);
    }

    private function provideTestRemoveData(): iterable
    {
        yield [
            'test' => 'Remove by random user',
            'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
            'user' => UserBuilder::create()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'response' => 'cannot be edited by user',
            'code' => Response::HTTP_FORBIDDEN,
            'assert' => static function (AcceptanceTester $I): void {
                $I->seeInRepository(Hall::class, [
                    'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
                ]);
            },
        ];

        yield [
            'test' => 'Remove by owner',
            'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'response' => sprintf('Hall %s removed', HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER),
            'code' => Response::HTTP_OK,
            'assert' => static function (AcceptanceTester $I): void {
                $I->dontSeeInRepository(Hall::class, [
                    'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
                ]);
            },
        ];
    }
}
