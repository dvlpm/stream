<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\Hall;

use Codeception\Example;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Builder\UserBuilder;
use Dvlpm\Stream\Tests\Support\Fixture\HallFixture;
use Dvlpm\Stream\Tests\Support\Stub\AuthenticationStub;
use Dvlpm\Stream\Tests\Support\Stub\ContentStub;
use Dvlpm\Stream\Tests\Support\Stub\HallStub;
use Dvlpm\Stream\Tests\Support\Stub\RoomStub;
use Dvlpm\Stream\Tests\Support\Stub\UserStub;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

final class ShowHallCest
{
    /**
     * @dataProvider provideTestShowData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testShow(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([new HallFixture()]);
        $I->wantToTest($example['test']);
        $I->amUser($example['user']);
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendShowHallApiRequest($example['identifier']);
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs(Response::HTTP_OK);
        (isset($example['hidden'])) && $I->dontSeeInApiResponse($example['hidden']);
    }

    private function provideTestShowData(): iterable
    {
        yield [
            'test' => 'Viewed by not listed guest',
            'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
            'user' => UserBuilder::create()->build(),
            'response' => [
                'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
                'rooms' => [
                    [
                        'name' => RoomStub::ANONYMOUS_NAME,
                    ],
                ],
            ],
            'hidden' => [
                'rooms' => [
                    [
                        'name' => RoomStub::AUTHORIZED_NAME,
                    ],
                    [
                        'name' => RoomStub::LISTED_NAME,
                    ],
                ],
            ],
        ];

        yield [
            'test' => 'Viewed by not listed registered user',
            'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
            'user' => UserBuilder::create()
                ->asRegistered()
                ->withId(Uuid::fromString(UserStub::FOURTH_REGISTERED_USER_ID))
                ->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'response' => [
                'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
                'rooms' => [
                    [
                        'name' => RoomStub::AUTHORIZED_NAME,
                    ],
                    [
                        'name' => RoomStub::ANONYMOUS_NAME,
                    ],
                ],
            ],
            'hidden' => [
                'rooms' => [
                    [
                        'name' => RoomStub::LISTED_NAME,
                    ],
                ],
            ],
        ];

        yield [
            'test' => 'Viewed by listed guest',
            'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
            'user' => UserBuilder::create()->asGuest()->build(),
            'response' => [
                'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
                'rooms' => [
                    [
                        'name' => RoomStub::ANONYMOUS_NAME,
                    ],
                    [
                        'name' => RoomStub::LISTED_NAME,
                    ],
                ],
            ],
            'hidden' => [
                'rooms' => [
                    [
                        'name' => RoomStub::AUTHORIZED_NAME,
                    ],
                ],
            ],
        ];

        yield [
            'test' => 'Viewed by owner',
            'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'response' => [
                'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
                'rooms' => [
                    [
                        'name' => RoomStub::ANONYMOUS_NAME,
                    ],
                    [
                        'name' => RoomStub::AUTHORIZED_NAME,
                    ],
                    [
                        'name' => RoomStub::LISTED_NAME,
                        'contents' => [
                            [
                                'id' => ContentStub::PLANNING_POKER_WITH_STORIES_ID,
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}
