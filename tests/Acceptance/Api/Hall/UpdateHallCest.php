<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\Hall;

use Codeception\Example;
use Dvlpm\Stream\Domain\Space\Model\Hall;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Builder\UserBuilder;
use Dvlpm\Stream\Tests\Support\Fixture\HallFixture;
use Dvlpm\Stream\Tests\Support\Generator\UpdateHallDataGenerator;
use Dvlpm\Stream\Tests\Support\Stub\AuthenticationStub;
use Dvlpm\Stream\Tests\Support\Stub\HallStub;
use Symfony\Component\HttpFoundation\Response;

final class UpdateHallCest
{
    /**
     * @dataProvider provideTestUpdateData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testUpdate(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([new HallFixture()]);
        $I->wantToTest($example['test']);
        $I->amUser(UserBuilder::create()->asRegistered()->build());
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendUpdateHallApiRequest($example['identifier'], $example['request']);
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs($example['code']);

        $example['assert']($I);
    }

    private function provideTestUpdateData(): iterable
    {
        yield [
            'test' => 'Send valid',
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'request' => UpdateHallDataGenerator::valid(),
            'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
            'response' => [
                'identifier' => HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER,
                'name' => HallStub::NAME_FOR_UPDATE,
            ],
            'assert' => static function (AcceptanceTester $I) {
                $I->seeInRepository(Hall::class, [
                    'name' => HallStub::NAME_FOR_UPDATE,
                ]);
            },
            'code' => Response::HTTP_OK,
        ];
    }
}
