<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\PlanningPoker;

use Codeception\Example;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerPlayer;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Builder\UserBuilder;
use Dvlpm\Stream\Tests\Support\Fixture\PlanningPokerFixture;
use Dvlpm\Stream\Tests\Support\Generator\CreatePlanningPokerPlayerDataGenerator;
use Dvlpm\Stream\Tests\Support\Stub\AuthenticationStub;
use Dvlpm\Stream\Tests\Support\Stub\ContentStub;
use Dvlpm\Stream\Tests\Support\Stub\PlanningPokerPlayerStub;
use Symfony\Component\HttpFoundation\Response;

final class AddPlanningPokerPlayerToPlanningPokerCest
{
    /**
     * @dataProvider provideTestAddStoryData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testAddStory(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([new PlanningPokerFixture()]);
        $I->wantToTest($example['test']);
        $I->amUser($example['user']);
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendAddPlanningPokerPlayerToPlanningPokerApiRequest($example['id'], $example['request']);
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs($example['code']);

        isset($example['assert']) && $example['assert']($I);
    }

    private function provideTestAddStoryData(): iterable
    {
        yield [
            'test' => 'Send valid',
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'id' => ContentStub::PLANNING_POKER_ID,
            'request' => CreatePlanningPokerPlayerDataGenerator::valid(),
            'response' => [
                'id' => ContentStub::PLANNING_POKER_ID,
            ],
            'assert' => static function (AcceptanceTester $I) {
                $I->seeInRepository(PlanningPokerPlayer::class, [
                    'id' => PlanningPokerPlayerStub::ID_FOR_CREATE,
                ]);
            },
            'code' => Response::HTTP_OK,
        ];
    }
}
