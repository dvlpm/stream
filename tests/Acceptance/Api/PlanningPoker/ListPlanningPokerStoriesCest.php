<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\PlanningPoker;

use Codeception\Example;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Builder\UserBuilder;
use Dvlpm\Stream\Tests\Support\Fixture\PlanningPokerFixture;
use Dvlpm\Stream\Tests\Support\Stub\AuthenticationStub;
use Dvlpm\Stream\Tests\Support\Stub\ContentStub;
use Dvlpm\Stream\Tests\Support\Stub\PlanningPokerStoryStub;
use Dvlpm\Stream\Tests\Support\Stub\PlanningPokerStoryVoteStub;
use Dvlpm\Stream\Tests\Support\Stub\UserStub;
use Symfony\Component\HttpFoundation\Response;

final class ListPlanningPokerStoriesCest
{
    /**
     * @dataProvider provideTestListStoriesData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testListStories(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([new PlanningPokerFixture()]);
        $I->wantToTest($example['test']);
        $I->amUser($example['user']);
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendListPlanningPokerStoriesApiRequest(
            $example['id'],
            $example['status'] ?? null,
            $example['limit'] ?? null,
            $example['offset'] ?? null,
        );
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs($example['code']);
        foreach ($example['hiddenList'] ?? [] as $hidden) {
            $I->dontSeeInApiResponse($hidden);
        }
    }

    private function provideTestListStoriesData(): iterable
    {
        yield [
            'test' => 'With no params',
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'id' => ContentStub::PLANNING_POKER_WITH_STORIES_ID,
            'response' => [
                [
                    'id' => PlanningPokerStoryStub::ID,
                    'votes' => [
                        [
                            'user' => [
                                'id' => UserStub::REGISTERED_USER_ID,
                            ],
                            'card' => PlanningPokerStoryVoteStub::CARD,
                        ],
                    ],
                ],
                ['id' => PlanningPokerStoryStub::COMPLETED_ID],
            ],
            'code' => Response::HTTP_OK,
        ];

        yield [
            'test' => 'With params with result',
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'id' => ContentStub::PLANNING_POKER_WITH_STORIES_ID,
            'status' => PlanningPokerStoryStub::STATUS,
            'limit' => '1',
            'offset' => '0',
            'response' => [
                'id' => PlanningPokerStoryStub::ID,
            ],
            'code' => Response::HTTP_OK,
        ];

        yield [
            'test' => 'With status with result',
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'id' => ContentStub::PLANNING_POKER_WITH_STORIES_ID,
            'status' => 'COMPLETED',
            'response' => [
                'id' => PlanningPokerStoryStub::COMPLETED_ID,
            ],
            'hiddenList' => [
                'id' => PlanningPokerStoryStub::ID,
            ],
            'code' => Response::HTTP_OK,
        ];

        yield [
            'test' => 'With offset with empty result',
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'id' => ContentStub::PLANNING_POKER_WITH_STORIES_ID,
            'limit' => '1',
            'offset' => '3',
            'response' => [],
            'hiddenList' => [
                ['id' => PlanningPokerStoryStub::ID],
                ['id' => PlanningPokerStoryStub::COMPLETED_ID],
            ],
            'code' => Response::HTTP_OK,
        ];
    }
}
