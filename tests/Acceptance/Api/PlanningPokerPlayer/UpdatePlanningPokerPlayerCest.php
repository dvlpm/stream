<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\PlanningPokerPlayer;

use Codeception\Example;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Builder\UserBuilder;
use Dvlpm\Stream\Tests\Support\Fixture\PlanningPokerFixture;
use Dvlpm\Stream\Tests\Support\Generator\UpdatePlanningPokerPlayerDataGenerator;
use Dvlpm\Stream\Tests\Support\Stub\AuthenticationStub;
use Dvlpm\Stream\Tests\Support\Stub\PlanningPokerPlayerStub;
use Symfony\Component\HttpFoundation\Response;

final class UpdatePlanningPokerPlayerCest
{
    /**
     * @dataProvider provideTestUpdateData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testUpdate(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([new PlanningPokerFixture()]);
        $I->wantToTest($example['test']);
        $I->amUser($example['user']);
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendUpdatePlanningPokerPlayerApiRequest($example['id'], $example['request']);
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs($example['code']);
    }

    private function provideTestUpdateData(): iterable
    {
        yield [
            'test' => 'Update',
            'id' => PlanningPokerPlayerStub::ID,
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'request' => UpdatePlanningPokerPlayerDataGenerator::valid(),
            'response' => UpdatePlanningPokerPlayerDataGenerator::valid(),
            'code' => Response::HTTP_OK,
        ];
    }
}
