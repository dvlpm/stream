<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\PlanningPokerStory;

use Codeception\Example;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Builder\UserBuilder;
use Dvlpm\Stream\Tests\Support\Fixture\PlanningPokerFixture;
use Dvlpm\Stream\Tests\Support\Generator\CreatePlanningPokerStoryVoteDataGenerator;
use Dvlpm\Stream\Tests\Support\Stub\AuthenticationStub;
use Dvlpm\Stream\Tests\Support\Stub\PlanningPokerStoryStub;
use Dvlpm\Stream\Tests\Support\Stub\PlanningPokerStoryVoteStub;
use Symfony\Component\HttpFoundation\Response;

final class AddPlanningPokerStoryVoteToPlanningPokerStoryCest
{
    /**
     * @dataProvider provideTestAddVoteData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testAddVote(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([new PlanningPokerFixture()]);
        $I->wantToTest($example['test']);
        $I->amUser($example['user']);
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendAddPlanningPokerStoryVoteToPlanningPokerStoryApiRequest($example['id'], $example['request']);
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs($example['code']);
    }

    private function provideTestAddVoteData(): iterable
    {
        yield [
            'test' => 'Add vote by not listed guest',
            'id' => PlanningPokerStoryStub::ID,
            'user' => UserBuilder::create()->asGuest()->build(),
            'request' => CreatePlanningPokerStoryVoteDataGenerator::valid(),
            'response' => 'cannot be voted by user',
            'code' => Response::HTTP_FORBIDDEN,
        ];

        yield [
            'test' => 'Update vote by owner',
            'id' => PlanningPokerStoryStub::ID,
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'request' => CreatePlanningPokerStoryVoteDataGenerator::valid(),
            'response' => [
                'votes' => [
                    'card' => PlanningPokerStoryVoteStub::CARD_FOR_UPDATE,
                ],
            ],
            'code' => Response::HTTP_OK,
        ];
    }
}
