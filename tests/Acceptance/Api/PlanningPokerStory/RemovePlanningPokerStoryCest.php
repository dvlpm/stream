<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\PlanningPokerStory;

use Codeception\Example;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerStory;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Builder\UserBuilder;
use Dvlpm\Stream\Tests\Support\Fixture\PlanningPokerFixture;
use Dvlpm\Stream\Tests\Support\Stub\AuthenticationStub;
use Dvlpm\Stream\Tests\Support\Stub\PlanningPokerStoryStub;
use Symfony\Component\HttpFoundation\Response;

final class RemovePlanningPokerStoryCest
{
    /**
     * @dataProvider provideTestRemoveData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testRemove(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([new PlanningPokerFixture()]);
        $I->wantToTest($example['test']);
        $I->amUser($example['user']);
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendRemovePlanningPokerStoryApiRequest($example['id']);
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs($example['code']);
        $example['assert']($I);
    }

    private function provideTestRemoveData(): iterable
    {
        yield [
            'test' => 'Remove by owner',
            'id' => PlanningPokerStoryStub::ID,
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'response' => sprintf('Story %s removed', PlanningPokerStoryStub::ID),
            'code' => Response::HTTP_OK,
            'assert' => static function (AcceptanceTester $I): void {
                $I->dontSeeInRepository(PlanningPokerStory::class, [
                    'id' => PlanningPokerStoryStub::ID,
                ]);
            },
        ];

        yield [
            'test' => 'Remove by random user',
            'id' => PlanningPokerStoryStub::ID,
            'user' => UserBuilder::create()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'response' => 'be edited by user',
            'code' => Response::HTTP_FORBIDDEN,
            'assert' => static function (AcceptanceTester $I): void {
                $I->seeInRepository(PlanningPokerStory::class, [
                    'id' => PlanningPokerStoryStub::ID,
                ]);
            },
        ];
    }
}
