<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\PlanningPokerStory;

use Codeception\Example;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerStoryVote;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Builder\UserBuilder;
use Dvlpm\Stream\Tests\Support\Fixture\PlanningPokerFixture;
use Dvlpm\Stream\Tests\Support\Stub\AuthenticationStub;
use Dvlpm\Stream\Tests\Support\Stub\PlanningPokerStoryStub;
use Symfony\Component\HttpFoundation\Response;

final class RemovePlanningPokerStoryVotesCest
{
    /**
     * @dataProvider provideTestRemoveVotesData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testRemoveVotes(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([new PlanningPokerFixture()]);
        $I->wantToTest($example['test']);
        $I->amUser($example['user']);
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendRemovePlanningPokerStoryVotesApiRequest($example['id']);
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs($example['code']);
        $example['assert']($I);
    }

    private function provideTestRemoveVotesData(): iterable
    {
        yield [
            'test' => 'Remove by owner',
            'id' => PlanningPokerStoryStub::ID,
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'response' => [
                'votes' => [],
            ],
            'code' => Response::HTTP_OK,
            'assert' => static function (AcceptanceTester $I): void {
                $I->dontSeeInRepository(PlanningPokerStoryVote::class, [
                    'story' => [
                        'id' => PlanningPokerStoryStub::ID,
                    ],
                ]);
            },
        ];
    }
}
