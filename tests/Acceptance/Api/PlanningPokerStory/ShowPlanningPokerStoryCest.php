<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\PlanningPokerStory;

use Codeception\Example;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Builder\UserBuilder;
use Dvlpm\Stream\Tests\Support\Fixture\HallFixture;
use Dvlpm\Stream\Tests\Support\Stub\AuthenticationStub;
use Dvlpm\Stream\Tests\Support\Stub\PlanningPokerStoryStub;
use Dvlpm\Stream\Tests\Support\Stub\PlanningPokerStoryVoteStub;
use Dvlpm\Stream\Tests\Support\Stub\UserStub;
use Symfony\Component\HttpFoundation\Response;

final class ShowPlanningPokerStoryCest
{
    /**
     * @dataProvider provideTestUpdateData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testUpdate(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([new HallFixture()]);
        $I->wantToTest($example['test']);
        $I->amUser($example['user']);
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendShowPlanningPokerStoryApiRequest($example['id']);
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs($example['code']);
        (isset($example['hidden'])) && $I->dontSeeInApiResponse($example['hidden']);
    }

    private function provideTestUpdateData(): iterable
    {
        yield [
            'test' => 'Viewed by listed with user id identification guest',
            'user' => UserBuilder::create()->asGuest()->build(),
            'id' => PlanningPokerStoryStub::ID,
            'response' => [
                'id' => PlanningPokerStoryStub::ID,
                'name' => PlanningPokerStoryStub::NAME,
                'link' => PlanningPokerStoryStub::EXTRENAL_LINK,
                'votes' => [
                    [
                        'user' => [
                            'id' => UserStub::REGISTERED_USER_ID,
                        ],
                    ],
                ],
            ],
            'hidden' => [
                'card' => PlanningPokerStoryVoteStub::CARD,
            ],
            'code' => Response::HTTP_OK,
        ];

        yield [
            'test' => 'Viewed by owner',
            'id' => PlanningPokerStoryStub::ID,
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'response' => [
                'id' => PlanningPokerStoryStub::ID,
                'name' => PlanningPokerStoryStub::NAME,
                'link' => PlanningPokerStoryStub::EXTRENAL_LINK,
                'votes' => [
                    [
                        'user' => [
                            'id' => UserStub::REGISTERED_USER_ID,
                        ],
                        'card' => PlanningPokerStoryVoteStub::CARD,
                    ],
                ],
            ],
            'code' => Response::HTTP_OK,
        ];
    }
}
