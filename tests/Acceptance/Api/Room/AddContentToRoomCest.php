<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\Room;

use Codeception\Example;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Builder\UserBuilder;
use Dvlpm\Stream\Tests\Support\Fixture\YouTubeStreamFixture;
use Dvlpm\Stream\Tests\Support\Fixture\HallFixture;
use Dvlpm\Stream\Tests\Support\Stub\AuthenticationStub;
use Dvlpm\Stream\Tests\Support\Stub\ContentStub;
use Dvlpm\Stream\Tests\Support\Stub\RoomStub;
use Dvlpm\Stream\Tests\Support\Stub\YouTubeStreamStub;
use Symfony\Component\HttpFoundation\Response;

final class AddContentToRoomCest
{
    /**
     * @dataProvider provideTestAddContentData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testAddContent(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([new HallFixture(), new YouTubeStreamFixture()]);
        $I->wantToTest($example['test']);
        $I->amUser($example['user']);
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendAddContentToRoomApiRequest($example['id'], $example['request']);
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs($example['code']);
    }

    private function provideTestAddContentData(): iterable
    {
        yield [
            'test' => 'Add content by owner',
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'id' => RoomStub::ANONYMOUS_ID,
            'request' => [
                'id' => ContentStub::ANOTHER_YOUTUBE_STREAM_ID,
            ],
            'response' => [
                'link' => YouTubeStreamStub::ANOTHER_LINK,
            ],
            'code' => Response::HTTP_OK,
        ];
    }
}
