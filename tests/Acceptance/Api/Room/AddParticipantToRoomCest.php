<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\Room;

use Codeception\Example;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Builder\UserBuilder;
use Dvlpm\Stream\Tests\Support\Fixture\HallFixture;
use Dvlpm\Stream\Tests\Support\Generator\CreateParticipantDataGenerator;
use Dvlpm\Stream\Tests\Support\Stub\AuthenticationStub;
use Dvlpm\Stream\Tests\Support\Stub\RoomStub;
use Dvlpm\Stream\Tests\Support\Stub\UserStub;
use Symfony\Component\HttpFoundation\Response;

final class AddParticipantToRoomCest
{
    /**
     * @dataProvider provideTestAddParticipantData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testAddParticipant(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([new HallFixture()]);
        $I->wantToTest($example['test']);
        $I->amUser($example['user']);
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendAddParticipantToRoomApiRequest($example['id'], $example['request']);
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs($example['code']);
    }

    private function provideTestAddParticipantData(): iterable
    {
        yield [
            'test' => 'Update by owner',
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'id' => RoomStub::ANONYMOUS_ID,
            'request' => CreateParticipantDataGenerator::valid(),
            'response' => [
                'userId' => UserStub::REGISTERED_WITH_EMAIL_USER_ID,
            ],
            'code' => Response::HTTP_OK,
        ];
    }
}
