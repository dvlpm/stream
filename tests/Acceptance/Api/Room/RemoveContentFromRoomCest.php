<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\Room;

use Codeception\Example;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Builder\UserBuilder;
use Dvlpm\Stream\Tests\Support\Fixture\HallFixture;
use Dvlpm\Stream\Tests\Support\Stub\AuthenticationStub;
use Dvlpm\Stream\Tests\Support\Stub\ContentStub;
use Dvlpm\Stream\Tests\Support\Stub\RoomStub;
use Dvlpm\Stream\Tests\Support\Stub\YouTubeStreamStub;
use Symfony\Component\HttpFoundation\Response;

final class RemoveContentFromRoomCest
{
    /**
     * @dataProvider provideTestRemoveContentData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testRemoveContent(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([new HallFixture()]);
        $I->wantToTest($example['test']);
        $I->amUser($example['user']);
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendRemoveContentFromRoomApiRequest($example['id'], $example['contentId']);
        $I->seeInApiResponse($example['response']);
        $I->dontSeeInApiResponse($example['hidden']);
        $I->seeApiResponseCodeIs($example['code']);
    }

    private function provideTestRemoveContentData(): iterable
    {
        yield [
            'test' => 'Update by owner',
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'id' => RoomStub::AUTHORIZED_ID,
            'contentId' => ContentStub::YOUTUBE_STREAM_ID,
            'response' => [
                'name' => RoomStub::AUTHORIZED_NAME,
            ],
            'hidden' => [
                'link' => YouTubeStreamStub::LINK,
            ],
            'code' => Response::HTTP_OK,
        ];
    }
}
