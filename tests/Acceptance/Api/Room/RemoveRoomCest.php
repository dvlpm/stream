<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\Room;

use Codeception\Example;
use Dvlpm\Stream\Domain\Space\Model\Room;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Builder\UserBuilder;
use Dvlpm\Stream\Tests\Support\Fixture\HallFixture;
use Dvlpm\Stream\Tests\Support\Stub\AuthenticationStub;
use Dvlpm\Stream\Tests\Support\Stub\RoomStub;
use Symfony\Component\HttpFoundation\Response;

final class RemoveRoomCest
{
    /**
     * @dataProvider provideTestRemoveData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testRemove(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([new HallFixture()]);
        $I->wantToTest($example['test']);
        $I->amUser($example['user']);
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendRemoveRoomApiRequest($example['id']);
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs($example['code']);
        $example['assert']($I);
    }

    private function provideTestRemoveData(): iterable
    {
        yield [
            'test' => 'Remove by owner',
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'id' => RoomStub::ANONYMOUS_ID,
            'response' => sprintf('Room %s removed.', RoomStub::ANONYMOUS_ID),
            'code' => Response::HTTP_OK,
            'assert' => static function (AcceptanceTester $I): void {
                $I->dontSeeInRepository(Room::class, [
                    'id' => RoomStub::ANONYMOUS_ID
                ]);
            }
        ];

        yield [
            'test' => 'Remove by random user',
            'user' => UserBuilder::create()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'id' => RoomStub::ANONYMOUS_ID,
            'response' => 'cannot be edited by user',
            'code' => Response::HTTP_FORBIDDEN,
            'assert' => static function (AcceptanceTester $I): void {
                $I->seeInRepository(Room::class, [
                    'id' => RoomStub::ANONYMOUS_ID
                ]);
            }
        ];
    }
}
