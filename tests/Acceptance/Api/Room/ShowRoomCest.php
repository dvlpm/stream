<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\Room;

use Codeception\Example;
use Dvlpm\Stream\Domain\Space\Model\Key;
use Dvlpm\Stream\Domain\Space\Model\Participant;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Builder\UserBuilder;
use Dvlpm\Stream\Tests\Support\Fixture\HallFixture;
use Dvlpm\Stream\Tests\Support\Stub\AuthenticationStub;
use Dvlpm\Stream\Tests\Support\Stub\ContentStub;
use Dvlpm\Stream\Tests\Support\Stub\PlanningPokerPlayerStub;
use Dvlpm\Stream\Tests\Support\Stub\RoomStub;
use Dvlpm\Stream\Tests\Support\Stub\UserStub;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

final class ShowRoomCest
{
    /**
     * @dataProvider provideTestShowData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testShow(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([new HallFixture()]);
        $I->wantToTest($example['test']);
        $I->amUser($example['user']);
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendShowRoomApiRequest($example['id'], $example['key'] ?? null);
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs($example['code']);
        (isset($example['hidden'])) && $I->dontSeeInApiResponse($example['hidden']);
        (isset($example['assert'])) && $example['assert']($I);
    }

    private function provideTestShowData(): iterable
    {
        yield [
            'test' => 'Viewed by listed with email identification guest',
            'user' => UserBuilder::create()->asRegisteredWithEmail()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'id' => RoomStub::LISTED_ID,
            'response' => [
                'name' => RoomStub::LISTED_NAME,
                'accessType' => RoomStub::LISTED_ACCESS_TYPE,
                'contents' => [
                    [
                        'id' => ContentStub::PLANNING_POKER_WITH_STORIES_ID,
                    ],
                ],
            ],
            'hidden' => [
                'key' => RoomStub::LISTED_KEY,
            ],
            'code' => Response::HTTP_OK,
            'assert' => static function (AcceptanceTester $I): void {
                $I->seeInRepository(Participant::class, [
                    'room' => [
                        'id' => RoomStub::LISTED_ID,
                    ],
                    'user' => [
                        'id' => UserStub::REGISTERED_WITH_EMAIL_USER_ID,
                    ],
                ]);
            },
        ];

        yield [
            'test' => 'Viewed by owner',
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'id' => RoomStub::LISTED_ID,
            'response' => [
                'name' => RoomStub::LISTED_NAME,
                'accessType' => RoomStub::LISTED_ACCESS_TYPE,
                'contents' => [
                    [
                        'id' => ContentStub::PLANNING_POKER_WITH_STORIES_ID,
                        'players' => [
                            'id' => PlanningPokerPlayerStub::ID,
                        ],
                    ],
                ],
                'key' => RoomStub::LISTED_KEY,
            ],
            'code' => Response::HTTP_OK,
        ];

        yield [
            'test' => 'Viewed by listed with user id identification guest',
            'user' => UserBuilder::create()->asGuest()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'id' => RoomStub::LISTED_ID,
            'response' => [
                'name' => RoomStub::LISTED_NAME,
                'accessType' => RoomStub::LISTED_ACCESS_TYPE,
                'contents' => [
                    [
                        'id' => ContentStub::PLANNING_POKER_WITH_STORIES_ID,
                    ],
                ],
            ],
            'hidden' => [
                'key' => RoomStub::LISTED_KEY,
            ],
            'code' => Response::HTTP_OK,
            'assert' => static function (AcceptanceTester $I): void {
                $I->seeInRepository(Participant::class, [
                    'room' => [
                        'id' => RoomStub::LISTED_ID,
                    ],
                    'user' => [
                        'id' => UserStub::GUEST_USER_ID,
                    ],
                ]);
            },
        ];

        yield [
            'test' => 'Random not listed registered user',
            'user' => UserBuilder::create()
                ->withId(Uuid::fromString(UserStub::FOURTH_REGISTERED_USER_ID))
                ->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'id' => RoomStub::LISTED_ID,
            'response' => 'cannot be viewed by user',
            'hidden' => [
                'name' => RoomStub::LISTED_NAME,
                'accessType' => RoomStub::LISTED_ACCESS_TYPE,
                'contents' => [
                    [
                        'id' => ContentStub::PLANNING_POKER_WITH_STORIES_ID,
                    ],
                ],
            ],
            'code' => Response::HTTP_FORBIDDEN,
            'assert' => static function (AcceptanceTester $I): void {
                $I->dontSeeInRepository(Participant::class, [
                    'room' => [
                        'id' => RoomStub::LISTED_ID,
                    ],
                    'user' => [
                        'id' => UserStub::FOURTH_REGISTERED_USER_ID,
                    ],
                ]);
            },
        ];

        yield [
            'test' => 'Random not listed registered user with key',
            'user' => UserBuilder::create()
                ->withId(Uuid::fromString(UserStub::FOURTH_REGISTERED_USER_ID))
                ->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'id' => RoomStub::LISTED_ID,
            'key' => Key::fromValue(RoomStub::LISTED_KEY),
            'response' => [
                'name' => RoomStub::LISTED_NAME,
                'accessType' => RoomStub::LISTED_ACCESS_TYPE,
                'contents' => [
                    [
                        'id' => ContentStub::PLANNING_POKER_WITH_STORIES_ID,
                    ],
                ],
            ],
            'code' => Response::HTTP_OK,
            'assert' => static function (AcceptanceTester $I): void {
                $I->seeInRepository(Participant::class, [
                    'room' => [
                        'id' => RoomStub::LISTED_ID,
                    ],
                    'user' => [
                        'id' => UserStub::FOURTH_REGISTERED_USER_ID,
                    ],
                ]);
            },
        ];
    }
}
