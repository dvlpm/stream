<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\User;

use Codeception\Example;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Fixture\UserFixture;
use Dvlpm\Stream\Tests\Support\Stub\UserStub;
use Symfony\Component\HttpFoundation\Response;

final class ListUsersCest
{
    /**
     * @dataProvider provideTestListData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testList(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([new UserFixture()]);
        $I->wantToTest($example['test']);
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendListUsersApiRequest(
            $example['searchQuery'] ?? null,
            $example['limit'] ?? null,
            $example['offset'] ?? null,
        );
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs(Response::HTTP_OK);
        (isset($example['hidden'])) && $I->dontSeeInApiResponse($example['hidden']);
    }

    private function provideTestListData(): iterable
    {
        yield [
            'test' => 'Listed with no params',
            'response' => [
                [
                    'id' => UserStub::REGISTERED_USER_ID,
                ],
                [
                    'id' => UserStub::REGISTERED_WITH_EMAIL_USER_ID,
                ],
            ],
        ];
    }
}
