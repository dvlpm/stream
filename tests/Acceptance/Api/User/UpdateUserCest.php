<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Acceptance\Api\User;

use Codeception\Example;
use Dvlpm\Stream\Domain\User\Model\User;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;
use Dvlpm\Stream\Tests\AcceptanceTester;
use Dvlpm\Stream\Tests\Support\Builder\UserBuilder;
use Dvlpm\Stream\Tests\Support\Fixture\UserFixture;
use Dvlpm\Stream\Tests\Support\Generator\UpdateUserDataGenerator;
use Dvlpm\Stream\Tests\Support\Stub\AuthenticationStub;
use Dvlpm\Stream\Tests\Support\Stub\UserStub;
use Symfony\Component\HttpFoundation\Response;

final class UpdateUserCest
{
    /**
     * @dataProvider provideTestUpdateData
     * @param AcceptanceTester $I
     * @param Example $example
     */
    public function testUpdate(AcceptanceTester $I, Example $example): void
    {
        $I->loadFixtures([new UserFixture()]);
        $I->wantToTest($example['test']);
        $I->amUser($example['user']);
        $I->haveAccessToken($example['accessToken'] ?? null);
        $I->sendUpdateUserApiRequest($example['id'], $example['request']);
        $I->seeInApiResponse($example['response']);
        $I->seeApiResponseCodeIs($example['code']);

        $example['assert']($I);
    }

    private function provideTestUpdateData(): iterable
    {
        yield [
            'test' => 'Send valid',
            'user' => UserBuilder::create()->asRegistered()->build(),
            'accessToken' => AccessToken::fromValue(AuthenticationStub::ACCESS_TOKEN),
            'request' => UpdateUserDataGenerator::valid(),
            'id' => 'me',
            'response' => [
                'id' => UserStub::REGISTERED_USER_ID,
                'name' => UserStub::NAME_FOR_UPDATE,
                'username' => UserStub::USERNAME_FOR_UPDATE,
                'meta' => [
                    'random_key' => 'random_value'
                ]
            ],
            'assert' => static function (AcceptanceTester $I) {
                $I->seeInRepository(User::class, [
                    'username' => UserStub::USERNAME_FOR_UPDATE,
                    'name' => UserStub::NAME_FOR_UPDATE,
                ]);
            },
            'code' => Response::HTTP_OK,
        ];
    }
}
