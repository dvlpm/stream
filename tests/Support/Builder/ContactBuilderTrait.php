<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Builder;

use Dvlpm\Stream\Domain\Content\Model\AbstractContent;
use Dvlpm\Stream\Domain\User\Model\AbstractContact;
use Dvlpm\Stream\Domain\User\Model\User;
use Dvlpm\Stream\Tests\Support\Util\ReflectionUtil;
use Ramsey\Uuid\UuidInterface;

trait ContactBuilderTrait
{
    private ?UuidInterface $id = null;
    private ?User $user = null;
    private bool $isPreferable = false;

    public function withId(?UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function withUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function withIsPreferable(bool $isPreferable): self
    {
        $this->isPreferable = $isPreferable;

        return $this;
    }

    protected function applyCommonPropertiesTo(AbstractContact $contact): self
    {
        ReflectionUtil::setPrivateProperty(AbstractContact::class, $contact, 'id', $this->id);
        if ($this->user !== null) {
            ReflectionUtil::setPrivateProperty(AbstractContact::class, $contact, 'user', $this->user);
        }
        ReflectionUtil::setPrivateProperty(AbstractContact::class, $contact, 'isPreferable', $this->isPreferable);

        return $this;
    }
}
