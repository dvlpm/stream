<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Builder;

use Dvlpm\Stream\Domain\Content\Model\AbstractContent;
use Dvlpm\Stream\Domain\Content\Model\ContentType;
use Dvlpm\Stream\Domain\User\Model\User;
use Dvlpm\Stream\Tests\Support\Util\ReflectionUtil;
use Ramsey\Uuid\UuidInterface;

trait ContentBuilderTrait
{
    private ?UuidInterface $id = null;
    private User $owner;

    public function withId(?UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function withOwner(User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    protected function applyCommonPropertiesTo(AbstractContent $content): self
    {
        ReflectionUtil::setPrivateProperty(AbstractContent::class, $content, 'id', $this->id);
        ReflectionUtil::setPrivateProperty(AbstractContent::class, $content, 'owner', $this->owner);

        return $this;
    }
}
