<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Builder;

use Dvlpm\Stream\Domain\User\Model\Email;
use Dvlpm\Stream\Domain\User\Model\User;
use Dvlpm\Stream\Tests\Support\Stub\EmailStub;
use Dvlpm\Stream\Tests\Support\Util\ReflectionUtil;

final class EmailBuilder
{
    use ContactBuilderTrait, HasObjectCollectionTrait;

    private string $email;
    private bool $isConfirmed = true;

    public function __construct(?User $user = null)
    {
        $this->email = EmailStub::FIRST_EMAIL;
        $this->user = $user;
    }

    public static function draft(): self
    {
        return new static();
    }

    public static function create(User $user): self
    {
        return new static($user);
    }

    public function withEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function withIsConfirmed(bool $isConfirmed): self
    {
        $this->isConfirmed = $isConfirmed;

        return $this;
    }

    public function build(): Email
    {
        /** @var Email $email */
        $email = ReflectionUtil::instantiate(Email::class);

        ReflectionUtil::setPrivateProperty(Email::class, $email, 'email', $this->email);
        ReflectionUtil::setPrivateProperty(Email::class, $email, 'isConfirmed', $this->isConfirmed);

        $this
            ->applyCommonPropertiesTo($email)
            ->collect($email);

        return $email;
    }
}
