<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Builder;

use Dvlpm\Stream\Domain\Common\Model\Name;
use Dvlpm\Stream\Domain\Space\Model\Hall;
use Dvlpm\Stream\Domain\Space\Model\Identifier;
use Dvlpm\Stream\Domain\Space\Model\Host;
use Dvlpm\Stream\Domain\Space\Model\Room;
use Dvlpm\Stream\Domain\User\Model\User;
use Dvlpm\Stream\Tests\Support\DataObject\ObjectCollection;
use Dvlpm\Stream\Tests\Support\Stub\HallStub;
use Dvlpm\Stream\Tests\Support\Util\ReflectionUtil;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

final class HallBuilder
{
    use HasObjectCollectionTrait;

    private UuidInterface $id;
    private Identifier $identifier;
    /** @var Host[] */
    private iterable $hosts = [];
    /** @var Room[] */
    private iterable $rooms = [];
    private ?Name $name;
    private ?string $description = null;

    private Hall $hall;

    public function __construct()
    {
        $this->id = Uuid::uuid4();
        $this->identifier = Identifier::fromValueOrFail(HallStub::WITH_EVERY_ACCESS_TYPE_IDENTIFIER);
        $this->name = Name::fromValue(HallStub::NAME);
        $this->objectCollection = ObjectCollection::empty();
        $this->hall = ReflectionUtil::instantiateWithConstructorArgs(
            Hall::class,
            $this->identifier,
            $this->name,
            $this->description,
        );
    }

    public static function create(): self
    {
        return new static();
    }

    public function withOwner(User $user): self
    {
        $this->hosts[] = HostBuilder::create($this->hall, $user)
            ->withObjectCollection($this->objectCollection)
            ->asOwner()
            ->build();

        return $this;
    }

    public function withAdministrator(User $user): self
    {
        $this->hosts[] = HostBuilder::create($this->hall, $user)
            ->withObjectCollection($this->objectCollection)
            ->asAdministrator()
            ->build();

        return $this;
    }

    public function withId(UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function withIdentifier(Identifier $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function withHosts(array $hosts): self
    {
        $this->hosts = $hosts;

        return $this;
    }

    public function withRooms(Room ...$rooms): self
    {
        $this->rooms = $rooms;

        return $this;
    }

    public function withName(?Name $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function withDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function build(): Hall
    {
        ReflectionUtil::setPrivateProperty(Hall::class, $this->hall, 'rooms', $this->rooms);
        foreach ($this->rooms as $room) {
            ReflectionUtil::setPrivateProperty(Room::class, $room, 'hall', $this->hall);
        }

        ReflectionUtil::setPrivateProperty(Hall::class, $this->hall, 'hosts', $this->hosts);
        foreach ($this->hosts as $host) {
            ReflectionUtil::setPrivateProperty(Host::class, $host, 'hall', $this->hall);
        }

        ReflectionUtil::setPrivateProperty(Hall::class, $this->hall, 'identifier', $this->identifier);

        $this->collect($this->hall);

        return $this->hall;
    }
}
