<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Builder;

use Dvlpm\Stream\Tests\Support\DataObject\ObjectCollection;

trait HasObjectCollectionTrait
{
    private ?ObjectCollection $objectCollection = null;

    public function withObjectCollection(ObjectCollection $objectCollection): self
    {
        $this->objectCollection = $objectCollection;

        return $this;
    }

    private function collectAll(object ...$objects): self
    {
        foreach ($objects as $object) {
            $this->collect($object);
        }

        return $this;
    }

    private function collect(object $object): self
    {
        if ($this->objectCollection === null) {
            $this->objectCollection = ObjectCollection::empty();
        }

        $this->objectCollection->add($object);

        return $this;
    }
}
