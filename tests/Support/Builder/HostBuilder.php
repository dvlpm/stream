<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Builder;

use Dvlpm\Stream\Domain\Space\Model\Hall;
use Dvlpm\Stream\Domain\Space\Model\Host;
use Dvlpm\Stream\Domain\Space\Model\HostRole;
use Dvlpm\Stream\Domain\User\Model\User;
use Dvlpm\Stream\Tests\Support\Util\ReflectionUtil;

final class HostBuilder
{
    use HasObjectCollectionTrait;

    private User $user;
    private HostRole $role;
    private Hall $hall;

    private function __construct(Hall $hall, User $user)
    {
        $this->hall = $hall;
        $this->user = $user;
        $this->role = HostRole::OWNER();
    }

    public static function create(Hall $hall, User $user): self
    {
        return new static($hall, $user);
    }

    public function asOwner(): self
    {
        $this->role = HostRole::OWNER();

        return $this;
    }

    public function asAdministrator(): self
    {
        $this->role = HostRole::OWNER();

        return $this;
    }

    public function withRole(HostRole $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function build(): Host
    {
        /** @var Host $host */
        $host = ReflectionUtil::instantiateWithConstructorArgs(
            Host::class,
            $this->hall,
            $this->user,
            $this->role
        );

        $this->collect($host);

        return $host;
    }
}
