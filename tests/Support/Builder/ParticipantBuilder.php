<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Builder;

use Dvlpm\Stream\Domain\Space\Model\Room;
use Dvlpm\Stream\Domain\Space\Model\Participant;
use Dvlpm\Stream\Domain\Space\Model\ParticipantIdentification;
use Dvlpm\Stream\Domain\Space\Model\ParticipantStatus;
use Dvlpm\Stream\Domain\User\Model\User;
use Dvlpm\Stream\Tests\Support\Stub\ParticipantStub;
use Dvlpm\Stream\Tests\Support\Util\ReflectionUtil;
use Ramsey\Uuid\Nonstandard\Uuid;
use Ramsey\Uuid\UuidInterface;

final class ParticipantBuilder
{
    private ?UuidInterface $id;
    private ?Room $room;
    private ParticipantStatus $status;
    private ParticipantIdentification $identification;
    private ?User $user = null;

    private function __construct(?Room $room = null) {
        $this->id = Uuid::uuid4();
        $this->status = ParticipantStatus::PENDING();
        $this->identification = ParticipantIdentification::fromUserId(Uuid::uuid4());
        $this->room = $room;
    }

    public static function draft(): self
    {
        return new static();
    }

    public static function create(Room $room): self
    {
        return new static($room);
    }

    public function asRegisteredIdentification(): self
    {
        $this->identification = ParticipantIdentification::fromUserId(
            Uuid::fromString(ParticipantStub::IDENTIFIED_BY_ID_USER_ID)
        );

        return $this;
    }

    public function asGuestIdentification(): self
    {
        $this->identification = ParticipantIdentification::fromUserId(
            Uuid::fromString(ParticipantStub::GUEST_USER_ID)
        );

        return $this;
    }

    public function asIdentifiedByEmail(): self
    {
        $this->identification = ParticipantIdentification::fromEmail(ParticipantStub::EMAIL);
        $this->id = Uuid::fromString(ParticipantStub::WITH_EMAIL_ID);

        return $this;
    }

    public function withId(?UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function withRoom(?Room $room): self
    {
        $this->room = $room;

        return $this;
    }

    public function withStatus(ParticipantStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function withIdentification(ParticipantIdentification $identification): self
    {
        $this->identification = $identification;

        return $this;
    }

    public function withUser(User $user): self
    {
        $this->user = $user;
        $this->identification = ParticipantIdentification::fromUserId($user->getId());

        return $this;
    }

    public function build(): Participant
    {
        /** @var Participant $participant */
        $participant = ReflectionUtil::instantiate(Participant::class);

        if ($this->room !== null) {
            ReflectionUtil::setPrivateProperty(Participant::class, $participant, 'room', $this->room);
        }

        ReflectionUtil::setPrivateProperty(Participant::class, $participant, 'id', $this->id);
        ReflectionUtil::setPrivateProperty(Participant::class, $participant, 'identification', $this->identification);
        ReflectionUtil::setPrivateProperty(Participant::class, $participant, 'status', $this->status);
        ReflectionUtil::setPrivateProperty(Participant::class, $participant, 'user', $this->user);

        return $participant;
    }
}
