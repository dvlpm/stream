<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Builder;

use Dvlpm\Stream\Domain\Content\Model\PlanningPoker;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerCard;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerPlayer;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerStory;
use Dvlpm\Stream\Tests\Support\Util\ReflectionUtil;

final class PlanningPokerBuilder
{
    use HasObjectCollectionTrait, ContentBuilderTrait;

    /** @var PlanningPokerStory[] */
    private iterable $stories = [];
    /** @var PlanningPokerCard[] */
    private iterable $cards;
    /** @var PlanningPokerPlayer[] */
    private iterable $players = [];

    private function __construct()
    {
        $this->cards = [
            PlanningPokerCard::HALF(),
            PlanningPokerCard::ONE(),
            PlanningPokerCard::QUESTION(),
        ];
    }

    public static function create(): self
    {
        return new static();
    }

    public function withStories(PlanningPokerStory ...$stories): self
    {
        $this->stories = $stories;

        return $this;
    }

    public function withPlayers(PlanningPokerPlayer ...$players): self
    {
        $this->players = $players;

        return $this;
    }

    public function build(): PlanningPoker
    {
        /** @var PlanningPoker $poker */
        $poker = ReflectionUtil::instantiateWithConstructorArgs(
            PlanningPoker::class,
            $this->owner,
            $this->cards
        );

        ReflectionUtil::setPrivateProperty(PlanningPoker::class, $poker, 'stories', $this->stories);
        ReflectionUtil::setPrivateProperty(PlanningPoker::class, $poker, 'players', $this->players);

        foreach ($this->stories as $story) {
            ReflectionUtil::setPrivateProperty(PlanningPokerStory::class, $story, 'poker', $poker);
        }

        foreach ($this->players as $player) {
            ReflectionUtil::setPrivateProperty(PlanningPokerPlayer::class, $player, 'poker', $poker);
        }

        $this
            ->applyCommonPropertiesTo($poker)
            ->collect($poker);

        return $poker;
    }
}
