<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Builder;

use Dvlpm\Stream\Domain\Content\Model\PlanningPokerPlayer;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerPlayerStatus;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerStory;
use Dvlpm\Stream\Domain\User\Model\User;
use Dvlpm\Stream\Tests\Support\Stub\PlanningPokerPlayerStub;
use Dvlpm\Stream\Tests\Support\Util\ReflectionUtil;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

final class PlanningPokerPlayerBuilder
{
    use HasObjectCollectionTrait;

    private UuidInterface $id;
    private User $user;
    private PlanningPokerPlayerStatus $status;
    private ?PlanningPokerStory $poker = null;

    private function __construct(User $user)
    {
        $this->id = Uuid::uuid4();
        $this->user = $user;
        $this->status = PlanningPokerPlayerStatus::ACTIVE();
    }

    public static function draft(User $user): self
    {
        return new static($user);
    }

    public function asDefault(): self
    {
        $this->id = Uuid::fromString(PlanningPokerPlayerStub::ID);

        return $this;
    }

    public function withId(UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function withStatus(PlanningPokerPlayerStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function build(): PlanningPokerPlayer
    {
        /** @var PlanningPokerPlayer $planningPokerPlayer */
        $planningPokerPlayer = ReflectionUtil::instantiate(PlanningPokerPlayer::class);

        if ($this->poker !== null) {
            ReflectionUtil::setPrivateProperty(PlanningPokerPlayer::class, $planningPokerPlayer, 'poker', $this->poker);
        }
        ReflectionUtil::setPrivateProperty(PlanningPokerPlayer::class, $planningPokerPlayer, 'user', $this->user);
        ReflectionUtil::setPrivateProperty(PlanningPokerPlayer::class, $planningPokerPlayer, 'status', $this->status);
        ReflectionUtil::setPrivateProperty(PlanningPokerPlayer::class, $planningPokerPlayer, 'id', $this->id);

        $this->collect($planningPokerPlayer);

        return $planningPokerPlayer;
    }
}
