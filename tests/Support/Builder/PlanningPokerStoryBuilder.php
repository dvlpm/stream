<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Builder;

use Dvlpm\Stream\Domain\Common\Model\Name;
use Dvlpm\Stream\Domain\Content\Model\PlanningPoker;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerStory;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerStoryStatus;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerStoryVote;
use Dvlpm\Stream\Tests\Support\Stub\PlanningPokerStoryStub;
use Dvlpm\Stream\Tests\Support\Util\ReflectionUtil;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

final class PlanningPokerStoryBuilder
{
    use HasObjectCollectionTrait;

    private ?UuidInterface $id;
    private ?PlanningPoker $poker = null;
    private Name $name;
    private int $order = 0;
    private PlanningPokerStoryStatus $status;
    private ?string $description;
    private string $score = '';
    private ?string $link;
    /** @var PlanningPokerStoryVote[] */
    private iterable $votes = [];

    private function __construct()
    {
        $this->id = Uuid::uuid4();
        $this->name = Name::fromValueOrFail(PlanningPokerStoryStub::NAME);
        $this->description = PlanningPokerStoryStub::DESCRIPTION;
        $this->link = PlanningPokerStoryStub::EXTRENAL_LINK;
        $this->status = PlanningPokerStoryStatus::BACKLOG();
    }

    public static function draft(): self
    {
        return new static();
    }

    public function asActive(): self
    {
        $this->id = Uuid::fromString(PlanningPokerStoryStub::ID);
        $this->status = PlanningPokerStoryStatus::ACTIVE();

        return $this;
    }

    public function asBacklog(): self
    {
        $this->id = Uuid::fromString(PlanningPokerStoryStub::BACKLOG_ID);
        $this->name = Name::fromValueOrFail(PlanningPokerStoryStub::BACKLOG_NAME);
        $this->status = PlanningPokerStoryStatus::BACKLOG();

        return $this;
    }

    public function asCompleted(): self
    {
        $this->id = Uuid::fromString(PlanningPokerStoryStub::COMPLETED_ID);
        $this->name = Name::fromValueOrFail(PlanningPokerStoryStub::COMPLETED_NAME);
        $this->description = PlanningPokerStoryStub::COMPLETED_DESCRIPTION;
        $this->score = PlanningPokerStoryStub::COMPLETED_SCORE;
        $this->status = PlanningPokerStoryStatus::COMPLETED();

        return $this;
    }

    public function withId(?UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function withPoker(PlanningPoker $poker): self
    {
        $this->poker = $poker;

        return $this;
    }

    public function withName(Name $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function withOrder(int $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function withStatus(PlanningPokerStoryStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function withDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function withScore(string $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function withExternalLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function withVotes(PlanningPokerStoryVote ...$votes): self
    {
        $this->votes = $votes;

        return $this;
    }

    public function build(): PlanningPokerStory
    {
        /** @var PlanningPokerStory $planningPokerStory */
        $planningPokerStory = ReflectionUtil::instantiate(PlanningPokerStory::class);

        ReflectionUtil::setPrivateProperty(PlanningPokerStory::class, $planningPokerStory, 'votes', $this->votes);
        foreach ($this->votes as $vote) {
            ReflectionUtil::setPrivateProperty(PlanningPokerStoryVote::class, $vote, 'story', $planningPokerStory);
        }

        if ($this->poker !== null) {
            ReflectionUtil::setPrivateProperty(PlanningPokerStory::class, $planningPokerStory, 'poker', $this->poker);
        }
        ReflectionUtil::setPrivateProperty(PlanningPokerStory::class, $planningPokerStory, 'name', $this->name);
        ReflectionUtil::setPrivateProperty(PlanningPokerStory::class, $planningPokerStory, 'order', $this->order);
        ReflectionUtil::setPrivateProperty(PlanningPokerStory::class, $planningPokerStory, 'link', $this->link);
        ReflectionUtil::setPrivateProperty(PlanningPokerStory::class, $planningPokerStory, 'description', $this->description);
        ReflectionUtil::setPrivateProperty(PlanningPokerStory::class, $planningPokerStory, 'id', $this->id);
        ReflectionUtil::setPrivateProperty(PlanningPokerStory::class, $planningPokerStory, 'score', $this->score);
        ReflectionUtil::setPrivateProperty(PlanningPokerStory::class, $planningPokerStory, 'status', $this->status);

        $this->collect($planningPokerStory);

        return $planningPokerStory;
    }
}
