<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Builder;

use Dvlpm\Stream\Domain\Content\Model\PlanningPokerCard;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerStory;
use Dvlpm\Stream\Domain\Content\Model\PlanningPokerStoryVote;
use Dvlpm\Stream\Domain\User\Model\User;
use Dvlpm\Stream\Tests\Support\Stub\PlanningPokerStoryVoteStub;
use Dvlpm\Stream\Tests\Support\Util\ReflectionUtil;

final class PlanningPokerStoryVoteBuilder
{
    use HasObjectCollectionTrait;

    private User $user;
    private PlanningPokerCard $card;
    private ?PlanningPokerStory $story = null;

    private function __construct(User $user)
    {
        $this->user = $user;
        $this->card = PlanningPokerCard::createByName(PlanningPokerStoryVoteStub::CARD);
    }

    public static function draft(User $user): self
    {
        return new static($user);
    }

    public function withCard(PlanningPokerCard $card): self
    {
        $this->card = $card;

        return $this;
    }

    public function build(): PlanningPokerStoryVote
    {
        /** @var PlanningPokerStoryVote $planningPokerStoryVote */
        $planningPokerStoryVote = ReflectionUtil::instantiate(PlanningPokerStoryVote::class);

        if ($this->story !== null) {
            ReflectionUtil::setPrivateProperty(PlanningPokerStoryVote::class, $planningPokerStoryVote, 'poker', $this->poker);
        }
        ReflectionUtil::setPrivateProperty(PlanningPokerStoryVote::class, $planningPokerStoryVote, 'user', $this->user);
        ReflectionUtil::setPrivateProperty(PlanningPokerStoryVote::class, $planningPokerStoryVote, 'card', $this->card);

        $this->collect($planningPokerStoryVote);

        return $planningPokerStoryVote;
    }
}
