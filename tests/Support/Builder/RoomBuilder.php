<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Builder;

use Dvlpm\Stream\Domain\Common\Model\Name;
use Dvlpm\Stream\Domain\Content\Model\AbstractContent;
use Dvlpm\Stream\Domain\Space\Model\AccessType;
use Dvlpm\Stream\Domain\Space\Model\Hall;
use Dvlpm\Stream\Domain\Space\Model\Key;
use Dvlpm\Stream\Domain\Space\Model\Participant;
use Dvlpm\Stream\Domain\Space\Model\Room;
use Dvlpm\Stream\Tests\Support\Stub\RoomStub;
use Dvlpm\Stream\Tests\Support\Util\ReflectionUtil;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

final class RoomBuilder
{
    use HasObjectCollectionTrait;

    private ?Hall $hall;
    private ?UuidInterface $id;
    private Name $name;
    private Key $key;
    private AccessType $accessType;
    /** @var Participant[] */
    private iterable $participants = [];
    private ?string $description = null;
    /** @var AbstractContent[] */
    private iterable $contents = [];

    private Room $room;

    public function __construct(?Hall $hall = null)
    {
        $this->hall = $hall;
        $this->id = Uuid::uuid4();
        $this->name = Name::fromValue(RoomStub::ANONYMOUS_NAME);
        $this->accessType = AccessType::createByName(RoomStub::ANONYMOUS_ACCESS_TYPE);
        $this->key = Key::generate();
        $this->room = ReflectionUtil::instantiate(Room::class);
    }

    public static function draft(): self
    {
        return new static();
    }

    public static function create(Hall $hall): self
    {
        return new static($hall);
    }

    public function asAnonymous(): self
    {
        $this->id = Uuid::fromString(RoomStub::ANONYMOUS_ID);

        return $this;
    }

    public function asAuthorized(): self
    {
        $this->id = Uuid::fromString(RoomStub::AUTHORIZED_ID);
        $this->name = Name::fromValue(RoomStub::AUTHORIZED_NAME);
        $this->accessType = AccessType::createByName(RoomStub::AUTHORIZED_ACCESS_TYPE);

        return $this;
    }

    public function asListed(Participant ...$participants): self
    {
        $this->id = Uuid::fromString(RoomStub::LISTED_ID);
        $this->name = Name::fromValue(RoomStub::LISTED_NAME);
        $this->key = Key::fromValue(RoomStub::LISTED_KEY);
        $this->accessType = AccessType::createByName(RoomStub::LISTED_ACCESS_TYPE);
        $this->withParticipants(...$participants);

        return $this;
    }

    public function withId(?UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function withHall(Hall $hall): self
    {
        $this->hall = $hall;

        return $this;
    }

    public function withName(Name $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function withAccessType(AccessType $accessType): self
    {
        $this->accessType = $accessType;

        return $this;
    }

    public function withParticipants(Participant ...$participants): self
    {
        foreach ($participants as $participant) {
            $this->participants[] = $participant;
        }

        return $this;
    }

    public function withDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function withContents(AbstractContent ...$contents): self
    {
        $this->contents = $contents;

        return $this;
    }

    public function build(): Room
    {
        if ($this->hall !== null) {
            $this->room->attachToHall($this->hall);
        }

        ReflectionUtil::setPrivateProperty(Room::class, $this->room, 'id', $this->id);
        ReflectionUtil::setPrivateProperty(Room::class, $this->room, 'contents', $this->contents);
        ReflectionUtil::setPrivateProperty(Room::class, $this->room, 'participants', $this->participants);
        foreach ($this->participants as $participant) {
            ReflectionUtil::setPrivateProperty(Participant::class, $participant, 'room', $this->room);
        }

        ReflectionUtil::setPrivateProperty(Room::class, $this->room, 'name', $this->name);
        ReflectionUtil::setPrivateProperty(Room::class, $this->room, 'accessType', $this->accessType);
        ReflectionUtil::setPrivateProperty(Room::class, $this->room, 'description', $this->description);
        ReflectionUtil::setPrivateProperty(Room::class, $this->room, 'key', $this->key);

        $this->collect($this->room);

        return $this->room;
    }
}
