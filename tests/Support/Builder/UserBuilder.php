<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Builder;

use Dvlpm\Stream\Domain\Common\Model\Meta;
use Dvlpm\Stream\Domain\User\Model\AbstractContact;
use Dvlpm\Stream\Domain\User\Model\ExternalId;
use Dvlpm\Stream\Domain\User\Model\User;
use Dvlpm\Stream\Domain\User\Model\UserRole;
use Dvlpm\Stream\Tests\Support\Stub\UserStub;
use Dvlpm\Stream\Tests\Support\Util\ReflectionUtil;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

final class UserBuilder
{
    use HasObjectCollectionTrait;

    private UuidInterface $id;
    private UserRole $role;
    private Meta $meta;
    private ?ExternalId $externalId = null;
    /** @var AbstractContact[] */
    private iterable $contacts = [];

    private function __construct()
    {
        $this->id = Uuid::uuid4();
        $this->role = UserRole::GUEST();
        $this->meta = Meta::empty();
    }

    public static function create(): self
    {
        return new static();
    }

    public function asRegistered(): self
    {
        return $this
            ->withId(Uuid::fromString(UserStub::REGISTERED_USER_ID))
            ->withExternalId(ExternalId::fromValue(UserStub::REGISTERED_USER_EXTERNAL_ID))
            ->withRole(UserRole::USER());
    }

    public function asRegisteredWithEmail(): self
    {
        return $this
            ->withId(Uuid::fromString(UserStub::REGISTERED_WITH_EMAIL_USER_ID))
            ->withExternalId(ExternalId::fromValue(UserStub::REGISTERED_WITH_EMAIL_EXTERNAL_ID))
            ->withRole(UserRole::USER())
            ->withContacts(EmailBuilder::draft()->build());
    }

    public function asGuest(): self
    {
        return $this->withId(Uuid::fromString(UserStub::GUEST_USER_ID));
    }

    public function withId(UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function withRole(UserRole $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function withMeta(Meta $meta): self
    {
        $this->meta = $meta;

        return $this;
    }

    public function withExternalId(?ExternalId $externalId): self
    {
        $this->externalId = $externalId;

        return $this;
    }

    public function withContacts(AbstractContact ...$contacts): self
    {
        $this->contacts = $contacts;

        return $this;
    }

    public function build(): User
    {
        /** @var User $user */
        $user = ReflectionUtil::instantiateWithConstructorArgs(User::class, $this->role, $this->meta, $this->id);
        ReflectionUtil::setPrivateProperty(User::class, $user, 'externalId', $this->externalId);
        ReflectionUtil::setPrivateProperty(User::class, $user, 'contacts', $this->contacts);

        foreach ($this->contacts as $contact) {
            ReflectionUtil::setPrivateProperty(AbstractContact::class, $contact, 'user', $user);
        }

        $this->collect($user);

        return $user;
    }
}
