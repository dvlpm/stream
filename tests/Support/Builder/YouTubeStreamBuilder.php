<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Builder;

use Dvlpm\Stream\Domain\Content\Model\YouTubeStream;
use Dvlpm\Stream\Tests\Support\Stub\YouTubeStreamStub;
use Dvlpm\Stream\Tests\Support\Util\ReflectionUtil;

final class YouTubeStreamBuilder
{
    use HasObjectCollectionTrait, ContentBuilderTrait;

    private string $link;

    private function __construct()
    {
        $this->link = YouTubeStreamStub::LINK;
    }

    public static function create(): self
    {
        return new static();
    }

    public function withLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function build(): YouTubeStream
    {
        /** @var YouTubeStream $youTubeStream */
        $youTubeStream = ReflectionUtil::instantiateWithConstructorArgs(
            YouTubeStream::class,
            $this->owner,
            $this->link
        );

        $this
            ->applyCommonPropertiesTo($youTubeStream)
            ->collect($youTubeStream);

        return $youTubeStream;
    }
}
