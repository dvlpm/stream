<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\DataObject;

use Doctrine\Common\Collections\ArrayCollection;
use Iterator;
use IteratorAggregate;

final class ObjectCollection implements IteratorAggregate
{
    private ArrayCollection $objects;

    private function __construct(iterable $objects = [])
    {
        $this->objects = new ArrayCollection($objects);
    }

    public static function empty(): self
    {
        return new static();
    }

    public function add(object $object): self
    {
        $this->objects->add($object);

        return $this;
    }

    public function getIterator(): Iterator
    {
        return $this->objects->getIterator();
    }
}
