<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Fixture;

use Doctrine\Common\Persistence\ObjectManager;

trait EntitiesFixtureTrait
{
    abstract protected function getEntities(): iterable;

    public function load(ObjectManager $manager): void
    {
        foreach ($this->getEntities() as $entity) {
            $manager->persist($entity);
        }

        $manager->flush();
    }
}
