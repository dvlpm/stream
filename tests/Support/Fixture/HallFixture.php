<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Dvlpm\Stream\Domain\Common\Model\Name;
use Dvlpm\Stream\Domain\Content\Model\PlanningPoker;
use Dvlpm\Stream\Domain\Content\Model\YouTubeStream;
use Dvlpm\Stream\Domain\Space\Model\Identifier;
use Dvlpm\Stream\Domain\Space\Model\ParticipantStatus;
use Dvlpm\Stream\Domain\User\Model\User;
use Dvlpm\Stream\Tests\Support\Builder\HallBuilder;
use Dvlpm\Stream\Tests\Support\Builder\ParticipantBuilder;
use Dvlpm\Stream\Tests\Support\Builder\RoomBuilder;
use Dvlpm\Stream\Tests\Support\DataObject\ObjectCollection;
use Dvlpm\Stream\Tests\Support\Stub\HallStub;

final class HallFixture extends AbstractFixture implements DependentFixtureInterface
{
    use EntitiesFixtureTrait;

    protected function getEntities(): iterable
    {
        /** @var User $user */
        $user = $this->getReference(UserFixture::REGISTERED_USER_REFERENCE);
        /** @var User $secondUser */
        $secondUser = $this->getReference(UserFixture::SECOND_REGISTERED_USER_REFERENCE);

        /** @var YouTubeStream $youtubeStream */
        $youtubeStream = $this->getReference(YouTubeStreamFixture::YOUTUBE_STREAM_REFERENCE);

        /** @var PlanningPoker $planningPoker */
        $planningPoker = $this->getReference(PlanningPokerFixture::PLANNING_POKER_WITH_STORIES_REFERENCE);

        HallBuilder::create()
            ->withObjectCollection($objectCollection = ObjectCollection::empty())
            ->withOwner($user)
            ->withRooms(
                RoomBuilder::draft()
                    ->asAnonymous()
                    ->build(),
                RoomBuilder::draft()
                    ->asAuthorized()
                    ->withContents($youtubeStream)
                    ->build(),
                RoomBuilder::draft()
                    ->asListed(
                        ParticipantBuilder::draft()
                            ->build(),
                        ParticipantBuilder::draft()
                            ->asGuestIdentification()
                            ->build(),
                        ParticipantBuilder::draft()
                            ->asRegisteredIdentification()
                            ->build(),
                        ParticipantBuilder::draft()
                            ->asIdentifiedByEmail()
                            ->build(),
                        ParticipantBuilder::draft()
                            ->withUser($user)
                            ->withStatus(ParticipantStatus::ACTIVE())
                            ->build(),
                    )
                    ->withContents($planningPoker)
                    ->build()
            )
            ->build();

        HallBuilder::create()
            ->withObjectCollection($objectCollection)
            ->withIdentifier(Identifier::fromValueOrFail(HallStub::ADMINISTRATED_BY_REGISTERED_USER_IDENTIFIER))
            ->withName(Name::fromValueOrFail(HallStub::ADMINISTRATED_BY_REGISTERED_USER_NAME))
            ->withAdministrator($user)
            ->build();

        HallBuilder::create()
            ->withObjectCollection($objectCollection)
            ->withIdentifier(Identifier::fromValueOrFail(HallStub::OWNED_BY_SECOND_REGISTERED_USER_IDENTIFIER))
            ->withName(Name::fromValueOrFail(HallStub::OWNED_BY_SECOND_REGISTERED_USER_NAME))
            ->withOwner($secondUser)
            ->build();

        return $objectCollection;
    }

    public function getDependencies(): array
    {
        return [
            UserFixture::class,
            YouTubeStreamFixture::class,
            PlanningPokerFixture::class,
        ];
    }
}
