<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Dvlpm\Stream\Domain\User\Model\User;
use Dvlpm\Stream\Tests\Support\Builder\PlanningPokerBuilder;
use Dvlpm\Stream\Tests\Support\Builder\PlanningPokerPlayerBuilder;
use Dvlpm\Stream\Tests\Support\Builder\PlanningPokerStoryBuilder;
use Dvlpm\Stream\Tests\Support\Builder\PlanningPokerStoryVoteBuilder;
use Dvlpm\Stream\Tests\Support\DataObject\ObjectCollection;
use Dvlpm\Stream\Tests\Support\Stub\ContentStub;
use Ramsey\Uuid\Uuid;

final class PlanningPokerFixture extends AbstractFixture implements DependentFixtureInterface
{
    use EntitiesFixtureTrait;

    public const PLANNING_POKER_REFERENCE = 'PLANNING_POKER_REFERENCE';
    public const PLANNING_POKER_WITH_STORIES_REFERENCE = 'PLANNING_POKER_WITH_STORIES_REFERENCE';

    protected function getEntities(): iterable
    {
        /** @var User $user */
        $user = $this->getReference(UserFixture::REGISTERED_USER_REFERENCE);

        $this->setReference(self::PLANNING_POKER_REFERENCE,
            PlanningPokerBuilder::create()
                ->withObjectCollection($objectCollection = ObjectCollection::empty())
                ->withId(Uuid::fromString(ContentStub::PLANNING_POKER_ID))
                ->withOwner($user)
                ->build()
        );

        $this->setReference(self::PLANNING_POKER_WITH_STORIES_REFERENCE,
            PlanningPokerBuilder::create()
                ->withObjectCollection($objectCollection)
                ->withId(Uuid::fromString(ContentStub::PLANNING_POKER_WITH_STORIES_ID))
                ->withOwner($user)
                ->withPlayers(
                    PlanningPokerPlayerBuilder::draft($user)
                        ->asDefault()
                        ->build()
                )
                ->withStories(
                    PlanningPokerStoryBuilder::draft()
                        ->asActive()
                        ->withVotes(PlanningPokerStoryVoteBuilder::draft($user)->build())
                        ->build(),
                    PlanningPokerStoryBuilder::draft()
                        ->asBacklog()
                        ->build(),
                    PlanningPokerStoryBuilder::draft()
                        ->asCompleted()
                        ->withVotes(PlanningPokerStoryVoteBuilder::draft($user)->build())
                        ->build()
                )
                ->build()
        );

        return $objectCollection;
    }

    public function getDependencies(): array
    {
        return [
            UserFixture::class,
        ];
    }
}
