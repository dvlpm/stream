<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Dvlpm\Stream\Tests\Support\Builder\UserBuilder;
use Dvlpm\Stream\Tests\Support\DataObject\ObjectCollection;

final class UserFixture extends AbstractFixture
{
    use EntitiesFixtureTrait;

    public const REGISTERED_USER_REFERENCE = 'REGISTERED_USER_REFERENCE';
    public const SECOND_REGISTERED_USER_REFERENCE = 'SECOND_REGISTERED_USER_REFERENCE';
    public const GUEST_USER_REFERENCE = 'GUEST_USER_REFERENCE';

    protected function getEntities(): iterable
    {
        $objectCollection = ObjectCollection::empty();

        $this->setReference(self::REGISTERED_USER_REFERENCE, UserBuilder::create()
            ->withObjectCollection($objectCollection)
            ->asRegistered()
            ->build()
        );

        $this->setReference(self::SECOND_REGISTERED_USER_REFERENCE, UserBuilder::create()
            ->withObjectCollection($objectCollection)
            ->asRegisteredWithEmail()
            ->build()
        );

        $this->setReference(self::GUEST_USER_REFERENCE, UserBuilder::create()
            ->withObjectCollection($objectCollection)
            ->asGuest()
            ->build()
        );

        return $objectCollection;
    }
}
