<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Dvlpm\Stream\Tests\Support\Builder\YouTubeStreamBuilder;
use Dvlpm\Stream\Tests\Support\DataObject\ObjectCollection;
use Dvlpm\Stream\Tests\Support\Stub\ContentStub;
use Dvlpm\Stream\Tests\Support\Stub\YouTubeStreamStub;
use Ramsey\Uuid\Uuid;

final class YouTubeStreamFixture extends AbstractFixture implements DependentFixtureInterface
{
    use EntitiesFixtureTrait;

    public const YOUTUBE_STREAM_REFERENCE = 'YOUTUBE_STREAM_REFERENCE';
    public const ANOTHER_YOUTUBE_STREAM_REFERENCE = 'ANOTHER_YOUTUBE_STREAM_REFERENCE';

    protected function getEntities(): iterable
    {
        $objectCollection = ObjectCollection::empty();

        $this->setReference(self::YOUTUBE_STREAM_REFERENCE,
            YouTubeStreamBuilder::create()
                ->withObjectCollection($objectCollection)
                ->withId(Uuid::fromString(ContentStub::YOUTUBE_STREAM_ID))
                ->withOwner($this->getReference(UserFixture::REGISTERED_USER_REFERENCE))
                ->build()
        );

        $this->setReference(self::ANOTHER_YOUTUBE_STREAM_REFERENCE,
            YouTubeStreamBuilder::create()
                ->withObjectCollection($objectCollection)
                ->withId(Uuid::fromString(ContentStub::ANOTHER_YOUTUBE_STREAM_ID))
                ->withLink(YouTubeStreamStub::ANOTHER_LINK)
                ->withOwner($this->getReference(UserFixture::REGISTERED_USER_REFERENCE))
                ->build()
        );

        return $objectCollection;
    }

    public function getDependencies(): array
    {
        return [
            UserFixture::class,
        ];
    }
}
