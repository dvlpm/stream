<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Generator;

use Dvlpm\Stream\Tests\Support\Stub\ParticipantStub;
use Dvlpm\Stream\Tests\Support\Stub\UserStub;

final class CreateParticipantDataGenerator
{
    public static function valid(array $data = []): array
    {
        return array_replace_recursive([
            'id' => ParticipantStub::TO_ADD_ID,
            'identification' => $data['identification'] ?? [
                    'userId' => UserStub::REGISTERED_WITH_EMAIL_USER_ID,
                ],
        ], $data);
    }
}
