<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Generator;

use Dvlpm\Stream\Tests\Support\Stub\ContentStub;
use Dvlpm\Stream\Tests\Support\Stub\PlanningPokerStub;

final class CreatePlanningPokerDataGenerator
{
    public static function valid(array $data = []): array
    {
        return array_replace_recursive([
            'id' => ContentStub::PLANNING_POKER_ID,
            'type' => PlanningPokerStub::TYPE,
            'cards' => PlanningPokerStub::CARDS,
        ], $data);
    }
}
