<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Generator;

use Dvlpm\Stream\Tests\Support\Stub\PlanningPokerPlayerStub;

final class CreatePlanningPokerPlayerDataGenerator
{
    public static function valid(array $data = []): array
    {
        return array_replace_recursive([
            'id' => PlanningPokerPlayerStub::ID_FOR_CREATE,
            'user' => [
                'id' => PlanningPokerPlayerStub::USER_ID_FOR_CREATE,
            ],
        ], $data);
    }
}
