<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Generator;

use Dvlpm\Stream\Tests\Support\Stub\PlanningPokerStoryStub;

final class CreatePlanningPokerStoryDataGenerator
{
    public static function valid(array $data = []): array
    {
        return array_replace_recursive([
            'id' => PlanningPokerStoryStub::ID_FOR_CREATE,
            'name' => PlanningPokerStoryStub::NAME_FOR_CREATE,
            'description' => PlanningPokerStoryStub::DESCRIPTION_FOR_CREATE,
            'link' => PlanningPokerStoryStub::EXTRENAL_LINK_FOR_CREATE,
        ], $data);
    }
}
