<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Generator;

use Dvlpm\Stream\Tests\Support\Stub\PlanningPokerStoryVoteStub;

final class CreatePlanningPokerStoryVoteDataGenerator
{
    public static function valid(array $data = []): array
    {
        return array_replace_recursive([
            'card' => PlanningPokerStoryVoteStub::CARD_FOR_UPDATE,
        ], $data);
    }
}
