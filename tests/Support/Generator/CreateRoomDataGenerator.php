<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Generator;

use Dvlpm\Stream\Tests\Support\Stub\RoomStub;

final class CreateRoomDataGenerator
{
    public static function valid(array $data = []): array
    {
        return array_replace_recursive([
            'name' => RoomStub::ANONYMOUS_NAME,
            'accessType' => RoomStub::ANONYMOUS_ACCESS_TYPE,
        ], $data);
    }
}
