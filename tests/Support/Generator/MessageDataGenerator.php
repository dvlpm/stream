<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Generator;

final class MessageDataGenerator
{
    public static function violation(string $property, string $message): array
    {
        return [
            'type' => 'VIOLATION',
            'content' => [$property => $message],
        ];
    }

    public static function warning(string $message): array
    {
        return [
            'type' => 'WARNING',
            'content' => $message,
        ];
    }
}
