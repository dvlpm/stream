<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Generator;

use Dvlpm\Stream\Tests\Support\Stub\HallStub;

final class UpdateHallDataGenerator
{
    public static function valid(array $data = []): array
    {
        return array_merge_recursive([
            'name' => HallStub::NAME_FOR_UPDATE,
        ], $data);
    }
}
