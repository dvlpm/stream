<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Generator;

use Dvlpm\Stream\Tests\Support\Stub\PlanningPokerStub;

final class UpdatePlanningPokerDataGenerator
{
    public static function valid(array $data = []): array
    {
        return array_replace_recursive([
            'type' => PlanningPokerStub::TYPE,
            'cards' => PlanningPokerStub::CARDS_FOR_UPDATE,
        ], $data);
    }
}
