<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Generator;

use Dvlpm\Stream\Tests\Support\Stub\PlanningPokerPlayerStub;

final class UpdatePlanningPokerPlayerDataGenerator
{
    public static function valid(array $data = []): array
    {
        return array_replace_recursive([
            'status' => PlanningPokerPlayerStub::STATUS_FOR_UPDATE,
        ], $data);
    }
}
