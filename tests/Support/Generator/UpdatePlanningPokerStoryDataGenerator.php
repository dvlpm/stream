<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Generator;

use Dvlpm\Stream\Tests\Support\Stub\PlanningPokerStoryStub;

final class UpdatePlanningPokerStoryDataGenerator
{
    public static function valid(array $data = []): array
    {
        return array_replace_recursive([
            'name' => PlanningPokerStoryStub::NAME_FOR_UPDATE,
            'status' => PlanningPokerStoryStub::STATUS_FOR_UPDATE,
            'score' => PlanningPokerStoryStub::SCORE_FOR_UPDATE,
            'description' => PlanningPokerStoryStub::DESCRIPTION_FOR_UPDATE,
            'link' => PlanningPokerStoryStub::EXTRENAL_LINK_FOR_UPDATE,
        ], $data);
    }
}
