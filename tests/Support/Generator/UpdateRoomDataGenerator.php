<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Generator;

use Dvlpm\Stream\Tests\Support\Stub\RoomStub;

final class UpdateRoomDataGenerator
{
    public static function valid(array $data = []): array
    {
        return array_replace_recursive([
            'name' => RoomStub::NAME_FOR_UPDATE,
            'accessType' => RoomStub::ACCESS_TYPE_FOR_UPDATE,
        ], $data);
    }
}
