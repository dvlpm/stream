<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Generator;

use Dvlpm\Stream\Tests\Support\Stub\UserStub;

final class UpdateUserDataGenerator
{
    public static function valid(array $data = []): array
    {
        return array_merge_recursive([
            'name' => UserStub::NAME_FOR_UPDATE,
            'username' => UserStub::USERNAME_FOR_UPDATE,
            'meta' => [
                'random_key' => 'random_value',
            ],
        ], $data);
    }
}
