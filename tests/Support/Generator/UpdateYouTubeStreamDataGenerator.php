<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Generator;

use Dvlpm\Stream\Tests\Support\Stub\YouTubeStreamStub;

final class UpdateYouTubeStreamDataGenerator
{
    public static function valid(array $data = []): array
    {
        return array_replace_recursive([
            'type' => YouTubeStreamStub::TYPE,
            'link' => YouTubeStreamStub::LINK
        ], $data);
    }
}
