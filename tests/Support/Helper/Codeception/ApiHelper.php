<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Helper\Codeception;

use Codeception\Lib\Interfaces\DependsOnModule;
use Codeception\Module;
use Codeception\Module\REST;
use Dvlpm\Stream\Infrastructure\Authentication\DataObject\AccessToken;

final class ApiHelper extends Module implements DependsOnModule
{
    private REST $rest;
    private UserHelper $userHelper;
    private ?AccessToken $accessToken = null;

    public function _depends(): array
    {
        return [
            REST::class,
            UserHelper::class,
        ];
    }

    public function _inject(REST $rest, UserHelper $userHelper): void
    {
        $this->rest = $rest;
        $this->userHelper = $userHelper;
    }

    public function haveAccessToken(?AccessToken $accessToken): void
    {
        $this->accessToken = $accessToken;
    }

    public function sendApiPost(string $url, $request): void
    {
        $this->assignAccessToken();
        $this->assignUser();
        $this->rest->sendPost(
            $url,
            json_encode($request, \JSON_THROW_ON_ERROR, 512)
        );
    }

    public function sendApiPut(string $url, $request): void
    {
        $this->assignAccessToken();
        $this->assignUser();
        $this->rest->sendPUT(
            $url,
            json_encode($request, \JSON_THROW_ON_ERROR, 512)
        );
    }

    public function sendApiGet(string $url, $params = []): void
    {
        $this->assignAccessToken();
        $this->assignUser();
        $this->rest->sendGET($url, $params);
    }

    public function sendApiDelete(string $url, $params = []): void
    {
        $this->assignAccessToken();
        $this->assignUser();
        $this->rest->sendDELETE($url, $params);
    }

    private function assignAccessToken(): void
    {
        if ($this->accessToken === null) {
            return;
        }
        $this->rest->haveHttpHeader('Authorization', 'Bearer ' . $this->accessToken);
    }

    private function assignUser(): void
    {
        if ($this->userHelper->getUser() === null) {
            return;
        }
        $this->rest->haveHttpHeader('User-Id', (string) $this->userHelper->getUser()->getId());
    }

    public function seeInApiResponse($expectedNeedleInResponse): void
    {
        if (is_array($expectedNeedleInResponse)) {
            $this->rest->seeResponseContainsJson($expectedNeedleInResponse);

            return;
        }

        if (is_string($expectedNeedleInResponse)) {
            $this->rest->seeResponseContains($expectedNeedleInResponse);

            return;
        }

        $this->assertTrue(false, 'Invalid expected needle in response provided');
    }

    public function dontSeeInApiResponse($expectedNeedleInResponse): void
    {
        if (is_array($expectedNeedleInResponse)) {
            $this->rest->dontSeeResponseContainsJson($expectedNeedleInResponse);

            return;
        }

        if (is_string($expectedNeedleInResponse)) {
            $this->rest->dontSeeResponseContains($expectedNeedleInResponse);

            return;
        }

        $this->assertTrue(false, 'Invalid expected needle in response provided');
    }

    public function seeApiResponseCodeIs(int $code): void
    {
        $this->rest->seeResponseCodeIs($code);
    }
}
