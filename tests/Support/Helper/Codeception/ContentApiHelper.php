<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Helper\Codeception;

use Codeception\Lib\Interfaces\DependsOnModule;
use Codeception\Module;

final class ContentApiHelper extends Module implements DependsOnModule
{
    private const URL_PREFIX = '/content';
    public const CREATE_URL_TEMPLATE = self::URL_PREFIX;
    public const UPDATE_URL_TEMPLATE = self::URL_PREFIX . '/%s';
    public const SHOW_URL_TEMPLATE = self::URL_PREFIX . '/%s';
    public const REMOVE_URL_TEMPLATE = self::URL_PREFIX . '/%s';

    private ApiHelper $apiHelper;

    public function _depends()
    {
        return [
            ApiHelper::class,
        ];
    }

    public function _inject(ApiHelper $api): void
    {
        $this->apiHelper = $api;
    }

    public function sendCreateContentApiRequest(array $request): void
    {
        $this->apiHelper->sendApiPost(self::CREATE_URL_TEMPLATE, $request);
    }

    public function sendUpdateContentApiRequest(string $id, array $request): void
    {
        $this->apiHelper->sendApiPut(
            sprintf(
                self::UPDATE_URL_TEMPLATE,
                $id
            ),
            $request
        );
    }

    public function sendShowContentApiRequest(string $id): void
    {
        $this->apiHelper->sendApiGet(sprintf(
            self::SHOW_URL_TEMPLATE,
            $id
        ));
    }

    public function sendRemoveContentApiRequest(string $id): void
    {
        $this->apiHelper->sendApiDelete(sprintf(
            self::REMOVE_URL_TEMPLATE,
            $id
        ));
    }
}
