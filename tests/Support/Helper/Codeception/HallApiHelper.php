<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Helper\Codeception;

use Codeception\Lib\Interfaces\DependsOnModule;
use Codeception\Module;

final class HallApiHelper extends Module implements DependsOnModule
{
    private const URL_PREFIX = '/hall';
    public const CREATE_URL_TEMPLATE = self::URL_PREFIX;
    public const LIST_URL_TEMPLATE = self::URL_PREFIX . 's';
    public const ADD_ROOM_URL_TEMPLATE = self::URL_PREFIX . '/%s/room';
    public const UPDATE_URL_TEMPLATE = self::URL_PREFIX . '/%s';
    public const SHOW_URL_TEMPLATE = self::URL_PREFIX . '/%s';
    public const REMOVE_URL_TEMPLATE = self::URL_PREFIX . '/%s';

    private ApiHelper $apiHelper;

    public function _depends()
    {
        return [
            ApiHelper::class,
        ];
    }

    public function _inject(ApiHelper $api): void
    {
        $this->apiHelper = $api;
    }

    public function sendCreateHallApiRequest(array $request): void
    {
        $this->apiHelper->sendApiPost(self::CREATE_URL_TEMPLATE, $request);
    }

    public function sendAddRoomToHallApiRequest(string $identifier, array $request): void
    {
        $this->apiHelper->sendApiPost(
            sprintf(
                self::ADD_ROOM_URL_TEMPLATE,
                $identifier
            ),
            $request
        );
    }

    public function sendUpdateHallApiRequest(string $identifier, array $request): void
    {
        $this->apiHelper->sendApiPut(
            sprintf(
                self::UPDATE_URL_TEMPLATE,
                $identifier
            ),
            $request
        );
    }

    public function sendShowHallApiRequest(string $identifier): void
    {
        $this->apiHelper->sendApiGet(sprintf(
            self::SHOW_URL_TEMPLATE,
            $identifier
        ));
    }

    public function sendListHallsApiRequest(
        ?string $hostUserId = null,
        iterable $hostRoles = []
    ): void {
        $this->apiHelper->sendApiGet(
            self::LIST_URL_TEMPLATE,
            [
                'host.user.id' => $hostUserId,
                'host.roles' => $hostRoles,
            ]
        );
    }

    public function sendRemoveHallApiRequest(string $identifier): void
    {
        $this->apiHelper->sendApiDelete(sprintf(
            self::REMOVE_URL_TEMPLATE,
            $identifier
        ));
    }
}
