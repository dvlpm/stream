<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Helper\Codeception;

use Codeception\Lib\Interfaces\DependsOnModule;
use Codeception\Module;

final class ParticipantApiHelper extends Module implements DependsOnModule
{
    private const URL_PREFIX = '/participant';
    public const REMOVE_URL_TEMPLATE = self::URL_PREFIX . '/%s';

    private ApiHelper $apiHelper;

    public function _depends()
    {
        return [
            ApiHelper::class,
        ];
    }

    public function _inject(ApiHelper $api): void
    {
        $this->apiHelper = $api;
    }

    public function sendRemoveParticipantApiRequest(string $id): void
    {
        $this->apiHelper->sendApiDelete(sprintf(
            self::REMOVE_URL_TEMPLATE,
            $id
        ));
    }
}
