<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Helper\Codeception;

use Codeception\Lib\Interfaces\DependsOnModule;
use Codeception\Module;

final class PlanningPokerApiHelper extends Module implements DependsOnModule
{
    private const URL_PREFIX = '/planning-poker';
    public const ADD_STORY_URL_TEMPLATE = self::URL_PREFIX . '/%s/story';
    public const ADD_PLAYER_URL_TEMPLATE = self::URL_PREFIX . '/%s/player';
    public const LIST_STORIES_URL_TEMPLATE = self::URL_PREFIX . '/%s/stories';

    private ApiHelper $apiHelper;

    public function _depends()
    {
        return [
            ApiHelper::class,
        ];
    }

    public function _inject(ApiHelper $api): void
    {
        $this->apiHelper = $api;
    }

    public function sendAddPlanningPokerStoryToPlanningPokerApiRequest(string $id, array $request): void
    {
        $this->apiHelper->sendApiPost(
            sprintf(
                self::ADD_STORY_URL_TEMPLATE,
                $id
            ),
            $request
        );
    }

    public function sendAddPlanningPokerPlayerToPlanningPokerApiRequest(string $id, array $request): void
    {
        $this->apiHelper->sendApiPost(
            sprintf(
                self::ADD_PLAYER_URL_TEMPLATE,
                $id
            ),
            $request
        );
    }

    public function sendListPlanningPokerStoriesApiRequest(
        string $id,
        ?string $status = null,
        ?int $limit = null,
        ?int $offset = null
    ): void {
        $this->apiHelper->sendApiGet(
            sprintf(
                self::LIST_STORIES_URL_TEMPLATE,
                $id
            ),
            [
                'status' => $status,
                'limit' => $limit,
                'offset' => $offset,
            ]
        );
    }
}
