<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Helper\Codeception;

use Codeception\Lib\Interfaces\DependsOnModule;
use Codeception\Module;

final class PlanningPokerPlayerApiHelper extends Module implements DependsOnModule
{
    private const URL_PREFIX = '/planning-poker-player';
    public const UPDATE_URL_TEMPLATE = self::URL_PREFIX . '/%s';
    public const REMOVE_URL_TEMPLATE = self::URL_PREFIX . '/%s';

    private ApiHelper $apiHelper;

    public function _depends()
    {
        return [
            ApiHelper::class,
        ];
    }

    public function _inject(ApiHelper $api): void
    {
        $this->apiHelper = $api;
    }

    public function sendUpdatePlanningPokerPlayerApiRequest(string $id, array $request): void
    {
        $this->apiHelper->sendApiPut(
            sprintf(
                self::UPDATE_URL_TEMPLATE,
                $id
            ),
            $request
        );
    }

    public function sendRemovePlanningPokerPlayerApiRequest(string $id): void
    {
        $this->apiHelper->sendApiDelete(
            sprintf(
                self::REMOVE_URL_TEMPLATE,
                $id
            ),
        );
    }
}
