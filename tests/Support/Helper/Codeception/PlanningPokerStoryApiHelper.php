<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Helper\Codeception;

use Codeception\Lib\Interfaces\DependsOnModule;
use Codeception\Module;

final class PlanningPokerStoryApiHelper extends Module implements DependsOnModule
{
    private const URL_PREFIX = '/planning-poker-story';
    public const UPDATE_URL_TEMPLATE = self::URL_PREFIX . '/%s';
    public const REMOVE_URL_TEMPLATE = self::URL_PREFIX . '/%s';
    public const ADD_VOTE_URL_TEMPLATE = self::URL_PREFIX . '/%s/vote';
    public const REMOVE_VOTES_URL_TEMPLATE = self::URL_PREFIX . '/%s/votes';

    private ApiHelper $apiHelper;

    public function _depends()
    {
        return [
            ApiHelper::class,
        ];
    }

    public function _inject(ApiHelper $api): void
    {
        $this->apiHelper = $api;
    }

    public function sendAddPlanningPokerStoryVoteToPlanningPokerStoryApiRequest(string $id, array $request): void
    {
        $this->apiHelper->sendApiPost(
            sprintf(
                self::ADD_VOTE_URL_TEMPLATE,
                $id
            ),
            $request
        );
    }

    public function sendUpdatePlanningPokerStoryApiRequest(string $id, array $request): void
    {
        $this->apiHelper->sendApiPut(
            sprintf(
                self::UPDATE_URL_TEMPLATE,
                $id
            ),
            $request
        );
    }

    public function sendRemovePlanningPokerStoryVotesApiRequest(string $id): void
    {
        $this->apiHelper->sendApiDelete(sprintf(
            self::REMOVE_VOTES_URL_TEMPLATE,
            $id
        ));
    }

    public function sendShowPlanningPokerStoryApiRequest(string $id): void
    {
        $this->apiHelper->sendApiGet(
            sprintf(
                self::UPDATE_URL_TEMPLATE,
                $id
            )
        );
    }

    public function sendRemovePlanningPokerStoryApiRequest(string $id): void
    {
        $this->apiHelper->sendApiDelete(
            sprintf(
                self::REMOVE_URL_TEMPLATE,
                $id
            ),
        );
    }
}
