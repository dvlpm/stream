<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Helper\Codeception;

use Codeception\Lib\Interfaces\DependsOnModule;
use Codeception\Module;

final class RoomApiHelper extends Module implements DependsOnModule
{
    private const URL_PREFIX = '/room';
    public const SHOW_URL_TEMPLATE = self::URL_PREFIX . '/%s';
    public const REMOVE_URL_TEMPLATE = self::URL_PREFIX . '/%s';
    public const UPDATE_URL_TEMPLATE = self::URL_PREFIX . '/%s';
    public const ADD_CONTENT_URL_TEMPLATE = self::URL_PREFIX . '/%s/content';
    public const REMOVE_CONTENT_URL_TEMPLATE = self::URL_PREFIX . '/%s/content/%s';
    public const ADD_PARTICIPANT_URL_TEMPLATE = self::URL_PREFIX . '/%s/participant';

    private ApiHelper $apiHelper;

    public function _depends()
    {
        return [
            ApiHelper::class,
        ];
    }

    public function _inject(ApiHelper $api): void
    {
        $this->apiHelper = $api;
    }

    public function sendShowRoomApiRequest(string $id, ?string $key = null): void
    {
        $this->apiHelper->sendApiGet(
            sprintf(self::SHOW_URL_TEMPLATE, $id),
            $key !== null ? [
                'key' => $key,
            ] : []
        );
    }

    public function sendRemoveRoomApiRequest(string $id): void
    {
        $this->apiHelper->sendApiDelete(sprintf(
            self::REMOVE_URL_TEMPLATE,
            $id
        ));
    }

    public function sendUpdateRoomApiRequest(string $id, array $request): void
    {
        $this->apiHelper->sendApiPut(
            sprintf(
                self::UPDATE_URL_TEMPLATE,
                $id
            ),
            $request
        );
    }

    public function sendAddContentToRoomApiRequest(string $id, array $request): void
    {
        $this->apiHelper->sendApiPost(
            sprintf(
                self::ADD_CONTENT_URL_TEMPLATE,
                $id
            ),
            $request
        );
    }

    public function sendRemoveContentFromRoomApiRequest(string $id, string $contentId): void
    {
        $this->apiHelper->sendApiDelete(
            sprintf(
                self::REMOVE_CONTENT_URL_TEMPLATE,
                $id,
                $contentId
            )
        );
    }

    public function sendAddParticipantToRoomApiRequest(string $id, array $request): void
    {
        $this->apiHelper->sendApiPost(
            sprintf(
                self::ADD_PARTICIPANT_URL_TEMPLATE,
                $id
            ),
            $request
        );
    }
}
