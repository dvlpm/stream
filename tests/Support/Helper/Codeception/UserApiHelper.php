<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Helper\Codeception;

use Codeception\Lib\Interfaces\DependsOnModule;
use Codeception\Module;

final class UserApiHelper extends Module implements DependsOnModule
{
    private const URL_PREFIX = '/user';
    private const LIST_URL_TEMPLATE = self::URL_PREFIX . 's';
    public const UPDATE_URL_TEMPLATE = self::URL_PREFIX . '/%s';

    private ApiHelper $apiHelper;

    public function _depends()
    {
        return [
            ApiHelper::class,
        ];
    }

    public function _inject(ApiHelper $api): void
    {
        $this->apiHelper = $api;
    }

    public function sendUpdateUserApiRequest(string $id, array $request): void
    {
        $this->apiHelper->sendApiPut(
            sprintf(
                self::UPDATE_URL_TEMPLATE,
                $id
            ),
            $request
        );
    }

    public function sendListUsersApiRequest(
        ?string $searchQuery = null,
        ?int $limit = null,
        ?int $offset = null
    ): void {
        $this->apiHelper->sendApiGet(
            self::LIST_URL_TEMPLATE,
            [
                'searchQuery' => $searchQuery,
                'limit' => $limit,
                'offset' => $offset,
            ]
        );
    }
}
