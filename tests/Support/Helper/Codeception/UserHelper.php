<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Helper\Codeception;

use Codeception\Module;
use Dvlpm\Stream\Domain\User\Model\User;

final class UserHelper extends Module
{
    public static $excludeActions = [
        'getUser',
    ];

    private ?User $user = null;

    public function amUser(User $user): void
    {
        $this->user = $user;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }
}
