<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Stub;

final class AuthenticationStub
{
    public const ACCESS_TOKEN = 'access-token';
}
