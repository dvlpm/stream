<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Stub;

final class ContentStub
{
    public const YOUTUBE_STREAM_ID = '85b85dad-2ed6-4695-8c8f-981df67a8a6c';
    public const ANOTHER_YOUTUBE_STREAM_ID = '3e886c4d-99a4-4983-a3be-fd2ebd2ebbc3';

    public const PLANNING_POKER_ID = '1ea23f38-7ead-4000-8cd7-b2b741ef0f1e';
    public const PLANNING_POKER_WITH_STORIES_ID = '12e094a8-67ff-49ea-ab87-da6f3929df95';
}
