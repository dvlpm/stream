<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Stub;

final class EmailStub
{
    public const FIRST_EMAIL = 'first@gmail.com';
}
