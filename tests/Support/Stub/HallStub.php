<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Stub;

final class HallStub
{
    # -------------------------------
    # Common
    # -------------------------------
    public const ID = '6414ee51-5b73-46e5-8474-8db79e1575a7';
    public const NAME = 'Awesome Hall 2020';
    public const WITH_EVERY_ACCESS_TYPE_IDENTIFIER = 'AWESOMEHALL';

    # -------------------------------
    # For update
    # -------------------------------
    public const NAME_FOR_UPDATE = 'Awesome Hall 2020 Updated';
    public const START_AT_FOR_UPDATE = '11.05.2020 16:10';
    public const END_AT_FOR_UPDATE = '12.05.2020 10:00';

    public const ADMINISTRATED_BY_REGISTERED_USER_IDENTIFIER = 'ADMINISTRATED';
    public const ADMINISTRATED_BY_REGISTERED_USER_NAME = 'Name administrated';
    public const OWNED_BY_SECOND_REGISTERED_USER_IDENTIFIER = 'OWNEDBYSECONDUSER';
    public const OWNED_BY_SECOND_REGISTERED_USER_NAME = 'Name owned';
}
