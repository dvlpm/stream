<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Stub;

final class ParticipantStub
{
    # -------------------------------
    # To add
    # -------------------------------
    public const TO_ADD_ID = 'c9ea058a-3999-4f97-8641-446c9203053a';
    # -------------------------------
    # With email
    # -------------------------------
    public const WITH_EMAIL_ID = '959f1134-b3dc-4f7e-8c40-da433d495ee7';
    public const EMAIL = EmailStub::FIRST_EMAIL;

    # -------------------------------
    # With user id
    # -------------------------------
    public const IDENTIFIED_BY_ID_USER_ID = UserStub::SECOND_REGISTERED_USER_ID;
    public const IDENTIFIED_BY_EMAIL_USER_ID = UserStub::REGISTERED_WITH_EMAIL_USER_ID;
    public const GUEST_USER_ID = UserStub::GUEST_USER_ID;
}
