<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Stub;

final class PlanningPokerPlayerStub
{
    public const ID = '6bd899f5-c573-49ab-8845-22d6212ab799';
    public const USER_ID = UserStub::REGISTERED_USER_ID;

    public const ID_FOR_CREATE = 'd34715c7-25b2-404d-a10a-e743dc1b9ff9';
    public const USER_ID_FOR_CREATE = UserStub::REGISTERED_WITH_EMAIL_USER_ID;

    public const STATUS_FOR_UPDATE = 'OBSERVER';
}
