<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Stub;

final class PlanningPokerStoryStub
{
    public const ID = 'e97014ff-e401-464e-a150-90936ba9de0d';
    public const NAME = 'CONTENT-16';
    public const STATUS = 'ACTIVE';
    public const DESCRIPTION = 'Update story endpoint';
    public const EXTRENAL_LINK = 'https://jira.dvl.pm/browse/CONTENT-16';

    public const NAME_FOR_UPDATE = 'UPDATE-16';
    public const STATUS_FOR_UPDATE = 'COMPLETED';
    public const SCORE_FOR_UPDATE = '6';
    public const DESCRIPTION_FOR_UPDATE = 'Update story endpoint imlemented';
    public const EXTRENAL_LINK_FOR_UPDATE = 'https://jira.dvl.pm/browse/UPDATE-15';

    public const COMPLETED_ID = '07c0bf05-b09b-4a6c-8a41-a6de5d9f96f9';
    public const COMPLETED_NAME = 'COMPLETED-1';
    public const COMPLETED_DESCRIPTION = 'Completed story';
    public const COMPLETED_SCORE = '8';

    public const BACKLOG_ID = '01d506be-74da-4077-a1f4-ebb77c809c85';
    public const BACKLOG_NAME = 'BACKLOG-16';

    public const ID_FOR_CREATE = 'defa17ef-20c1-424c-966c-06c03f219981';
    public const NAME_FOR_CREATE = 'CONTENT-15';
    public const DESCRIPTION_FOR_CREATE = 'Add story endpoint';
    public const EXTRENAL_LINK_FOR_CREATE = 'https://jira.dvl.pm/browse/CONTENT-15';
}
