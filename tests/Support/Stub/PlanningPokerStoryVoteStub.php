<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Stub;

final class PlanningPokerStoryVoteStub
{
    public const CARD = 'COFFEE';
    public const CARD_FOR_UPDATE = 'HALF';
}
