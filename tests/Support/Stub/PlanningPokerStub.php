<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Stub;

final class PlanningPokerStub
{
    public const TYPE = 'PLANNING_POKER';
    public const CARD_ONE = 'ONE';
    public const CARD_HALF = 'HALF';
    public const CARD_QUESTION = 'QUESTION';
    public const CARD_FIVE = 'FIVE';

    public const CARDS = [
        PlanningPokerStub::CARD_ONE,
        PlanningPokerStub::CARD_HALF,
        PlanningPokerStub::CARD_QUESTION,
    ];

    public const CARDS_FOR_UPDATE = [
        self::CARD_ONE,
        self::CARD_HALF,
        self::CARD_FIVE,
        self::CARD_QUESTION,
    ];
}
