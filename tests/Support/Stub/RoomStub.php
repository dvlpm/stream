<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Stub;

final class RoomStub
{
    # -------------------------------
    # Common
    # -------------------------------
    public const START_AT = '15.05.2020 15:40';
    public const END_AT = '15.05.2020 17:00';

    # -------------------------------
    # Anonymous
    # -------------------------------
    public const ANONYMOUS_ID = '8a8854ef-606e-4992-b3c3-37403544dbe7';
    public const ANONYMOUS_NAME = 'Awesome Anonymous Access Room';
    public const ANONYMOUS_ACCESS_TYPE = 'ANONYMOUS';

    # -------------------------------
    # Authorized
    # -------------------------------
    public const AUTHORIZED_ID = '5d8a1700-81a5-438e-bbcc-60e11348c122';
    public const AUTHORIZED_NAME = 'Awesome Authorized Access Room';
    public const AUTHORIZED_ACCESS_TYPE = 'AUTHORIZED';

    # -------------------------------
    # Listed
    # -------------------------------
    public const LISTED_ID = '9a7a0292-8b1f-4e8c-a16f-94c72f3e0fc4';
    public const LISTED_NAME = 'Awesome Listed Access Room';
    public const LISTED_ACCESS_TYPE = 'LISTED';
    public const LISTED_KEY = '36osmkbzls0000cwc484c0wogw4k8k0wg0owoskk4ow4gcg0ss';

    # -------------------------------
    # For update
    # -------------------------------
    public const NAME_FOR_UPDATE = 'Awesome Anonymous Access Room 2 Updated';
    public const ACCESS_TYPE_FOR_UPDATE = 'AUTHORIZED';
    public const START_AT_FOR_UPDATE = '16.05.2020 19:40';
    public const END_AT_FOR_UPDATE = '16.05.2020 20:00';

    # -------------------------------
    # For create
    # -------------------------------
    public const ID_FOR_CREATE = 'bd6a00dd-d71e-466d-885d-9621262caf44';
    public const NAME_FOR_CREATE = 'Awesome Anonymous Access Room Created';
    public const ACCESS_TYPE_FOR_CREATE = 'AUTHORIZED';
    public const START_AT_FOR_CREATE = '16.05.2020 19:40';
    public const END_AT_FOR_CREATE = '16.05.2020 20:00';
}
