<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Stub;

final class UserStub
{
    # -------------------------------
    # Registered
    # -------------------------------
    public const REGISTERED_USER_ID = '85b85dad-2ed6-4695-8c8f-981df67a8a6a';
    public const REGISTERED_USER_EXTERNAL_ID = 'registered|external_id';

    public const SECOND_REGISTERED_USER_ID = 'd7ace461-b575-4e87-967e-32aecd60cf7b';
    public const REGISTERED_WITH_EMAIL_USER_ID = 'd7ace461-b575-4e87-967e-32aecd60cf7c';
    public const REGISTERED_WITH_EMAIL_EXTERNAL_ID = 'registered_with_email|external_id';
    public const FOURTH_REGISTERED_USER_ID = 'bec46d57-643b-4189-bf4b-adb58d7531c5';

    # -------------------------------
    # Guest
    # -------------------------------
    public const GUEST_USER_ID = 'fa7ddbda-b07e-4b7b-9e9a-5b0218a92df4';


    public const NAME_FOR_UPDATE = 'User Name';
    public const USERNAME_FOR_UPDATE = 'username';
}
