<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Stub;

final class YouTubeStreamStub
{
    public const TYPE = 'YOUTUBE_STREAM';
    public const LINK = 'https://youtube.com';

    public const ANOTHER_LINK = 'https://youtube.com/another';
}
