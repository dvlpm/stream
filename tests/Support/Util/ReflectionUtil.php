<?php

declare(strict_types=1);

namespace Dvlpm\Stream\Tests\Support\Util;

use ReflectionClass;

final class ReflectionUtil
{
    public static function instantiateWithConstructorArgs(string $class, ...$constructorArgs): object
    {
        $reflectionClass = new ReflectionClass($class);
        $object = $reflectionClass->newInstanceWithoutConstructor();
        $constructor = $reflectionClass->getConstructor();
        $constructor->setAccessible(true);
        $constructor->invokeArgs($object, $constructorArgs);

        return $object;
    }

    public static function instantiate(string $class): object
    {
        $reflectionClass = new ReflectionClass($class);

        return $reflectionClass->newInstanceWithoutConstructor();
    }

    public static function setPrivateProperty(string $class, object $object, string $name, $value): void
    {
        $property = new \ReflectionProperty($class, $name);
        $property->setAccessible(true);
        $property->setValue($object, $value);
    }
}
